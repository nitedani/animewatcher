import { Injectable } from '@angular/core';
import { Anime } from 'server/models/anime.interface';
import { get as lodashGet } from 'lodash';

export class AnimeSorter {
  constructor() {}

  static getSorter(type: string) {
    switch (type) {
      case 'nosort':
        return AnimeSorter.topScore;
        break;
      case 'date':
        return AnimeSorter.sortByDate;
        break;
      case 'score':
        return AnimeSorter.sortByScore;
        break;
      case 'members':
        return AnimeSorter.sortByPopular;
        break;
      default:
        return AnimeSorter.sortByPopular;
        break;
    }
  }

  static noSort(anime: Anime[]) {
    return anime;
  }

  static topScore(anime: Anime[]) {
    return anime.sort((a, b) => b.score - a.score);
  }

  static sortByScore(anime: Anime[]) {
    anime.sort((a, b) => b.score - a.score);
    return AnimeSorter.sortBy(anime, [
      { key: 'type', dir: 'desc', customOrder: ['TV'] },
     // { key: 'continuing', dir: 'asc' }
    ]);
  }

  static sortByDate(anime: Anime[]) {
    anime.sort(
      (a, b) =>
        (b.torrents.length ? new Date(b.torrents[b.torrents.length - 1].uploadDate).getTime() : 0) -
        (a.torrents.length ? new Date(a.torrents[a.torrents.length - 1].uploadDate).getTime() : 0)
    );
    return AnimeSorter.sortBy(anime, [
      { key: 'type', dir: 'desc', customOrder: ['TV'] },
     // { key: 'continuing', dir: 'asc' }
    ]);
  }

  static sortByPopular(anime: Anime[]) {
    return AnimeSorter.sortBy(anime, [
     // { key: 'type', dir: 'desc', customOrder: ['TV'] },
     // { key: 'continuing', dir: 'asc' },
      { key: 'members', dir: 'desc' }
    ]);
  }

  static sortBy<T>(
    toSort: T[],
    sortOpts: { key: string; dir?: 'asc' | 'desc'; priority?: number; customOrder?: any[] }[]
  ): T[] {
    sortOpts.sort((a, b) => (a.priority ? a.priority : 0) - (b.priority ? b.priority : 0));
    return toSort.sort((a, b) =>
      sortOpts.reduce((acc, opt) => {
        const key = opt.key;
        const dir = opt.dir === 'desc' ? -1 : 1;
        const currSort = opt.customOrder
          ? (opt.customOrder.findIndex(value => value === lodashGet(a, key)) -
              opt.customOrder.findIndex(value => value === lodashGet(b, key))) *
            dir
          : lodashGet(a, key) === lodashGet(b, key)
          ? 0
          : (lodashGet(a, key) ? lodashGet(a, key) : 0) < (lodashGet(b, key) ? lodashGet(b, key) : 0)
          ? -dir
          : dir;
        return acc || currSort;
      }, 0)
    );
  }
}
