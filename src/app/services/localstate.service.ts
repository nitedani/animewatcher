import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { isPlatformBrowser } from '@angular/common';
import { ReplaySubject, Observable } from 'rxjs';
import { map, mergeMap, distinctUntilChanged, take, shareReplay, filter } from 'rxjs/operators';
import { QueryService } from 'src/app/modules/currentseason/services/query.service';
import { BreakpointObserver } from '@angular/cdk/layout';
import { EventReplayer } from 'preboot';
import { isEqual } from 'lodash';
import { Anime } from 'server/models/anime.interface';
export interface WatchProgress {
  episodeId?: string;
  currentTime: number;
  duration: number;
  finished: boolean;
  timestamp: number;
}

export interface LocalState {
  watchList: { [malId: string]: boolean };
  isSmallScreen: boolean;
  watchProgress: { [malId: string]: { episodes: { [episodeId: string]: WatchProgress }; finished?: boolean } };
  selectedSubtitle: string;
  vlc: boolean;
  upscale: boolean;
}

@Injectable({ providedIn: 'root' })
export class LocalStateService {
  localState: LocalState = {
    watchList: {},
    watchProgress: {},
    isSmallScreen: true,
    selectedSubtitle: 'eng',
    vlc: false,
    upscale: false,
  };
  localState$ = new ReplaySubject<LocalState>(1);

  constructor(
    private storage: StorageMap,
    @Inject(PLATFORM_ID) private platformId: any,
    private queryService: QueryService,
    private breakpointObserver: BreakpointObserver,
    private replayer: EventReplayer
  ) {
    if (isPlatformBrowser(this.platformId)) {
      this.setupState();
    }
  }

  manualReplay() {
    this.replayer.replayAll();
  }

  private async setupState() {
    await this.loadState();

    this.breakpointObserver.observe('(max-width: 700px)').subscribe((next) => {
      if (next.matches) {
        this.localState.isSmallScreen = true;
      } else {
        this.localState.isSmallScreen = false;
      }
      this.saveState();
    });
  }
  private async saveState() {
    await this.storage.set('localState', this.localState).toPromise();
    this.localState$.next(this.localState);
  }

  private async loadState() {
    return await this.storage
      .get('localState')
      .toPromise()
      .then((data) => {
        if (data !== undefined) {
          this.localState = { ...this.localState, ...(data as LocalState) };
        }
        this.localState$.next(this.localState);
      });
  }

  public get isSmallScreen() {
    return this.localState$.pipe(
      map((state) => state.isSmallScreen),
      distinctUntilChanged()
    );
  }

  public get isSmallScreenString() {
    return this.localState$.pipe(
      map((state) => state.isSmallScreen),
      map((isSmallScreen) => (isSmallScreen ? 'mobile' : 'desktop')),
      distinctUntilChanged()
    );
  }

  public get watchList() {
    return this.localState$.pipe(
      map((state) => state.watchList),
      // I dont know why, distinctUntilChanged keeps emitting anyway
      // distinctUntilChanged(isEqual),
      mergeMap(
        (watchlist) =>
          this.queryService
            .getAnime(QueryService.AnimeSeasonQuery, { malId: Object.keys(watchlist).map((key) => parseInt(key, 10)) })
            .pipe(take(1)),
        1
      )
      // shareReplay()
    );
  }

  public get currentlyWatchingAnime() {
    return this.localState$.pipe(
      map((state) => state.watchProgress),
      mergeMap(
        (watchedAnime) =>
          this.queryService
            .getAnime(QueryService.AnimeSeasonQuery, {
              malId: Object.keys(watchedAnime)
                .filter((key) => !watchedAnime[key].finished)
                .map((key) => parseInt(key, 10)),
            })
            .pipe(
              take(1),
              map((animes) =>
                animes.filter(
                  (anime) =>
                    !watchedAnime[anime.malId].episodes[anime.torrents[anime.torrents.length - 1].episode]?.finished
                )
              )
            ),
        1
      ),
      map(
        (animes) =>
          animes.sort((a, b) => this.lastWatchedEpisode(b.malId).timestamp - this.lastWatchedEpisode(a.malId).timestamp)

        /*
            !(Object.values(this.localState.watchProgress[anime.malId].episodes).every(episode => episode.finished) &&
            anime.torrents.every(torrent => Object.keys(this.localState.watchProgress[anime.malId].episodes).includes(torrent.episode)))

            */
      )
      // shareReplay()
    );
  }

  lastWatchedEpisode(malId: number): WatchProgress {
    return Object.values(this.localState.watchProgress[malId].episodes).reduce((acc, curr) =>
      acc.timestamp < curr.timestamp ? curr : acc
    );
  }

  public changeWatchList(malId: number) {
    if (this.localState.watchList[malId]) {
      this.removeFromWatchlist(malId);
    } else {
      this.addToWatchlist(malId);
    }
  }

  public async addToWatchlist(malId: number) {
    this.localState.watchList[malId] = true;
    await this.saveState();
  }

  public async removeFromWatchlist(malId: number) {
    delete this.localState.watchList[malId];
    await this.saveState();
  }

  public isInWatchlist(malId: number): Observable<boolean> {
    return this.localState$.pipe(
      map((state) => state.watchList[malId]),
      distinctUntilChanged()
    );
  }

  public async setWatchProgress(anime: Anime, episodeId: string, opts: { progress: WatchProgress }) {
    if (!this.localState.watchProgress[anime.malId]) {
      this.localState.watchProgress[anime.malId] = {} as any;
      this.localState.watchProgress[anime.malId].episodes = {} as any;
    }
    this.localState.watchProgress[anime.malId].episodes[episodeId] = { ...opts.progress, episodeId };
    this.localState.watchProgress[anime.malId].finished =
      parseInt(episodeId, 10) === anime.episodes && opts.progress.finished;

    await this.saveState();
  }

  public async setFinishedAnime(malId: string) {
    this.localState.watchProgress[malId].finished = true;
    await this.saveState();
  }

  public getWatchProgress(malId: string) {
    return this.localState$.pipe(
      map((state) => (state.watchProgress[malId] ? state.watchProgress[malId] : { episodes: {} }))
    );
  }

  public async selectSubtitle(language: string) {
    this.localState.selectedSubtitle = language;
    await this.saveState();
  }

  public async toggleVlc(vlc: boolean) {
    this.localState.vlc = vlc;
    await this.saveState();
  }

  public get vlc() {
    return this.localState$.pipe(
      map((state) => state.vlc),
      distinctUntilChanged()
    );
  }

  public async toggleUpscale(upscale: boolean) {
    this.localState.upscale = upscale;
    await this.saveState();
  }

  public get upscale() {
    return this.localState$.pipe(
      map((state) => state.upscale),
      distinctUntilChanged()
    );
  }

  public getSelectedSubtitle() {
    return this.localState$.pipe(
      map((state) => state.selectedSubtitle),
      distinctUntilChanged()
    );
  }
}
