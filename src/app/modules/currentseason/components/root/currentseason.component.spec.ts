import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { CurrentseasonComponent } from './currentseason.component';

describe('CurrentseasonComponent', () => {
  let component: CurrentseasonComponent;
  let fixture: ComponentFixture<CurrentseasonComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentseasonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CurrentseasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
