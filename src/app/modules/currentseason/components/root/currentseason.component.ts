import { Component, OnInit, ViewChild, Inject, PLATFORM_ID, AfterViewInit, ElementRef } from '@angular/core';
import { Anime, GetAnimeArgs } from 'server/models/anime.interface';
import { QueryService } from '../../services/query.service';
import { Subject, of, BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map, flatMap, take } from 'rxjs/operators';
import { Season } from 'server/models/season.interface';
import { AnimeSorter } from 'src/app/services/animesorter';
import { trigger, state, style, animate, transition, query, stagger, keyframes } from '@angular/animations';
import { isPlatformBrowser } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { ThemeService } from 'src/app/modules/shared/theme/theme.service';
import { AnimationEvent } from '@angular/animations';
import { LocalStateService } from 'src/app/services/localstate.service';

@Component({
  selector: 'app-currentseason',
  templateUrl: './currentseason.component.html',
  styleUrls: ['./currentseason.component.scss'],
  animations: [
    trigger('coverAnimation', [
      state(
        '*',
        style({
          opacity: 1
        })
      ),
      transition(
        'void => *',
        animate('.2s ease-in', keyframes([style({ opacity: 0, offset: 0 }), style({ opacity: 1, offset: 1 })]))
      )
    ]),
    trigger('cardAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),

        query(
          ':enter',
          stagger('20ms', [
            animate(
              '.4s ease-out',
              keyframes([
                style({ opacity: 0, transform: 'translateY(-10%)', offset: 0 }),
                style({ opacity: 0.5, transform: 'translateY(-10px)', offset: 0.5 }),
                style({ opacity: 1, transform: 'translateY(0)', offset: 1 })
              ])
            )
          ]),
          { optional: true }
        )
      ])
    ]),
    trigger('cardAnimationMobile', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),

        query(
          ':enter',
          stagger('50ms', [
            animate('.4s ease-out', keyframes([style({ opacity: 0, offset: 0 }), style({ opacity: 1, offset: 1 })]))
          ]),
          { optional: true }
        )
      ])
    ]),
    trigger('prepareDetails', [
      // ...
      state('away', style({ opacity: 0 })),
      state('void', style({ opacity: 0 })),
      state('start', style({ opacity: 0 })),
      state('false', style({ opacity: 1 })),
      transition('* => start', [
        animate(
          '150ms ease-out',
          keyframes([
            style({ opacity: 1, offset: 0 }),
            style({ opacity: 0.8, filter: 'blur(0px)', offset: 0.5 }),
            style({ opacity: 0, transform: 'scale(0.8)', filter: 'blur(25px)', offset: 1 })
          ])
        )
      ]),
      transition('* => away', [
        animate(
          '150ms ease-out',
          keyframes([
            style({ opacity: 1, filter: 'blur(0px)', offset: 0 }),
            style({
              opacity: 0,
              transform: 'translateY(40px) scale(0.7)',
              filter: 'blur(25px)',
              offset: 0.5
            }),
            style({
              opacity: 0,
              transform: 'translateY(40px) scale(0.7)',
              filter: 'blur(25px)',
              offset: 1
            })
          ])
        )
      ])
    ]),
    trigger('toolbarAnimation', [
      state(
        '*',
        style({
          opacity: 1
        })
      ),
      state(
        'void',
        style({
          opacity: 0
        })
      ),
      transition(
        '* => void',
        animate(
          '150ms ease-out',
          keyframes([
            style({ opacity: 1, filter: 'blur(0px)', offset: 0 }),
            style({ opacity: 0, filter: 'blur(25px)', offset: 1 })
          ])
        )
      )
    ])
  ]
})
export class CurrentseasonComponent implements OnInit, AfterViewInit {
  sorter = AnimeSorter;
  genres = [
    'Action',
    'Adventure',
    'Cars',
    'Comedy',
    'Dementia',
    'Demons',
    'Drama',
    'Ecchi',
    'Fantasy',
    'Game',
    'Harem',
    'Historical',
    'Horror',
    'Josei',
    'Kids',
    'Magic',
    'Martial Arts',
    'Mecha',
    'Military',
    'Music',
    'Mystery',
    'Parody',
    'Police',
    'Psychological',
    'Romance',
    'Samurai',
    'School',
    'Sci-Fi',
    'Seinen',
    'Shoujo',
    'Shoujo Ai',
    'Shounen',
    'Shounen Ai',
    'Slice of Life',
    'Space',
    'Sports',
    'Super Power',
    'Supernatural',
    'Thriller',
    'Vampire',
    'Yaoi'
  ];

  selectedGenres: string[] = [];

  query: GetAnimeArgs = { season: '2021spring', sortBy: 'members', order: 'desc', r18: false, genres: [] };
  patchQuery$: Subject<GetAnimeArgs> = new Subject();
  prevSort = 'members';

  value = '';

  anime$: BehaviorSubject<Anime[]> = new BehaviorSubject([]);
  anime: Anime[];
  displayAnime: Anime[] = [];
  seasons: Season[];
  years: string[];
  animateCard = {};
  count = 20;
  sortChanged = false;
  selectedYear: string;
  selectedSeason: Season;
  selectedSort: string;
  toolbarState = '*';
  isSmallScreen$: Observable<boolean>;

  @ViewChild('container', { static: false }) cardCont: ElementRef;

  getSeasons = year => this.seasons.filter(season => season.year === year);

  constructor(
    @Inject(PLATFORM_ID) private platformId: any,
    private service: QueryService,
    private router: Router,
    private route: ActivatedRoute,
    private localStateService: LocalStateService
  ) {}

  toDetails = (malId: number, noAnimate: boolean) => {
    this.isSmallScreen$.pipe(take(1)).subscribe(small => {
      if (!small && !noAnimate) {
        this.animateCard[malId] = { state: 'start' };
        this.toolbarState = 'void';
        for (const key in this.animateCard) {
          if (key !== malId.toString()) this.animateCard[key] = { state: 'away' };
        }
      } else {
        this.router.navigate(['/anime', malId]);
      }
    });
  };

  nSizeArray(n: number) {
    return [...Array(n).keys()];
  }

  selectYear(event: Event, year: string) {
    event.stopPropagation();
    this.selectedYear = year;
  }

  patchTitle(title: string) {
    this.patchQuery$.next({
      title
    });
  }

  getSeasonalAnime(season: string, sort: string) {
    if (!sort) sort = 'members';
    if (!season) season = '2021spring';
    this.selectedSeason = this.seasons.find(s => s.value === season);
    this.patchQuery$.next({
      ...(sort && { sortBy: sort as any }),
      season,
      r18: false
    });
  }

  getTopAnime(sort?: string) {
    this.selectedSeason = { value: 'top', viewValue: 'ALL SEASONS', year: 'none', season: 'none' };
    this.patchQuery$.next({
      sortBy: sort as any,
      order: 'desc',
      limit: 200,
      r18: false,
      season: null
    });
  }

  openDetails(event: AnimationEvent, malId: number) {
    if (event.toState === 'start') this.router.navigate(['/anime', malId]);
  }

  fillUpCards(scroll?: number) {
    if (
      this.cardCont &&
      this.cardCont.nativeElement &&
      this.cardCont.nativeElement.lastElementChild &&
      window &&
      this.count < this.anime.length &&
      window.innerHeight + scroll > (this.cardCont.nativeElement.lastElementChild as HTMLElement).offsetTop
    ) {
      const freeHeight =
        window.innerHeight + scroll - (this.cardCont.nativeElement.lastElementChild as HTMLElement).offsetTop;
      const rowsToPush =
        Math.floor(freeHeight / (this.cardCont.nativeElement as HTMLElement).firstElementChild.clientHeight) + 1;

      const toPushCount =
        Math.floor(
          (this.cardCont.nativeElement as HTMLElement).clientWidth /
            (this.cardCont.nativeElement as HTMLElement).firstElementChild.clientWidth
        ) * rowsToPush;
      /*
      if((this.count + toPushCount) > this.anime.length)
       toPushCount = this.anime.length - this.count;
*/
      for (let index = 0; index < toPushCount && this.count < this.anime.length; index++) {
        this.displayAnime.push(this.anime[this.count]);
        this.count++;
      }
    }
  }

  ngAfterViewInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      const scrollSubj = new BehaviorSubject(0);
      window.addEventListener(
        'scroll',
        e => {
          const scroll = (e.target as any).scrollTop;
          scrollSubj.next(scroll);
        },
        true
      );

      scrollSubj.subscribe(scroll => this.fillUpCards(scroll));
    }
  }

  ngOnInit(): void {
    this.isSmallScreen$ = this.localStateService.isSmallScreen;
    this.service.getSeasons().then(seasons => {
      let searchAllSeasons = false;

      this.seasons = this.sorter.sortBy(seasons, [
        { key: 'year', dir: 'desc' },
        { key: 'season', customOrder: ['WINTER', 'SPRING', 'SUMMER', 'FALL'], dir: 'desc' }
      ]);
      this.years = [...new Set(this.seasons.map(s => s.year))];
      let prevSelectedSeason: Season = this.selectedSeason;

      if (isPlatformBrowser(this.platformId)) {
        this.patchQuery$
          .pipe(
            flatMap(patchQuery => {
              for (const key in patchQuery) {
                if (patchQuery.hasOwnProperty(key)) {
                  this.query[key] = patchQuery[key];
                }
              }

              if ((this.query.title as string)?.length > 2) {
                if (!searchAllSeasons) {
                  searchAllSeasons = true;
                  prevSelectedSeason = this.selectedSeason;
                  this.query.season = null;
                  this.query.limit = 25;
                  this.selectedSeason = { value: '', viewValue: 'ALL SEASONS', year: 'none', season: 'none' };
                }
                if (this.selectedSeason.value) prevSelectedSeason = this.selectedSeason;
              } else {
                if (searchAllSeasons) {
                  searchAllSeasons = false;
                  if (prevSelectedSeason.value === 'top') {
                    this.query.limit = 200;
                    this.query.season = null;
                  } else {
                    this.query.limit = null;
                    this.query.season = prevSelectedSeason.value;
                  }

                  this.selectedSeason = prevSelectedSeason;
                }
              }

              return this.service
                .getAnime(QueryService.AnimeSeasonQuery, {
                  ...(this.query.title && { title: this.query.title }),
                  ...(this.query.season && { season: this.query.season }),
                  ...(this.query.genres.length && { genres: this.query.genres }),
                  ...(this.query.sortBy && {
                    sortBy: (this.query.sortBy as any) === 'nosort' ? 'score' : this.query.sortBy
                  }),
                  ...(this.query.order && { order: this.query.order }),
                  ...(this.query.r18 && { r18: this.query.r18 }),
                  ...(this.query.limit && { limit: this.query.limit })
                })
                .pipe(
                  map(_anime => {
                    if (this.anime) {
                      const currentAnimeIds = this.anime.map(a => a.malId);
                      const queryAnimeIds = _anime.map(b => b.malId);
                      const newAnime = [...this.anime.filter(a => queryAnimeIds.includes(a.malId))];
                      newAnime.push(..._anime.filter(b => !currentAnimeIds.includes(b.malId)));
                      return this.sorter.getSorter(this.query.sortBy)(newAnime);
                    } else {
                      return this.sorter.getSorter(this.query.sortBy)(_anime);
                    }
                  })
                );
            })
          )
          .subscribe(anime => {
            this.animateCard = {};
            anime.forEach(a => (this.animateCard[a.malId] = { state: 'stop' }));
            this.anime = anime;
            this.displayAnime = anime.slice(0, 20);
            this.count = 20;
          });

        this.route.paramMap.subscribe(params => {
          const seasonParam = params.get('season');
          const sortParam = params.get('sort');

          if (seasonParam !== 'top') {
            this.getSeasonalAnime(seasonParam, sortParam as any);
          } else {
            this.getTopAnime(sortParam as any);
          }
        });
      }
    });
  }

  navigate(opts: { season?: string; sort?: string; search?: string }) {
    if (!opts.season) opts.season = this.selectedSeason.value;
    if (!opts.sort) {
      opts.sort = this.prevSort;
    } else if (opts.season !== 'top') {
      this.prevSort = opts.sort;
    }
    this.router.navigate(['/browse', opts]);
  }

  clearSearch() {
    this.value = '';
    this.patchQuery$.next({ title: null });
  }

  selectGenre(event: Event, genre: string) {
    event.stopPropagation();
    if (this.selectedGenres.includes(genre)) this.selectedGenres = this.selectedGenres.filter(g => g !== genre);
    else this.selectedGenres.push(genre);
    this.patchQuery$.next({ genres: this.selectedGenres });
  }
}
