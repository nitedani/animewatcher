import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  Inject,
  PLATFORM_ID,
  AfterViewInit,
  Output
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Anime } from 'server/models/anime.interface';
import { Router } from '@angular/router';
import { Torrent } from 'server/models/torrent.interface';
import { AnimeDetailsService } from '../../services/anime-details.service';
import { of, Observable, Subscription, fromEvent, defer, concat } from 'rxjs';
import * as getVideoId from 'get-video-id';
import { map, take, tap, delay, timeoutWith, skipUntil, mapTo, filter, flatMap } from 'rxjs/operators';
import { ThemeService } from 'src/app/modules/shared/theme/theme.service';
import { LocalStateService } from 'src/app/services/localstate.service';
import { isPlatformBrowser } from '@angular/common';
import { trigger, state, style, animate, transition, query, stagger, keyframes } from '@angular/animations';
import { EventEmitter } from '@angular/core';
import { QueryService } from '../../services/query.service';

@Component({
  selector: 'app-anime-item',
  templateUrl: './anime-item.component.html',
  styleUrls: ['./anime-item.component.css'],
  animations: [
    trigger('coverAnimation', [
      state(
        '*',
        style({
          opacity: 1
        })
      ),
      transition(
        'void => *',
        animate('.2s ease-in', keyframes([style({ opacity: 0, offset: 0 }), style({ opacity: 1, offset: 1 })]))
      )
    ])
  ]
})
export class AnimeItemComponent implements OnInit, AfterViewInit {
  isMobileString$: Observable<string>;
  trailerUrl$: Observable<string>;
  hovered = false;
  ytBarWidth = 0;
  iframe: HTMLIFrameElement;
  playingTrailerObs: Observable<boolean>;
  inWatchlist$: Observable<boolean>;
  playYtDelay: Subscription;
  trailerStarted = false;
  moveAndStopSubscription$: Subscription;
  @Input() anime: Anime;
  @Output() clicked = new EventEmitter();

  @ViewChild('card', { static: true }) cardRef: ElementRef;

  constructor(
    private sanitizer: DomSanitizer,
    private router: Router,
    private themeService: ThemeService,
    private localStateService: LocalStateService,
    private queryService: QueryService,
    @Inject(PLATFORM_ID) private platformId: any
  ) {
    this.hovered = false;
    this.playingTrailerObs = this.themeService.playingTrailerObs;
  }

  prepareDetails() {
    this.clicked.emit('click');
  }

  openDetails() {
    this.router.navigate(['/anime', this.anime.malId]);
  }

  watchListClick(event: Event) {
    event.stopPropagation();
    this.localStateService.changeWatchList(this.anime.malId);
  }

  public onYtStateChange(event) {
    if (event.data === YT.PlayerState.PLAYING) {
      if (this.trailerStarted === false) {
        this.iframe.style.left = 0 + 'px';
        this.ytBarWidth = 640;
      }

      this.trailerStarted = true;
      this.themeService.playingTrailerObs.next(true);
    }
  }

  public videoReady(event) {
    this.iframe = document.querySelector('iframe');
    event.target.playVideo();
    this.iframe.style.pointerEvents = 'auto';
  }

  public mouseEvent(status: boolean) {
    if (status) {
      this.hovered = status;
    } else {
      this.ytBarWidth = 0;
      this.hovered = false;
      if (this.trailerStarted) this.themeService.trailerStopped();
      if (this.moveAndStopSubscription$) this.moveAndStopSubscription$.unsubscribe();
      this.trailerStarted = false;
    }
  }

  startStream(torrent: Torrent) {
    this.router.navigate(['/watch', torrent.uniqueID]);
  }

  getImageUrl(url: string, large: boolean = false) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${large ? url.replace('.jpg', 'l.jpg') : url})`);
  }

  openMalPage(event: MouseEvent) {
    event.stopPropagation();
    window.open(this.anime.url, '_blank');
  }

  onMouseEnter() {
    const move$ = fromEvent(this.cardRef.nativeElement, 'mousemove').pipe(mapTo('move'));
    const moveAndStop$ = move$.pipe(
      timeoutWith(
        50,
        defer(() => concat(of('stop'), moveAndStop$.pipe(skipUntil(move$))))
      ),
      filter(e => e === 'stop')
    );

    this.moveAndStopSubscription$ = moveAndStop$.subscribe(() => this.mouseEvent(true));
  }

  nextEpisodeNumber() {
    if (this.anime.torrents.length) {
      const nextEpisode = (parseInt(this.anime.torrents[this.anime.torrents.length - 1].episode, 10) + 1).toString();
      return nextEpisode.length === 1 ? '0' + nextEpisode : nextEpisode;
    } else {
      return '01';
    }
  }
  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.isMobileString$ = this.localStateService.isSmallScreenString;
      this.trailerUrl$ = this.anime.trailer_url
        ? of(getVideoId(this.anime.trailer_url).id)
        : this.queryService
            .getAnime(QueryService.AnimeDetailsQuery, { malId: this.anime.malId, fetchDetails: 'force' })
            .pipe(map(anime => getVideoId(anime[0].trailer_url).id));
      this.inWatchlist$ = this.localStateService.isInWatchlist(this.anime.malId);
    }
  }

  ngAfterViewInit(): void {}
}
