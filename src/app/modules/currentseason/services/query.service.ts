import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Anime, GetAnimeArgs } from 'server/models/anime.interface';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import { filter, map } from 'rxjs/operators';
import { Season } from 'server/models/season.interface';
import { DocumentNode } from 'apollo-link';

export type AnimeQuery = (opts?: string) => DocumentNode;

@Injectable()
export class QueryService {
  constructor(private readonly apollo: Apollo) {}

  public static AnimeSeasonQuery: AnimeQuery = (query?: string) => {
    return gql`
    query AnimeDetailsQuery {
      anime${query ? '(' + query + ')' : ''} {
        image_url
        trailer_url
        title
        malId
        url
        score
        members
        continuing
        type
        normalizedName
        season
        episodes
        recommendations{mal_id, recommendation_count}
        genres{name}
        nextEpisode{
          days
          hours
        }
        torrents{
          uniqueID
          episode
          uploadDate
        }
      }
    }
  `;
  };

  public static RecommendationQuery: AnimeQuery = (query?: string) => gql`
  query RecommendationQuery {
    anime${query ? '(' + query + ')' : ''} {
      image_url
      title
      malId
      url
      score
      members
      continuing
      type
      normalizedName
      season
      episodesUpdated
      torrents{ seeders uploadDate}
    }
  }
`;

  public static AnimeDetailsQuery: AnimeQuery = (query?: string) => gql`
  query AnimeDetailsQuery {
    anime${query ? '(' + query + ')' : ''} {
      image_url
      trailer_url
      title
      malId
      url
      score
      members
      continuing
      type
      normalizedName
      season
      synopsis
      recommendations{mal_id}
      genres{name}
      studios{name}
      type
      status
      rank
      source
      title_english
      aired{string}
      related {
        Prequel {
          name
          mal_id
        }
        Sequel {
          name
          mal_id
        }
        Adaptation {
          name
          mal_id
        }
      }
      nextEpisode{
        days
        hours
      }
      torrents{
        uniqueID
        episode
        uploadDate
      }
    }
  }
`;

  public static SeasonQuery = () => gql`
    query SeasonQuery {
      season {
        value
        viewValue
        year
        season
      }
    }
  `;

  getSeasons(): Promise<Season[]> {
    return this.apollo
      .query<{ season: Season[] }>({ query: QueryService.SeasonQuery() })
      .pipe(
        filter(({ data }) => data.season.length > 0),
        map(({ data }) => data.season)
      )
      .toPromise();
  }

  getAnime(query: AnimeQuery, opts?: GetAnimeArgs): Observable<Anime[]> {
    if (opts.malId && !Array.isArray(opts.malId)) opts.malId = [opts.malId];
    opts = { ...opts, r18: false };
    return this.apollo
      .watchQuery<{ anime: Anime[] }>({
        query: opts
          ? query(
              Object.keys(opts).reduce(
                (acc, curr) =>
                  (acc +=
                    curr +
                    (typeof opts[curr] === 'string'
                      ? ':"' + opts[curr] + '" '
                      : ':' + JSON.stringify(opts[curr]) + ' ')),
                ''
              )
            )
          : query()
      })
      .valueChanges.pipe(map(({ data }) => data.anime));
  }
}
