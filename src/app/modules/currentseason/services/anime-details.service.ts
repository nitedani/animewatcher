import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnimeDetailsService {
  constructor(private http: HttpClient) {}

  getTrailer(id: number): Observable<string> {
    return this.http.get<any>(`https://api.jikan.moe/v3/anime/${id}`).pipe(
      take(1),
      map(details => details.trailer_url)
    );
  }
}
