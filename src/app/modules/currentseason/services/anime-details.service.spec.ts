import { TestBed } from '@angular/core/testing';

import { AnimeDetailsService } from './anime-details.service';

describe('AnimeDetailsService', () => {
  let service: AnimeDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnimeDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
