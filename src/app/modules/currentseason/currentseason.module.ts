import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { CurrentseasonComponent } from './components/root/currentseason.component';
import { AnimeItemComponent } from './components/anime-item/anime-item.component';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { YoutubeComponent } from './components/youtube/youtube.component';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule } from '@angular/forms';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
@NgModule({
  imports: [
    YouTubePlayerModule,
    CommonModule,
    RouterModule.forChild([{ path: '', component: CurrentseasonComponent, pathMatch: 'full' }]),
    SharedModule,
    MaterialModule,
    FormsModule,
    VirtualScrollerModule
  ],
  providers: [],
  declarations: [CurrentseasonComponent, AnimeItemComponent, YoutubeComponent],
})
export class CurrentSeasonModule {}
