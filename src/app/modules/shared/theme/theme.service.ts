import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

export enum Theme {
  DARK = 'dark-theme',
  BLACK = 'black-theme'
}

@Injectable()
export class ThemeService {
  private _theme = new BehaviorSubject<Theme>(Theme.DARK);
  public playingTrailerObs = new BehaviorSubject(false);

  public navbarHidden = new BehaviorSubject(false);

  trailerPlaying() {
    this.playingTrailerObs.next(true);
  }
  trailerStopped() {
    this.playingTrailerObs.next(false);
  }

  setTheme(theme: Theme): void {
    this._theme.next(theme);
  }

  getTheme(): Observable<Theme> {
    return this._theme.asObservable();
  }
}
