import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';


@Injectable()
export class MySocket {
  public socket: Socket;
  constructor() {
    this.socket = new Socket({ url: window.location.origin, options: {} });
  }
}