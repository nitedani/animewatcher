import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UniversalInterceptorService } from './interceptors/universal-interceptor.service';
import { ThemeService } from './theme/theme.service';
import { MySocket } from './socket/socket.service';


@NgModule({
  imports: [CommonModule],
  providers: [UniversalInterceptorService, ThemeService, MySocket],
  declarations: []
})
export class SharedModule {}
