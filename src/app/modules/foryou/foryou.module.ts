import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ForyouComponent } from './components/foryou.component';
import { SharedModule } from '../shared/shared.module';
import { EpisodeCardComponent } from './components/episode-card/episode-card.component';
import { ScrollerComponent } from './components/scroller/scroller.component';
import { MaterialModule } from 'src/app/material.module';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
@NgModule({
  declarations: [ForyouComponent, EpisodeCardComponent, ScrollerComponent],
  imports: [
    RouterModule.forChild([{ path: '', component: ForyouComponent, pathMatch: 'full' }]),
    CommonModule,
    SharedModule,
    MaterialModule,
    VirtualScrollerModule
  ]
})
export class ForyouModule {}
