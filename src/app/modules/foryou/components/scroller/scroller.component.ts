import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit,
  Inject,
  PLATFORM_ID,
  OnDestroy,
  AfterViewChecked
} from '@angular/core';
import { Anime } from 'server/models/anime.interface';
import { isPlatformBrowser } from '@angular/common';
import ScrollBooster from 'scrollbooster';
import { trigger, transition, query, style, animate, stagger, keyframes, state } from '@angular/animations';
import { Observable, Subject } from 'rxjs';
import { LocalStateService } from 'src/app/services/localstate.service';
import { takeUntil, map } from 'rxjs/operators';

@Component({
  selector: 'app-scroller',
  templateUrl: './scroller.component.html',
  styleUrls: ['./scroller.component.scss'],
  animations: [
    trigger('cardAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),
        query(
          ':enter',
          stagger('20ms', [
            animate(
              '.4s ease-out',
              keyframes([
                style({ opacity: 0, transform: 'translateY(-10%)', offset: 0 }),
                style({ opacity: 0.5, transform: 'translateY(-10px)', offset: 0.5 }),
                style({ opacity: 1, transform: 'translateY(0)', offset: 1 })
              ])
            )
          ]),
          { optional: true }
        )
      ])
    ]),
    trigger('coverAnimation', [
      state(
        '*',
        style({
          opacity: 1
        })
      ),
      transition(
        'void => *',
        animate(
          '.2s ease-in',
          keyframes([
            style({ opacity: 0, filter: 'blur(25px)', offset: 0 }),
            style({ opacity: 1, filter: 'blur(0px)', offset: 1 })
          ])
        )
      )
    ])
  ]
})
export class ScrollerComponent implements OnInit, AfterViewInit, OnDestroy, AfterViewChecked {
  @Input() title = '';
  @Input() anime: Anime[];
  @Input() showProgress = false;
  _isMobileString: string;
  makeScroller: boolean;
  @Input() set isMobileString(isMobile: string) {
    this._isMobileString = isMobile;
    if (isMobile === 'desktop') {
      this.makeScroller = true;
    } else if (this.scroller) {
      this.scroller.destroy();
      this.scroller = null;
    }
  }

  @ViewChild('viewport', { static: false }) viewportRef: ElementRef<HTMLElement>;
  scroller: ScrollBooster;

  constructor(@Inject(PLATFORM_ID) private platformId: any) {}

  ngAfterViewChecked(): void {
    if (this.makeScroller && !this.scroller) {
      this.scroller = new ScrollBooster({
        viewport: this.viewportRef.nativeElement,
        content: this.viewportRef.nativeElement.firstElementChild as HTMLElement,
        scrollMode: 'transform',
        direction: 'horizontal',
        emulateScroll: true
      });
      this.makeScroller = false;
    }
  }
  ngOnDestroy(): void {
    if (this.scroller) {
      this.scroller.destroy();
      this.scroller = null;
    }
  }
  ngAfterViewInit(): void {
    if (isPlatformBrowser(this.platformId)) {
    }
  }

  ngOnInit(): void {}
}
