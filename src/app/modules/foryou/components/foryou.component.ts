import { Component, OnInit, Inject, PLATFORM_ID, AfterViewInit, OnDestroy } from '@angular/core';
import { LocalStateService } from 'src/app/services/localstate.service';
import { Observable, Subject, combineLatest } from 'rxjs';
import { Anime } from 'server/models/anime.interface';
import { platformBrowser } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
import { map, flatMap, filter, takeUntil, tap, reduce, shareReplay } from 'rxjs/operators';
import { QueryService } from '../../currentseason/services/query.service';
import { AnimeSorter } from 'src/app/services/animesorter';

@Component({
  selector: 'app-foryou',
  templateUrl: './foryou.component.html',
  styleUrls: ['./foryou.component.scss']
})
export class ForyouComponent implements OnInit, AfterViewInit, OnDestroy {
  continue$: Observable<Anime[]>;
  upComing$: Observable<Anime[]>;
  recommended$: Observable<Anime[]>;
  destroyed$ = new Subject();
  isMobileString: Observable<string>;
  noContent = false;

  constructor(
    private localStateService: LocalStateService,
    @Inject(PLATFORM_ID) private platformId: any,
    private queryService: QueryService
  ) {}
  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
  ngAfterViewInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.localStateService.manualReplay();
    }
  }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.isMobileString = this.localStateService.isSmallScreenString;

      this.continue$ = this.localStateService.currentlyWatchingAnime.pipe();

      this.upComing$ = this.localStateService.watchList.pipe(
        map(watchlist =>
          watchlist
            .filter(e => e.nextEpisode)
            .sort(
              (a, b) => a.nextEpisode.days * 24 + a.nextEpisode.hours - (b.nextEpisode.days * 24 + b.nextEpisode.hours)
            )
        )
      );

      this.recommended$ = this.localStateService.watchList.pipe(
        map(watchlist => watchlist.filter(anime => anime.recommendations)),
        map(watchlist => {
          const recommendationCountMap: { [mal_id: string]: number } = {};
          watchlist.forEach(anime =>
            anime.recommendations.forEach(recommendation => {
              if (recommendationCountMap[recommendation.mal_id])
                recommendationCountMap[recommendation.mal_id] += recommendation.recommendation_count;
              else recommendationCountMap[recommendation.mal_id] = recommendation.recommendation_count;
            })
          );

          return Object.keys(recommendationCountMap)
            .map(key => ({
              mal_id: parseInt(key, 10),
              count: recommendationCountMap[key]
            }))
            .filter(rec => !watchlist.map(a => a.malId).includes(rec.mal_id))
            .sort((a, b) => b.count - a.count)
            .slice(0, 50);
        }),
        flatMap(recommendations => {
          const malIds = recommendations.map(r => r.mal_id);
          return this.queryService
            .getAnime(QueryService.RecommendationQuery, {
              malId: malIds
            })
            .pipe(
              map(animeArray => {
                return AnimeSorter.sortBy(
                  animeArray.filter(
                    anime =>
                      anime.torrents.some(torrent => torrent.seeders > 10) ||
                      (!anime.torrents.length && new Date(anime.episodesUpdated).getTime() === 0)
                  ),
                  [{ key: 'malId', customOrder: malIds }]
                );
              })
            );
        })
      );

      combineLatest([this.continue$, this.upComing$, this.recommended$])
        .pipe(takeUntil(this.destroyed$))
        .subscribe(animes => {
          this.noContent = animes.every(anime => !anime.length);
        });
    }
  }
}
