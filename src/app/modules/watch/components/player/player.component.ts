import {
  Component,
  OnInit,
  Inject,
  PLATFORM_ID,
  Injector,
  ViewChild,
  ElementRef,
  HostListener,
  AfterViewInit,
  OnDestroy,
  Input,
} from '@angular/core';

import { isPlatformBrowser } from '@angular/common';
import { SocketService } from '../../services/socket/socket.service';
import { Router, ActivatedRoute } from '@angular/router';
import {
  takeLast,
  takeUntil,
  throttleTime,
  take,
  map,
  switchMap,
  switchMapTo,
  flatMap,
  withLatestFrom,
} from 'rxjs/operators';
import { ThemeService } from 'src/app/modules/shared/theme/theme.service';
import { fromEvent, Subject, Observable, combineLatest } from 'rxjs';
import { compile } from 'ass-compiler';
import { CustomVideoComponent } from '../custom-video/custom-video.component';
import { LocalStateService } from 'src/app/services/localstate.service';
import { Anime } from 'server/models/anime.interface';
import { Torrent } from 'server/models/torrent.interface';
import { delay } from 'lodash';
declare const ASS: any;
declare const ldBar: any;

interface FsDocument extends HTMLDocument {
  mozFullScreenElement?: Element;
  msFullscreenElement?: Element;
  webkitFullscreenElement?: Element;
  msExitFullscreen?: () => Promise<void>;
  mozCancelFullScreen?: () => Promise<void>;
  webkitExitFullscreen?: () => Promise<void>;
}

interface FsDocumentElement extends HTMLElement {
  msRequestFullscreen?: () => Promise<void>;
  mozRequestFullScreen?: () => Promise<void>;
  webkitRequestFullscreen?: () => Promise<void>;
}

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnInit, AfterViewInit, OnDestroy {
  constructor(
    private injector: Injector,
    @Inject(PLATFORM_ID) private platformId: any,
    private router: Router,
    private route: ActivatedRoute,
    private localState: LocalStateService,
    private themeService: ThemeService
  ) {}

  vlc: Observable<boolean>;
  upscale: Observable<boolean>;

  @Input() anime: Anime;
  @Input() episodeId: string;

  socketService: SocketService;
  streamUrl: string;
  subs: any;
  nextEpisode: Torrent;

  videoLoaded = false;
  preLoading = true;
  bar: any;
  fullscreen = false;
  destroyed$ = new Subject();
  languages: string[];
  selectedSubtitle: string;

  @ViewChild('container') containerRef: ElementRef;
  @ViewChild('video') videoRef: CustomVideoComponent;
  @ViewChild('loadingSvg') loadSvgRef: ElementRef;
  @ViewChild('loadingBar') ldBarRef: ElementRef;

  @HostListener('window:orientationchange', ['$event'])
  onOrientationChange(event) {
    if (screen.orientation.type.includes('landscape') && !this.isFullScreen()) {
      this.goFullscreen();
    } else {
      //  this.exitFullscreen();
    }
  }
  container = (): HTMLElement => this.containerRef.nativeElement;
  ldSvgElem = (): HTMLElement => this.loadSvgRef.nativeElement;
  ldBarElem = (): HTMLElement => this.ldBarRef.nativeElement;

  getMalSyncData() {
    const data = {
      page: 'episode',
      name: this.anime.title,
      episode: this.episodeId,
      mal_id: this.anime.malId,
      series_url: `https://justanime.app/anime/${this.anime.malId}`,
      ...(this.nextEpisode && {
        next_episode_url: `https://justanime.app/watch;id=${this.anime.malId};ep=${this.nextEpisode.episode}`,
      }),
    };
    return JSON.stringify(data);
  }

  ngOnDestroy(): void {
    this.socketService?.disconnect();
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  addFsClass() {
    this.fullscreen = true;
  }

  removeFsClass() {
    this.fullscreen = false;
  }

  toggleFullScreen(): void {
    if (!this.isFullScreen()) {
      this.goFullscreen();
    } else this.exitFullscreen();
  }

  goFullscreen() {
    const fsDocElem = this.container() as FsDocumentElement;
    if (fsDocElem.requestFullscreen) {
      fsDocElem.requestFullscreen().then(() => this.addFsClass());
    } else if (fsDocElem.msRequestFullscreen) {
      fsDocElem.msRequestFullscreen().then(() => this.addFsClass());
    } else if (fsDocElem.mozRequestFullScreen) {
      fsDocElem.mozRequestFullScreen().then(() => this.addFsClass());
    } else if (fsDocElem.webkitRequestFullscreen) {
      fsDocElem.webkitRequestFullscreen().then(() => this.addFsClass());
    }
  }

  exitFullscreen() {
    const fsDoc = document as FsDocument;
    if (fsDoc.exitFullscreen) {
      fsDoc.exitFullscreen().then(() => this.removeFsClass());
    } else if (fsDoc.msExitFullscreen) {
      fsDoc.msExitFullscreen().then(() => this.removeFsClass());
    } else if (fsDoc.mozCancelFullScreen) {
      fsDoc.mozCancelFullScreen().then(() => this.removeFsClass());
    } else if (fsDoc.webkitExitFullscreen) {
      fsDoc.webkitExitFullscreen().then(() => this.removeFsClass());
    }
  }

  isFullScreen(): boolean {
    const fsDoc = document as FsDocument;

    return !!(
      fsDoc.fullscreenElement ||
      fsDoc.mozFullScreenElement ||
      fsDoc.webkitFullscreenElement ||
      fsDoc.msFullscreenElement
    );
  }

  onResized() {
    if (this.subs) {
      this.videoRef.hideVideoEL = false;

      setTimeout(() => {
        this.subs.resize();
        this.videoRef.hideVideoEL = true;
      }, 100);
    }
  }

  loadedmetadata() {
    this.localState
      .getWatchProgress(this.anime.malId.toString())
      .pipe(take(1))
      .subscribe((watchProgress) => {
        if (watchProgress && watchProgress.episodes[this.episodeId])
          this.videoRef.videoEl.currentTime = watchProgress.episodes[this.episodeId].currentTime;

        this.videoRef.videoEl.play();
      });
  }

  loadeddata() {
    this.onResized();
    this.ldBarElem().remove();

    fromEvent(this.videoRef.getVideoEl(), 'timeupdate')
      .pipe(throttleTime(1000), takeUntil(this.socketService.subsInfo().pipe(takeLast(1))))
      .subscribe(() => {
        if (
          !this.subs ||
          this.subs.dialogues.length === 0 ||
          (this.videoRef.currentTime > this.subs.dialogues[this.subs.dialogues.length - 1].start - 2 &&
            this.videoRef.currentTime < this.videoRef.duration - 60)
        ) {
          this.socketService.reloadSubs();
        }
      });

    fromEvent(this.videoRef.videoEl, 'timeupdate')
      .pipe(takeUntil(this.destroyed$), throttleTime(5000))
      .subscribe((_) => {
        this.localState.setWatchProgress(this.anime, this.episodeId, {
          progress: {
            currentTime: this.videoRef.currentTime,
            duration: this.videoRef.duration,
            timestamp: new Date().getTime(),
            finished: this.videoRef.duration - this.videoRef.currentTime < 160,
          },
        });
      });
  }

  playNextEpisode() {
    this.router.navigate(['/watch', { id: this.anime.malId, ep: this.nextEpisode.episode }]);
  }

  async ngAfterViewInit(): Promise<void> {
    if (isPlatformBrowser(this.platformId)) {
      this.socketService
        .streamReady()
        .pipe(withLatestFrom(this.localState.vlc))
        .subscribe(([ready, vlc]) => {
          const videoUrl =
            window.location.protocol +
            '//' +
            window.location.host +
            `/watch/${this.anime.malId + this.episodeId}/` +
            this.socketService.getSocketId();
          if (!vlc) {
            this.streamUrl = videoUrl;
          } else {
            this.ldBarElem().remove();
            const i = document.createElement('iframe');
            i.style.display = 'none';
            i.onload = function () {
              i.parentNode.removeChild(i);
            };
            i.src = `vlc://${videoUrl}`;
            document.body.appendChild(i);
          }
        });

      this.socketService.subsInfo().subscribe((status) => {
        if (status !== 'unavailable') {
          this.loadSubtitle();
        }
      });
    }
  }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.upscale = this.localState.upscale;
      this.vlc = this.localState.vlc;
      this.localState
        .getSelectedSubtitle()
        .pipe(takeUntil(this.destroyed$))
        .subscribe((selected) => {
          this.selectedSubtitle = selected;
        });
      this.socketService = this.injector.get<SocketService>(SocketService);
      this.nextEpisode = this.anime.torrents.find((torrent) => torrent.episode > this.episodeId);

      this.socketService.getLanguages().subscribe((languages) => {
        console.log(languages);
        this.videoRef.setLanguages([...languages, 'none']);
      });
      this.socketService.startStream({
        torrentId: this.anime.malId.toString() + this.episodeId,
        malId: this.anime.malId.toString(),
        episode: this.episodeId,
      });

      this.bar = new ldBar('#loadingBar');
      this.socketService.getPercentage().subscribe((percentage) => {
        if (percentage !== 100) {
          this.preLoading = false;
          this.bar.set(percentage);
        }
      });
    }
  }

  async selectSubtitle(language: string) {
    await this.localState.selectSubtitle(language);
    this.loadSubtitle();
  }

  loadSubtitle() {
    if (this.selectedSubtitle === 'none') {
      if (this.subs?.dialogues) {
        this.subs.destroy();
        this.subs = null;
      }
    } else {
      fetch(this.streamUrl + '/subs/' + this.selectedSubtitle)
        .then((res) => res.text())
        .then((text) => {
          if (this.subs) {
            const { info, width, height, dialogues } = compile(text);
            this.subs.info = info;
            this.subs._.scriptRes = {
              width: width || this.videoRef.videoWidth,
              height: height || this.videoRef.videoHeight,
            };
            this.subs.dialogues = dialogues;
          } else {
            this.subs = new ASS(text, this.videoRef.getVideoEl(), {
              container: this.container(),
            });
          }
          this.onResized();
        });
    }
  }
}
