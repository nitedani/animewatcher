/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Component, OnInit, ViewChild, ElementRef, Output, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BehaviorSubject, EMPTY, interval, NEVER, Observable, Observer, Subject, Subscription } from 'rxjs';
import { exhaustMap, flatMap, switchMap, takeUntil, tap } from 'rxjs/operators';
const Anime4K = require('anime4k');
declare const WebGLImageFilter: any;
@Component({
  selector: 'app-custom-video',
  templateUrl: './custom-video.component.html',
  styleUrls: ['./custom-video.component.scss'],
})
export class CustomVideoComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('video', { static: false }) videoRef: ElementRef;
  videoEl: HTMLVideoElement;

  @ViewChild('canvas', { static: false }) canvasRef: ElementRef;
  @ViewChild('sharpened', { static: false }) sharpenedCanvasRef: ElementRef;

  @Input() src: string;
  @Input() upscale = true;
  @Input() hasNextEpisode = false;

  @Input() selectedSubtitle = 'eng';
  @Output() selectSubtitleEvent = new EventEmitter<string>();
  @Output() stateChange = new EventEmitter<boolean>();
  @Output() fullscreenChange = new EventEmitter<boolean>();
  @Output() loadeddataChange = new EventEmitter<Event>();
  @Output() loadedmetadataChange = new EventEmitter<Event>();
  @Output() timeupdateChange = new EventEmitter<Event>();
  @Output() playNextEpisode = new EventEmitter<Event>();

  upscalePaused = new BehaviorSubject<boolean>(false);
  destroyed = new Subject<boolean>();
  upscaler: Subscription = null;

  public hideVideoEL = false;

  languageMapping = {
    eng: 'English',
    ger: 'Deutsch',
    fre: 'Fran\u00e7ais (France)',
    ara: '\u0627\u0644\u0639\u0631\u0628\u064a\u0629',
    ita: 'Italiano',
    spa: 'Espa\u00f1ol (Espa\u00f1a)',
    spala: 'Espa\u00f1ol (Am\u00e9rica)',
    por: 'Portugu\u00eas (Brasil)',
    rus: '\u0420\u0443\u0441\u0441\u043a\u0438\u0439',
    pol: 'Polski',
    dut: 'Nederlands',
    nob: 'Bokmål',
    fin: 'Suomen kieli',
    tur: 'Türkçe',
    swe: 'Svenska',
    gre: 'Νέα Ελληνικά',
    heb: 'עברית',
    rum: 'Română',
    ind: 'Bahasa Indonesia',
    tha: 'ภาษาไทย',
    kor: '한국어',
    dan: 'Dansk',
    jpn: '日本語',
    chi: '中文',
    vie: 'Tiếng Việt',
    cze: 'Čeština',
    hun: 'Magyar',
    none: 'None',
  };

  subtitleMenuClick = false;
  playing = false;
  fullscreen = false;
  currentTimeStr = '00:00 / 00:00';
  barWidth = '0%';
  clicked = false;
  controlsVisible = true;
  timeout = null;
  autoplay = false;
  languages: { short: string; display: string }[] = [{ short: 'none', display: 'None' }];
  longestLanguageLength = 6;
  constructor() {}

  setAutoplay(event: MouseEvent) {
    event.stopPropagation();
    this.autoplay =
      ((event.target as HTMLElement).children[0] as HTMLInputElement).getAttribute('aria-checked') === 'true'
        ? false
        : true;
    console.log(this.autoplay);
  }

  public setLanguages(languages: string[]) {
    this.languages = languages.map((language) => ({ short: language, display: this.languageMapping[language] }));
    this.longestLanguageLength = Math.max(...this.languages.map((language) => language.display.length));
  }

  get videoWidth() {
    return this.upscale ? (this.canvasRef.nativeElement as HTMLCanvasElement).width : this.videoEl.videoWidth;
  }

  get videoHeight() {
    console.log((this.canvasRef.nativeElement as HTMLCanvasElement).height);
    return this.upscale ? (this.canvasRef.nativeElement as HTMLCanvasElement).height : this.videoEl.videoHeight;
  }

  getVideoEl(): HTMLVideoElement {
    return this.videoEl;
  }

  get duration(): number {
    return this.videoEl.duration;
  }

  get currentTime(): number {
    return this.videoEl.currentTime;
  }
  ngAfterViewInit(): void {
    this.videoEl = this.videoRef.nativeElement;
    this.videoEl.controls = false;
    this.videoEl.addEventListener('wheel', (e) => {
      e.preventDefault();
      const delta = Math.sign(e.deltaY);
      if (delta === -1 && this.videoEl.volume <= 0.91) {
        this.changeVolume(this.videoEl.volume + 0.1);
      }
      if (delta === 1 && this.videoEl.volume > 0.09) {
        this.changeVolume(this.videoEl.volume - 0.1);
      }
    });
    this.canvasRef.nativeElement.addEventListener('wheel', (e) => {
      e.preventDefault();
      const delta = Math.sign(e.deltaY);
      if (delta === -1 && this.videoEl.volume <= 0.91) {
        this.changeVolume(this.videoEl.volume + 0.1);
      }
      if (delta === 1 && this.videoEl.volume > 0.09) {
        this.changeVolume(this.videoEl.volume - 0.1);
      }
    });
  }

  mouseMoved() {
    this.controlsVisible = true;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.controlsVisible = false;
    }, 3000);
  }

  videoClick() {
    if (this.clicked) {
      this.fullscreenChange.next(true);
    }

    this.clicked = true;
    setTimeout(() => {
      this.clicked = false;
    }, 500);
  }

  openControls(event: MouseEvent) {
    event.stopPropagation();
    clearTimeout(this.timeout);
    this.controlsVisible = true;
  }

  closeControls() {
    this.timeout = setTimeout(() => {
      this.controlsVisible = false;
    }, 1000);
  }

  ngOnInit(): void {}

  cumulativeOffset(element) {
    let top = 0;
    let left = 0;
    do {
      top += element.offsetTop || 0;
      left += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);

    return {
      top,
      left,
    };
  }

  changeFullscreen() {
    if (this.fullscreen) {
      this.fullscreenChange.emit(false);
    } else {
      this.fullscreenChange.emit(true);
    }
  }

  setPlaying() {
    this.playing = true;
    this.timeout = setTimeout(() => {
      this.controlsVisible = false;
    }, 3000);

    if (this.upscale && !this.upscaler) {
      const scaler = Anime4K.Scaler((this.canvasRef.nativeElement as HTMLCanvasElement).getContext('webgl'));
      scaler.inputVideo(this.videoEl);
      scaler.resize(2.0, { bold: 1.2 });

      const filter = new WebGLImageFilter({ canvas: this.sharpenedCanvasRef.nativeElement });
      filter.addFilter('sharpen', 0.6);
      console.log(this.canvasRef.nativeElement.width);

      this.upscaler = this.pausableInterval(this.upscalePaused)
        .pipe(
          takeUntil(this.destroyed),
          exhaustMap((_) => {
            scaler.render();
            filter.apply(this.canvasRef.nativeElement);
            return EMPTY;
          })
        )
        .subscribe();
    }
  }

  pausableInterval(paused: Observable<boolean>): Observable<number> {
    return new Observable((obs: Observer<number>) => {
      let i = 0;
      const ticker = interval(1000 / 48).pipe(tap(() => i++));

      const p = paused.pipe(switchMap((paused) => (paused ? NEVER : ticker)));
      return p.subscribe(
        (val) => obs.next(val),
        (err) => obs.error(err),
        () => obs.complete()
      );
    });
  }
  seek(e: MouseEvent) {
    const pos = e.offsetX / (e.target as HTMLElement).offsetWidth;
    this.videoEl.currentTime = pos * this.videoEl.duration;
  }

  playOrPause() {
    if (this.videoEl.paused) {
      this.videoEl.play();
      this.playing = true;
      this.stateChange.emit(true);
      this.upscalePaused.next(false);
    } else {
      this.videoEl.pause();
      this.playing = false;
      this.stateChange.emit(false);
      this.upscalePaused.next(true);
    }
  }

  onloadeddata(event: Event) {
    this.loadeddataChange.emit(event);
  }
  onloadedmetadata(event: Event) {
    this.loadedmetadataChange.emit(event);
  }
  timeupdate() {
    const position = this.videoEl.currentTime / this.videoEl.duration;

    this.barWidth = position * 100 + '%';

    this.convertTime(Math.round(this.videoEl.currentTime));

    if (this.videoEl.ended) {
      this.playing = false;
      this.stateChange.emit(false);
    }
  }

  convertTime(seconds) {
    let min = Math.floor(seconds / 60) as any;
    let sec = (seconds % 60) as any;
    min = min < 10 ? '0' + min : min;
    sec = sec < 10 ? '0' + sec : sec;
    this.currentTimeStr = min + ':' + sec;

    this.totalTime(Math.round(this.videoEl.duration));
  }

  totalTime(seconds) {
    let min = Math.floor(seconds / 60) as any;
    let sec = (seconds % 60) as any;

    min = min < 10 ? '0' + min : min;
    sec = sec < 10 ? '0' + sec : sec;
    this.currentTimeStr += ' / ' + min + ':' + sec;
  }

  changeVolume(value) {
    this.videoEl.volume = value;
    if (value <= 0.01) {
      this.videoEl.muted = true;
    } else {
      this.videoEl.muted = false;
    }
  }

  mute() {
    if (!this.videoEl.muted) {
      this.videoEl.muted = true;
    } else {
      this.videoEl.muted = false;
    }
  }

  onEnded() {
    if (this.autoplay && this.hasNextEpisode) this.playNextEpisode.emit();
  }

  selectSubtitle(language: string) {
    this.subtitleMenuClick = false;
    this.selectedSubtitle = language;
    this.selectSubtitleEvent.emit(language);
  }

  openSubtitleMenu(event: MouseEvent, state: boolean) {
    event.stopPropagation();
    this.subtitleMenuClick = state;
  }
  ngOnDestroy(): void {
    this.destroyed.next(true);
    this.destroyed.complete();
  }
}
