import {
  Component,
  OnInit,
  Inject,
  Injector,
  PLATFORM_ID,
  OnDestroy,
  AfterViewInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import { SocketService } from '../../services/socket/socket.service';
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStateService } from 'src/app/services/localstate.service';
import { Anime } from 'server/models/anime.interface';
import { QueryService } from 'src/app/modules/currentseason/services/query.service';
import { map, throttleTime, filter } from 'rxjs/operators';
import { Observable, fromEvent } from 'rxjs';
import { ThemeService } from 'src/app/modules/shared/theme/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit, OnDestroy, AfterViewInit {
  private socketService: SocketService;
  streamReady = false;
  streamUrl = '';
  anime$: Observable<Anime>;
  episodeId: string;
  timeout = null;
  headerVisible = true;
  malId: string;

  @ViewChild('page', { static: true }) page: ElementRef;

  constructor(
    private injector: Injector,
    @Inject(PLATFORM_ID) private platformId: any,
    private route: ActivatedRoute,
    private router: Router,
    private localState: LocalStateService,
    private queryService: QueryService,
    private themeService: ThemeService
  ) {}

  goBack() {
    this.router.navigate(['/anime', this.malId]);
  }

  showNavbar() {
    this.themeService.navbarHidden.next(false);
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.themeService.navbarHidden.next(true);
    }, 1000);
  }
  ngAfterViewInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.localState.manualReplay();
      setTimeout(() => this.themeService.navbarHidden.next(true));
    }
  }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.timeout = setTimeout(() => {
        this.headerVisible = false;
      }, 3000);

      fromEvent(this.page.nativeElement, 'mousemove').subscribe(() => {
        this.headerVisible = true;
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
          this.headerVisible = false;
        }, 3000);
      });

      this.socketService = this.injector.get<SocketService>(SocketService);
      this.route.paramMap.subscribe(params => {
        this.malId = params.get('id');
        this.episodeId = params.get('ep');

        if (!this.episodeId) this.router.navigate(['/anime', this.malId]);
        this.anime$ = this.queryService
          .getAnime(QueryService.AnimeSeasonQuery, { malId: parseInt(this.malId, 10) })
          .pipe(
            map(anime => anime[0]),
            filter(anime => anime.torrents.some(torrent => torrent.episode === this.episodeId))
          );
      });
    }
  }

  ngOnDestroy(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.socketService.disconnect();
      clearTimeout(this.timeout);
      this.themeService.navbarHidden.next(false);
    }
  }
}
