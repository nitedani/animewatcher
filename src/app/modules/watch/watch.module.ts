import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { RootComponent } from './components/root/root.component';
import { SocketService } from './services/socket/socket.service';
import { PlayerComponent } from './components/player/player.component';
import { AngularResizedEventModule } from 'angular-resize-event';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MySocket } from '../shared/socket/socket.service';
import { CustomVideoComponent } from './components/custom-video/custom-video.component';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: RootComponent, pathMatch: 'full' }]),
    AngularResizedEventModule,
    MatProgressBarModule,
    MaterialModule
  ],
  providers: [MySocket, SocketService],
  declarations: [RootComponent, PlayerComponent, CustomVideoComponent]
})
export class WatchModule {}
