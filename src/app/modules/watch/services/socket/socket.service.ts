import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { map, takeWhile, tap, takeUntil, switchMap, take, distinctUntilChanged, filter } from 'rxjs/operators';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { MySocket } from 'src/app/modules/shared/socket/socket.service';

@Injectable()
export class SocketService {
  private _percentage = new Subject<number>();
  private _stopped = new Subject<boolean>();
  private _streamready = new BehaviorSubject<boolean>(false);
  private socket: Socket;
  constructor(private mySocket: MySocket) {
    this.socket = mySocket.socket;
    this.socket
      .fromEvent<any>('percentage')
      .pipe(
        takeWhile(data => data <= 100),
        tap(data => {
          this._percentage.next(parseInt(data, 10));
        })
      )
      .subscribe();
  }

  getSocketId(): string {
    return this.socket.ioSocket.id;
  }

  sendMessage(event: string, msg: string) {
    this.socket.emit(event, msg);
  }

  reloadSubs() {
    this.socket.emit('reloadsubs');
  }

  startStream(opts: {torrentId: string, malId:string, episode:string}) {
    if (this.socket.ioSocket.disconnected) this.socket.connect();
    this.socket.emit('startstream', opts);
  }

  subsInfo(): Observable<string> {
    return this.socket.fromEvent<any>('subtitles').pipe(
      map(data => JSON.parse(data)),
      takeWhile(status => status !== 'complete', true),
      takeUntil(this._stopped)
    );
  }

  getLanguages(): Observable<string[]> {
    return this.socket.fromEvent<any>('languages').pipe(
      tap(console.log),
      map(data => JSON.parse(data)),
      take(1),
      takeUntil(this._stopped)
    );
  }


  getPercentage(): Observable<number> {
    return this._percentage.pipe(
      tap(percentage => {
        if (percentage >= 10) {
          this._streamready.next(true);
        }
      }),
      takeWhile(percentage => percentage <= 100),
      takeUntil(this._stopped)
    );
  }

  streamReady(): Observable<boolean> {
    return this._streamready.pipe(
      distinctUntilChanged(),
      filter(status => status === true),
      take(1)
    );
  }

  disconnect() {
    this._streamready.next(false);
    this._stopped.next(true);
    this._percentage.next(0);
    this.socket.disconnect();
  }

  connect() {
    this.socket.connect();
  }

  stopStream() {
    this.socket.emit('stopstream');
    this._streamready.next(false);
    this._stopped.next(true);
    this._percentage.next(0);
  }
}
