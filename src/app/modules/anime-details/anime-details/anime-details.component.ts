import { Component, OnInit, PLATFORM_ID, Inject, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Anime } from 'server/models/anime.interface';
import { Torrent } from 'server/models/torrent.interface';
import { DomSanitizer } from '@angular/platform-browser';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import { Observable, BehaviorSubject, merge, Subject } from 'rxjs';
import { map, flatMap, filter, take, tap, takeUntil } from 'rxjs/operators';
import { LocalStateService, WatchProgress } from 'src/app/services/localstate.service';
import { subscribe } from 'graphql';
import { trigger, state, style, animate, transition, query, stagger, keyframes } from '@angular/animations';
import { isPlatformBrowser } from '@angular/common';
import { QueryService } from '../../currentseason/services/query.service';
import { AnimeSorter } from 'src/app/services/animesorter';
import { AnimationEvent } from '@angular/animations';
import { ResizedEvent } from 'angular-resize-event';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-anime-details',
  templateUrl: './anime-details.component.html',
  styleUrls: ['./anime-details.component.scss'],
  animations: [
    trigger('episodesAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),
        query(
          ':enter',
          stagger('20ms', [
            animate(
              '.2s ease-in',
              keyframes([
                style({ opacity: 0, transform: 'translateY(-50%)', offset: 0 }),
                style({ opacity: 0.5, transform: 'translateY(-10px) scale(1.1)', offset: 0.3 }),
                style({ opacity: 1, transform: 'translateY(0)', offset: 1 }),
              ])
            ),
          ]),
          { optional: true }
        ),
      ]),
    ]),
    trigger('coverAnimation', [
      state(
        '*',
        style({
          opacity: 1,
        })
      ),
      transition(
        'void => *',
        animate('.2s ease-in', keyframes([style({ opacity: 0, offset: 0 }), style({ opacity: 1, offset: 1 })]))
      ),
    ]),
    trigger('bgmaskAnimation', [
      state(
        '*',
        style({
          opacity: 0,
        })
      ),
      state(
        'void',
        style({
          opacity: 1,
        })
      ),
      transition('* => *', animate('3s ease-out')),
    ]),
    trigger('ratingAnimation', [
      state(
        'void',
        style({
          width: '0%',
          opacity: 0,
        })
      ),
      state(
        '*',
        style({
          width: '{{score}}',
          opacity: 1,
        }),
        {
          params: {
            score: '0%',
          },
        }
      ),
      transition('void <=> *', animate(250)),
    ]),
    trigger('recommendationsAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),
        query(
          ':enter',
          stagger('50ms', [
            animate(
              '.4s ease-out',
              keyframes([
                style({ opacity: 0, transform: 'translateY(-10%)', offset: 0 }),
                style({ opacity: 0.5, transform: 'translateY(-10px)', offset: 0.5 }),
                style({ opacity: 1, transform: 'translateY(0)', offset: 1 }),
              ])
            ),
          ]),
          { optional: true }
        ),
      ]),
    ]),
  ],
})
export class AnimeDetailsComponent implements OnInit {
  constructor(
    @Inject(PLATFORM_ID) private platformId: any,
    private route: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer,
    private localStateService: LocalStateService,
    private queryService: QueryService,
    private cd: ChangeDetectorRef
  ) {}

  vlc: boolean;
  upscale: boolean;
  anime: Anime;
  inWatchlist$: Observable<boolean>;
  animateCard = {};
  expandedAbout = false;
  expandedSimilar = false;
  recommendations: Anime[] = [];
  sequels: Anime[] = [];
  prequels: Anime[] = [];
  notFoundEpisodes = false;
  navigatedAway$: Subject<boolean> = new Subject<boolean>();
  aboutContentHeight$ = new Subject<number>();
  isMobileString: Observable<string>;
  noRecommendations = false;
  watchProgress$: Observable<{ episodes: { [episodeId: string]: WatchProgress }; finished?: boolean }>;

  nSizeArray(n: number) {
    return Array(n);
  }

  getMalSyncData() {
    const data = {
      page: 'anime',
      name: this.anime.title,
      mal_id: this.anime.malId,
      series_url: `https://justanime.app/anime/${this.anime.malId}`,
      selector_position: '#syncData',
    };
    return JSON.stringify(data);
  }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.localStateService.upscale
        .pipe(takeUntil(this.navigatedAway$))
        .subscribe((upscale) => (this.upscale = upscale));
      this.localStateService.vlc.pipe(takeUntil(this.navigatedAway$)).subscribe((vlc) => (this.vlc = vlc));
      this.isMobileString = this.localStateService.isSmallScreenString;
      this.route.paramMap.subscribe((params) => {
        this.noRecommendations = false;
        this.notFoundEpisodes = false;
        this.recommendations = [];
        this.sequels = [];
        this.prequels = [];
        const malId = parseInt(params.get('id'), 10);
        this.queryService
          .getAnime(QueryService.AnimeDetailsQuery, { malId })
          .pipe(takeUntil(this.navigatedAway$))
          .subscribe((anime) => {
            this.anime = anime[0];
            this.watchProgress$ = this.localStateService.getWatchProgress(this.anime.malId.toString());
            this.localStateService.manualReplay();
            this.queryService
              .getAnime(QueryService.AnimeDetailsQuery, { malId, fetchDetails: 'fetch', fetchRecommendations: 'fetch' })
              .pipe(takeUntil(this.navigatedAway$))
              .subscribe((animeWithRecommendations) => {
                const { torrents, ...rest } = animeWithRecommendations[0];
                Object.assign(this.anime, rest);

                if (this.anime.related?.Sequel) {
                  this.queryService
                    .getAnime(QueryService.RecommendationQuery, {
                      malId: this.anime.related.Sequel.map((seq) => seq.mal_id),
                    })
                    .pipe(takeUntil(this.navigatedAway$))
                    .subscribe((sequels) => (this.sequels = sequels));
                }

                if (this.anime.related?.Prequel) {
                  this.queryService
                    .getAnime(QueryService.RecommendationQuery, {
                      malId: this.anime.related.Prequel.map((pre) => pre.mal_id),
                    })
                    .pipe(takeUntil(this.navigatedAway$))
                    .subscribe((prequels) => (this.prequels = prequels));
                }

                if (this.anime.recommendations.length) {
                  this.anime.recommendations.forEach((r) => (this.animateCard[r.mal_id] = 'stop'));
                  const recommendationIds = this.anime.recommendations
                    .sort((a, b) => b.recommendation_count - a.recommendation_count)
                    .map((r) => r.mal_id);
                  this.queryService
                    .getAnime(QueryService.RecommendationQuery, {
                      malId: recommendationIds,
                    })
                    .pipe(takeUntil(this.navigatedAway$))
                    .subscribe((recommendationsResponse) => {
                      if (!recommendationsResponse.length) {
                        this.noRecommendations = true;
                        return false;
                      }
                      this.recommendations = AnimeSorter.sortBy(recommendationsResponse, [
                        { key: 'malId', customOrder: recommendationIds },
                      ]);
                    });
                } else {
                  this.noRecommendations = true;
                }
              });

            if (!this.anime.torrents.length)
              this.queryService
                .getAnime(QueryService.AnimeDetailsQuery, {
                  malId,
                  fetchNewEpisodes: 'fetch',
                })
                .pipe(takeUntil(this.navigatedAway$))
                .subscribe((animeWithNewEps) => {
                  this.anime.torrents = animeWithNewEps[0].torrents;
                  if (!this.anime.torrents.length && !this.anime.nextEpisode) this.notFoundEpisodes = true;
                });

            this.inWatchlist$ = this.localStateService.isInWatchlist(this.anime.malId);
          });
      });
    }
  }

  getStar() {
    return this.anime.score * 10 + '%';
  }

  watchListClick() {
    this.localStateService.changeWatchList(this.anime.malId);
  }

  expandAbout() {
    this.expandedAbout = !this.expandedAbout;
  }

  expandSimilar() {
    this.expandedSimilar = !this.expandedSimilar;
  }
  openDetails(malId: number) {
    this.navigatedAway$.next(true);
    this.router.navigate(['/anime', malId]);
  }

  vlcToggled(event: MatSlideToggleChange) {
    this.localStateService.toggleVlc(event.checked);
    if (event.checked && !this.fontAvailable('Habibi')) {
      const fileUrl = window.location.protocol + '//' + window.location.host + '/assets/vlc-protocol.bat';
      this.downLoadFile(fileUrl);
    }
  }

  upscaleToggled(event: MatSlideToggleChange) {
    this.localStateService.toggleUpscale(event.checked);
  }

  downLoadFile(path: string) {
    const a = document.createElement('a') as HTMLAnchorElement;
    a.href = path;
    a.download = path.substr(path.lastIndexOf('/') + 1);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  fontAvailable(font: string) {
    const body = document.body;

    const container = document.createElement('span');
    container.innerHTML = Array(100).join('wi');
    container.style.cssText = ['position:absolute', 'width:auto', 'font-size:128px', 'left:-99999px'].join(
      ' !important;'
    );

    const getWidth = (fontFamily) => {
      container.style.fontFamily = fontFamily;

      body.appendChild(container);
      const width = container.clientWidth;
      body.removeChild(container);

      return width;
    };

    // Pre compute the widths of monospace, serif & sans-serif
    // to improve performance.
    const monoWidth = getWidth('monospace');
    const serifWidth = getWidth('serif');
    const sansWidth = getWidth('sans-serif');

    return (
      monoWidth !== getWidth(font + ',monospace') ||
      sansWidth !== getWidth(font + ',sans-serif') ||
      serifWidth !== getWidth(font + ',serif')
    );
  }

  startStream(torrent: Torrent) {
    this.router.navigate(['/watch', { id: this.anime.malId, ep: torrent.episode }]);
  }

  getEpisodeLink(torrent: Torrent) {
    return `https://justanime.app/watch;id=${this.anime.malId};ep=${torrent.episode}`;
  }

  getImageUrl(url: string, large = false) {
    //return this.sanitizer.bypassSecurityTrustStyle(`url(/assets/Wave-10s-1323px.svg)`);
    return this.sanitizer.bypassSecurityTrustStyle(`url(${large ? url.replace('.jpg', 'l.jpg') : url})`);
  }

  openMalPage(url) {
    window.open(url, '_blank');
  }
}
