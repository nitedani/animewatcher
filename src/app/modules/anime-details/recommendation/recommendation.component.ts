import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  Inject,
  PLATFORM_ID,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Anime } from 'server/models/anime.interface';
import { DomSanitizer } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.scss']
})
export class RecommendationComponent implements OnInit {
  @Input() anime: Anime;
  @Input() noCover = false;
  @Output() clicked = new EventEmitter();

  constructor(private sanitizer: DomSanitizer, @Inject(PLATFORM_ID) private platformId: any) {}

  ngOnInit(): void {}

  getImageUrl(url: string, large: boolean = false) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${large ? url.replace('.jpg', 'l.jpg') : url})`);
  }

  prepareDetails() {
    this.clicked.emit('click');
  }
}
