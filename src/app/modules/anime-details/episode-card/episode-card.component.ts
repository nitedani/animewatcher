import { Component, OnInit, Input, Inject, PLATFORM_ID, ViewChild, ElementRef } from '@angular/core';
import { Anime } from 'server/models/anime.interface';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { WatchProgress, LocalStateService } from 'src/app/services/localstate.service';
import { isPlatformBrowser } from '@angular/common';
import { Observable } from 'rxjs';
import { tap, map, filter, take } from 'rxjs/operators';
import { MatIcon } from '@angular/material/icon';
@Component({
  selector: 'app-episode-card',
  templateUrl: './episode-card.component.html',
  styleUrls: ['./episode-card.component.scss']
})
export class EpisodeCardComponent implements OnInit {
  @Input() anime: Anime;
  @Input() noCover = false;
  @Input() showProgress = false;
  watchProgress: Observable<WatchProgress>;

  mousePos = { x: 0, y: 0 };

  @ViewChild('continueicon', { static: false }) continueIcon: MatIcon;

  constructor(
    private sanitizer: DomSanitizer,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: any,
    private localState: LocalStateService
  ) {}

  nextEpisodeNumber() {
    return parseInt(this.anime.torrents[this.anime.torrents.length - 1].episode, 10) + 1;
  }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.watchProgress = this.localState.getWatchProgress(this.anime.malId.toString()).pipe(
        filter(_ => !!_),
        map(
          watchProgress => {
            const lastFinished = Object.values(watchProgress.episodes)
              .sort((a, b) => b.timestamp - a.timestamp)
              .find(progress => progress.finished);
            const nextEpisode =
              lastFinished && this.anime.torrents.find(torrent => torrent.episode > lastFinished.episodeId);
            return nextEpisode && !watchProgress[nextEpisode.episode]
              ? {
                  episodeId: nextEpisode.episode,
                  duration: -1,
                  currentTime: 0,
                  finished: false,
                  timestamp: -1
                }
              : Object.values(watchProgress.episodes)
                  .sort((a, b) => b.timestamp - a.timestamp)
                  .find(progress => !progress.finished) || {
                  episodeId: this.anime.torrents.find(torrent => !Object.keys(watchProgress).includes(torrent.episode))
                    .episode,
                  duration: -1,
                  currentTime: 0,
                  finished: false,
                  timestamp: -1
                };
          }
          /*
            this.anime.torrents
              .sort((a, b) => watchProgress[b.episode].timestamp - watchProgress[a.episode].timestamp)
              .reduce((ret, curr) => (watchProgress[ret.episode]?.finished ? curr : ret))
            */
        )
      );
    }
  }

  mouseDown(event: MouseEvent) {
    this.mousePos.x = event.screenX;
    this.mousePos.y = event.screenY;
  }

  mouseUp(event: MouseEvent, episodeId?: string) {
    const distance = Math.sqrt(
      Math.pow(event.screenX - this.mousePos.x, 2) + Math.pow(event.screenY - this.mousePos.y, 2)
    );

    if (distance < 20) {
      event.stopPropagation();
      if (event.target === this.continueIcon?._elementRef.nativeElement) {
        this.watchProgress
          .pipe(take(1))
          .subscribe(progress => this.router.navigate(['/watch', { id: this.anime.malId, ep: progress.episodeId }]));
      } else {
        this.openDetails();
      }
    }
  }

  openDetails() {
    this.router.navigate(['/anime', this.anime.malId]);
  }
  getImageUrl(url: string, large: boolean = false) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${large ? url.replace('.jpg', 'l.jpg') : url})`);
  }
}
