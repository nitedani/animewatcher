import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimeDetailsComponent } from './anime-details/anime-details.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material.module';
import { RatingModule } from 'ng-starrating';
import { RecommendationComponent } from './recommendation/recommendation.component';
import { AngularResizedEventModule } from 'angular-resize-event';
import { ScrollerComponent } from './scroller/scroller.component';
import { EpisodeCardComponent } from './episode-card/episode-card.component';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [AnimeDetailsComponent, RecommendationComponent, ScrollerComponent, EpisodeCardComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{ path: '', component: AnimeDetailsComponent, pathMatch: 'full' }]),
    MaterialModule,
    VirtualScrollerModule,
    FormsModule,
    AngularResizedEventModule
  ]
})
export class AnimeDetailsModule {}
