import {
  Component,
  OnInit,
  PLATFORM_ID,
  Inject,
  OnDestroy,
  AfterViewInit,
  DoCheck,
  AfterViewChecked
} from '@angular/core';
import { Anime } from 'server/models/anime.interface';
import { LocalStateService } from 'src/app/services/localstate.service';
import { isPlatformBrowser } from '@angular/common';
import { Observable, Subscription, Subject, BehaviorSubject, combineLatest } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import { AnimeSorter } from 'src/app/services/animesorter';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.scss'],
  animations: [
    trigger('coverAnimation', [
      state(
        '*',
        style({
          opacity: 1
        })
      ),
      transition(
        'void => *',
        animate(
          '.2s ease-in',
          keyframes([
            style({ opacity: 0, filter: 'blur(25px)', offset: 0 }),
            style({ opacity: 1, filter: 'blur(0px)', offset: 1 })
          ])
        )
      )
    ])
  ]
})
export class WatchlistComponent implements OnInit, OnDestroy, AfterViewInit {
  $destroyed: Subject<boolean> = new Subject();
  anime$: Observable<{ season: string; anime: Anime[] }[]>;
  sorter = AnimeSorter;
  value = '';
  search: BehaviorSubject<string> = new BehaviorSubject('');
  isSmallScreenString: Observable<string>;

  constructor(private localStateService: LocalStateService, @Inject(PLATFORM_ID) private platformId: any) {}

  ngAfterViewInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.localStateService.manualReplay();
    }
  }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.isSmallScreenString = this.localStateService.isSmallScreenString;
      this.anime$ = combineLatest([this.localStateService.watchList, this.search]).pipe(
        map(([anime, search]) => {
          let seasons = [...new Set(anime.map(a => a.season))].map(season => ({
            value: season,
            viewValue: season.substr(0, 4) + ' ' + season.substr(4).toUpperCase(),
            year: season.substr(0, 4),
            season: season.substr(4).toUpperCase()
          }));

          seasons = this.sorter.sortBy(seasons, [
            { key: 'year', dir: 'desc' },
            { key: 'season', customOrder: ['WINTER', 'SPRING', 'SUMMER', 'FALL'], dir: 'desc' }
          ]);

          return seasons.map(season => ({
            season: season.viewValue,
            anime: anime.filter(a => a.season === season.value && new RegExp(search, 'i').test(a.title))
          }));
        })
      );
    }
  }

  ngOnDestroy(): void {
    this.$destroyed.next(true);
  }

  clearSearch = () => {
    this.value = '';
    this.search.next('');
  };
}
