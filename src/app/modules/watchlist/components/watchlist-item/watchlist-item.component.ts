import { Component, OnInit, Input } from '@angular/core';
import { Anime } from 'server/models/anime.interface';
import { Torrent } from 'server/models/torrent.interface';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { LocalStateService } from 'src/app/services/localstate.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { takeUntil } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-watchlist-item',
  templateUrl: './watchlist-item.component.html',
  styleUrls: ['./watchlist-item.component.css']
})
export class WatchlistItemComponent implements OnInit {
  @Input() anime: Anime;
  inWatchList$: Observable<boolean>;
  mousePos = { x: 0, y: 0, target: null };
  constructor(
    private router: Router,
    private sanitizer: DomSanitizer,
    private localStateService: LocalStateService,
    private snackBar: MatSnackBar
  ) {}

  stopEvent(event: MouseEvent) {
    event.stopPropagation();
  }
  mouseDown(event: MouseEvent) {
    this.mousePos.x = event.screenX;
    this.mousePos.y = event.screenY;
    this.mousePos.target = event.target;
  }

  mouseUp(event: MouseEvent) {
    if (event.target === this.mousePos.target) {
      const distance = Math.sqrt(
        Math.pow(event.screenX - this.mousePos.x, 2) + Math.pow(event.screenY - this.mousePos.y, 2)
      );

      if (distance < 20) this.openDetails();
    }
  }
  watchListClick(event: MouseEvent) {
    event.stopPropagation();
    this.localStateService.removeFromWatchlist(this.anime.malId);
    const button = this.snackBar.open('Removed from liked anime', 'Undo', {
      duration: 3 * 1000,
      panelClass: ['snack-bar-color']
    });
    button
      .onAction()
      .pipe(takeUntil(button.afterDismissed()))
      .subscribe(_ => this.localStateService.addToWatchlist(this.anime.malId));
  }

  openDetails() {
    this.router.navigate(['/anime', this.anime.malId]);
  }

  startStream(torrent: Torrent) {
    this.router.navigate(['/watch', torrent.uniqueID]);
  }
  getImageUrl(url: string, large: boolean = false) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${large ? url.replace('.jpg', 'l.jpg') : url})`);
  }

  openMalPage() {
    window.open(this.anime.url, '_blank');
  }

  ngOnInit(): void {
    this.inWatchList$ = this.localStateService.isInWatchlist(this.anime.malId);
  }
}
