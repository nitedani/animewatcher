import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit,
  Inject,
  PLATFORM_ID,
  OnDestroy,
  AfterViewChecked
} from '@angular/core';
import { Anime } from 'server/models/anime.interface';
import { isPlatformBrowser } from '@angular/common';
import ScrollBooster from 'scrollbooster';
import { trigger, state, style, transition, animate, keyframes, query, stagger } from '@angular/animations';

@Component({
  selector: 'app-scroller',
  templateUrl: './scroller.component.html',
  styleUrls: ['./scroller.component.scss'],
  animations: [
    trigger('coverAnimation', [
      state(
        '*',
        style({
          opacity: 1
        })
      ),
      transition(
        'void => *',
        animate(
          '.2s ease-in',
          keyframes([
            style({ opacity: 0, filter: 'blur(25px)', offset: 0 }),
            style({ opacity: 1, filter: 'blur(0px)', offset: 1 })
          ])
        )
      )
    ]),
    trigger('cardAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),
        query(
          ':enter',
          stagger('20ms', [
            animate(
              '.4s ease-out',
              keyframes([
                style({ opacity: 0, transform: 'translateY(-10%)', offset: 0 }),
                style({ opacity: 0.5, transform: 'translateY(-10px)', offset: 0.5 }),
                style({ opacity: 1, transform: 'translateY(0)', offset: 1 })
              ])
            )
          ]),
          { optional: true }
        )
      ])
    ])
  ]
})
export class ScrollerComponent implements OnInit, OnDestroy, AfterViewChecked {
  @Input() seasonalAnime: { season: string; anime: Anime[] };
  @Input() set isMobileString(isMobile: string) {
    this._isMobileString = isMobile;
    if (isMobile === 'desktop') {
      this.makeScroller = true;
    } else if (this.scroller) {
      this.scroller.destroy();
      this.scroller = null;
    }
  }
  @ViewChild('viewport', { static: false }) viewportRef: ElementRef<HTMLElement>;
  scroller: ScrollBooster;
  _isMobileString: string;
  makeScroller: boolean;
  constructor(@Inject(PLATFORM_ID) private platformId: any) {}
  ngOnDestroy(): void {
    if (this.scroller) {
      this.scroller.destroy();
      this.scroller = null;
    }
  }
  ngAfterViewChecked(): void {
    if (this.makeScroller && !this.scroller) {
      this.scroller = new ScrollBooster({
        viewport: this.viewportRef.nativeElement,
        content: this.viewportRef.nativeElement.firstElementChild as HTMLElement,
        scrollMode: 'transform',
        direction: 'horizontal',
        emulateScroll: true
      });
      this.makeScroller = false;
    }
  }
  ngOnInit(): void {}
}
