import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WatchlistComponent } from './components/watchlist/watchlist.component';
import { WatchlistItemComponent } from './components/watchlist-item/watchlist-item.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule } from '@angular/forms';
import { ScrollerComponent } from './components/scroller/scroller.component';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: WatchlistComponent, pathMatch: 'full' }]),
    SharedModule,
    MaterialModule,
    FormsModule,
    VirtualScrollerModule
  ],
  declarations: [WatchlistComponent, WatchlistItemComponent, ScrollerComponent]
})
export class WatchlistModule {}
