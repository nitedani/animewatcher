import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/browse', data: { animation: 'HomePage' } },

  {
    path: 'browse',
    loadChildren: () => import('./modules/currentseason/currentseason.module').then(m => m.CurrentSeasonModule),
    data: { animation: 'HomePage' }
  },
  {
    path: 'watch',
    loadChildren: () => import('./modules/watch/watch.module').then(m => m.WatchModule),
    data: { animation: 'StreamPage' }
  },
  {
    path: 'list',
    loadChildren: () => import('./modules/watchlist/watchlist.module').then(m => m.WatchlistModule),
    data: { animation: 'WatchList' }
  },
  {
    path: 'feed',
    loadChildren: () => import('./modules/foryou/foryou.module').then(m => m.ForyouModule),
    data: { animation: 'feed' }
  },
  {
    path: 'anime/:id',
    loadChildren: () => import('./modules/anime-details/anime-details.module').then(m => m.AnimeDetailsModule),
    data: { animation: 'DetailsPage' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { initialNavigation: 'enabled', scrollPositionRestoration: 'top' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
