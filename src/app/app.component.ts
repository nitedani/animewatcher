import { MediaMatcher } from '@angular/cdk/layout';
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  Inject,
  ViewChild,
  ElementRef,
  HostListener,
  AfterViewInit
} from '@angular/core';
import { slideInAnimation } from './animations';
import { RouterOutlet, Router, Scroll, NavigationEnd } from '@angular/router';
import { ThemeService, Theme } from './modules/shared/theme/theme.service';
import { Observable } from 'rxjs';
import { isPlatformBrowser, ViewportScroller } from '@angular/common';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import ScrollManager from 'window-scroll-manager';
import { delay, filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [slideInAnimation]
})
export class AppComponent implements OnDestroy, OnInit, AfterViewInit {
  public theme: Observable<Theme>;
  scrollY$: Observable<number>;

  @ViewChild('snav', { static: false })
  snavRef: MatSidenav;
  navbarHidden: Observable<boolean>;
  isSmallScreen = true;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private themeService: ThemeService,
    @Inject(PLATFORM_ID) private platformId: any,
    private router: Router,
    private viewportScroller: ViewportScroller
  ) {}

  ngAfterViewInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.snavRef.open();

      this.breakpointObserver.observe('(max-width: 700px)').subscribe(next => {
        if (next.matches) {
          this.isSmallScreen = true;
        } else {
          this.isSmallScreen = false;
        }
      });
    }
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  setTheme() {
    this.themeService.setTheme(Theme.BLACK);
  }
  setTheme2() {
    this.themeService.setTheme(Theme.DARK);
  }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.navbarHidden = this.themeService.navbarHidden;
      this.router.events
        .pipe(
          filter(event => event instanceof NavigationEnd),
          delay(10)
        )
        .subscribe(e => {
          const contentContainer = document.querySelector('.mat-sidenav-content');
          if (contentContainer) {
            contentContainer.scroll({ top: 0, left: 0, behavior: 'smooth' });
          } else {
            window.scroll({ top: 0, left: 0, behavior: 'smooth' });
          }
        });
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      document.body.appendChild(tag);
    }
    this.theme = this.themeService.getTheme();
  }
  ngOnDestroy(): void {}
}
