import { HttpClientModule } from '@angular/common/http';
import { NgModule, PLATFORM_ID, Inject, APP_INITIALIZER } from '@angular/core';
import {
  BrowserModule,
  makeStateKey,
  TransferState,
  BrowserTransferStateModule,
  ɵgetDOM
} from '@angular/platform-browser';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { AppComponent } from './app.component';
import { SharedModule } from './modules/shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData, isPlatformBrowser, DOCUMENT } from '@angular/common';
import en from '@angular/common/locales/en';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MaterialModule } from './material.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ApolloModule, Apollo } from 'apollo-angular';
import { HttpLinkModule, HttpLink, HttpLinkHandler } from 'apollo-angular-link-http';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { LocalStateService } from './services/localstate.service';
import { QueryService } from './modules/currentseason/services/query.service';
import { PrebootModule } from 'preboot';
const STATE_KEY = makeStateKey<any>('apollo.state');
const uri = '/graphql';

registerLocaleData(en);

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    BrowserModule.withServerTransition({ appId: 'animewatcher-app' }),
    PrebootModule.withConfig({ appRoot: 'app-root' }),
    BrowserTransferStateModule,
    TransferHttpCacheModule,
    SharedModule,
    AppRoutingModule,
    FormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    environment.production ? ServiceWorkerModule.register('ngsw-worker.js') : [],
    ApolloModule,
    HttpLinkModule
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: function(document: HTMLDocument, platformId: Object): Function {
        return () => {
          if (isPlatformBrowser(platformId)) {
            const dom = ɵgetDOM().getDefaultDocument();
            const styles: any[] = Array.prototype.slice.apply(dom.querySelectorAll(`style[ng-transition]`));
            styles.forEach(el => {
              // Remove ng-transition attribute to prevent Angular appInitializerFactory
              // to remove server styles before preboot complete
              el.removeAttribute('ng-transition');
            });
            /*
            document.addEventListener('PrebootComplete', () => {
              // After preboot complete, remove the server scripts
              setTimeout(() =>
                styles.forEach(el => {
                  try {
                    dom.removeChild(el);
                    console.log(el);
                  } catch (error) {
                    console.log(el);
                    return false;
                  }
                })
              );
            });
            */
          }
        };
      },
      deps: [DOCUMENT, PLATFORM_ID],
      multi: true
    },

    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' }, multi: true },
    LocalStateService,
    QueryService
  ]
})
export class AppModule {
  cache: InMemoryCache;
  link: HttpLinkHandler;

  constructor(
    private readonly apollo: Apollo,
    private readonly transferState: TransferState,
    private readonly httpLink: HttpLink,
    @Inject(PLATFORM_ID) readonly platformId: any
  ) {
    const isBrowser = isPlatformBrowser(platformId);
    this.cache = new InMemoryCache();
    this.link = this.httpLink.create({ uri });

    this.apollo.create({
      link: this.link,
      cache: this.cache,
      ...(isBrowser
        ? {
            ssrForceFetchDelay: 200
          }
        : {
            ssrMode: true
          })
    });

    if (isBrowser) {
      this.onBrowser();
    } else {
      this.onServer();
    }
  }

  onServer() {
    this.transferState.onSerialize(STATE_KEY, () => this.cache.extract());
  }

  onBrowser() {
    const state = this.transferState.get<NormalizedCacheObject>(STATE_KEY, null);
    this.cache.restore(state);
  }
}
