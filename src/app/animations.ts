import { trigger, transition, style, query, animateChild, animate, group } from '@angular/animations';

export const slideInAnimation = trigger('routeAnimations', [
  transition('DetailsPage => *', [
    group([
      query(
        ':leave',
        [
          style({
            position: 'absolute',
            overflow: 'hidden',
            top: 0,
            left: 0,
            width: '100%',
            'z-index': '60',
          }),
          animate('200ms ease-out', style({ top: '100%' })),
        ],
        { optional: true }
      ),
      query(
        ':enter',
        [
          style({
            top: 0,
            left: 0,
            width: '100%',
          }),
        ],
        { optional: true }
      ),
    ]),
  ]),
]);
