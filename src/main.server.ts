import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';

enableProdMode();

export { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
export { renderModule, renderModuleFactory } from '@angular/platform-server';
export { AppServerModule } from './app/app.server.module';
