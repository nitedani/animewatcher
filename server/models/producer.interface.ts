import { Field, ID, ObjectType, Int, ArgsType, Float } from 'type-graphql';
import { prop as Property, arrayProp as ArrayProperty, Typegoose } from '@typegoose/typegoose';

@ObjectType()
export class Producer {
  @Field(type => Int, { nullable: true })
  @Property()
  mal_id?: number;

  @Field(type => String, { nullable: true })
  @Property()
  type?: string;

  @Field(type => String, { nullable: true })
  @Property()
  name?: string;

  @Field(type => String, { nullable: true })
  @Property()
  url?: string;
}
