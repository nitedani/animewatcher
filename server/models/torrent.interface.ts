import { ObjectType, Field } from 'type-graphql';
import { prop as Property, arrayProp as ArrayProperty, Typegoose } from '@typegoose/typegoose';

@ObjectType()
export class TorrentLinks {
  @Field({ nullable: true })
  @Property()
  page?: string;

  @Field({ nullable: true })
  @Property({ required: true })
  file?: string;

  @Field({ nullable: true })
  @Property()
  magnet?: string;
}

export enum TorrentCategory {
  ENGSUB = '1_2',
  ANIME = '1_0',
  RAW = '1_4',
  NON_ENG_SUB = '1_3',
}

@ObjectType()
export class Torrent {
  normalizedName?: string;
  timestamp?: string;

  @Field((type) => [String], { nullable: true })
  @Property({ required: true })
  category: string[];

  @Field({ nullable: true })
  @Property({ required: true })
  seeders?: number;

  @Field({ nullable: true })
  @Property({ required: true })
  fileSize?: number;

  @Field({ nullable: true })
  @Property({ required: true })
  bundle?: boolean;

  @Field({ nullable: true })
  @Property({ required: true })
  hevc?: boolean;

  @Field({ nullable: true })
  @Property({ required: true })
  uniqueID?: string;

  @Field({ nullable: true })
  @Property({ required: true })
  name?: string;

  @Field({ nullable: true })
  @Property({ required: true })
  season?: string;

  @Field({ nullable: true })
  @Property({ required: true })
  episode?: string;

  @Field((type) => TorrentLinks, { nullable: true })
  @Property({ required: true })
  links?: TorrentLinks;

  @Field({ nullable: true })
  @Property({ required: true })
  uploadDate?: Date;

  static fromNyaaResponse(nyaaTorrent: {
    name: string;
    links: TorrentLinks;
    timestamp: string;
    seeders: string;
    fileSize: number;
    category: {
      label: string;
      code: string;
    };
  }) {
    return new Torrent()
      .setName(nyaaTorrent.name)
      .setLinks(nyaaTorrent.links)
      .setUploadDate(new Date(parseInt(nyaaTorrent.timestamp + '000', 10)))
      .setSeeders(parseInt(nyaaTorrent.seeders, 10))
      .setHevc(/x265|hevc/gim.test(nyaaTorrent.name))
      .setCategory([nyaaTorrent.category.code])
      .setFileSize(nyaaTorrent.fileSize);
  }

  normalize(str: string): string {
    return str
      .split(/[^a-zA-Z0-9]/)
      .filter((x) => x !== '')
      .join('')
      .toLowerCase();
  }

  setSeason(season: string) {
    this.season = season;
    return this;
  }

  getSeason() {
    return this.season;
  }

  setBundle(isBundle: boolean) {
    this.bundle = isBundle;
    return this;
  }
  isBundle() {
    return this.bundle;
  }

  setFileSize(fileSize: number) {
    this.fileSize = fileSize;
    return this;
  }

  getFileSize(): number {
    return this.fileSize;
  }

  setCategory(category: string[]) {
    this.category = category;
    return this;
  }

  getCategory(): string[] {
    return this.category;
  }

  setHevc(hevc: boolean) {
    this.hevc = hevc;
    return this;
  }

  getHevc(): boolean {
    return this.hevc;
  }

  getNormalizedName(): string {
    if (!this.normalizedName) this.normalizedName = this.normalize(this.name);
    return this.normalizedName;
  }

  setName(name: string): Torrent {
    this.name = name;
    this.normalizedName = this.normalize(this.name);
    return this;
  }

  getName(): string {
    return this.name;
  }

  setSeeders(seeders: number) {
    this.seeders = seeders;
    return this;
  }

  getSeeders(): number {
    return this.seeders ? this.seeders : 0;
  }

  setId(str: string) {
    this.uniqueID = str;
    return this;
  }

  getId(): string {
    return this.uniqueID;
  }

  setEpisode(episode: string) {
    if (episode.length === 1) episode = '0' + episode;
    this.episode = episode;
    return this;
  }

  getEpisode(): string {
    return this.episode;
  }

  setLinks(links: TorrentLinks) {
    this.links = links;
    return this;
  }

  getLinks(): TorrentLinks {
    return this.links;
  }

  setUploadDate(date: Date) {
    this.uploadDate = date;
    return this;
  }

  getUploadDate(): Date {
    return this.uploadDate;
  }

  getTimeStamp(): string {
    return this.timestamp;
  }
}
