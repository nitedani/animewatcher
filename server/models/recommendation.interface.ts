import { Field, ID, ObjectType, Int, ArgsType, Float } from 'type-graphql';
import { prop as Property, arrayProp as ArrayProperty, Typegoose } from '@typegoose/typegoose';

@ObjectType()
export class Recommendation {
  @Field((type) => Int, { nullable: true })
  @Property()
  mal_id: number;

  @Field((type) => String, { nullable: true })
  @Property()
  url: string;

  @Field((type) => String, { nullable: true })
  @Property()
  // tslint:disable-next-line: variable-name
  image_url: string;

  @Field((type) => String, { nullable: true })
  @Property()
  // tslint:disable-next-line: variable-name
  recommendation_url: string;

  @Field((type) => String, { nullable: true })
  @Property({ required: true })
  title: string;

  @Field((type) => Int, { nullable: true })
  @Property()
  recommendation_count: number;
}
