import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Season {
  @Field((type) => String, { nullable: true })
  value: string;

  @Field((type) => String, { nullable: true })
  viewValue: string;

  @Field((type) => String, { nullable: true })
  year: string;

  @Field((type) => String, { nullable: true })
  season: string;
}
