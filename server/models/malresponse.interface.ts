import { Genre } from './genre.interface';
import { Producer } from './producer.interface';
import { Recommendation } from './recommendation.interface';
import { Aired } from './airing.interface';
import { Summary } from '@angular/compiler';

export interface MalSeasonResponse {
  request_hash: string;
  request_cached: boolean;
  request_cache_expiry: number;
  season_name: string;
  season_year: number;
  anime: MalSeasonAnime[];
}

export interface MalSeasonAnime {
  mal_id: number;
  url: string;
  title: string;
  image_url: string;
  synopsis: string;
  type: string;
  airing_start?: Date;
  episodes?: number;
  members: number;
  genres: Genre[];
  source: string;
  producers: Producer[];
  score?: number;
  licensors: string[];
  r18: boolean;
  kids: boolean;
  continuing: boolean;
}

export interface Adaptation {
  mal_id: number;
  type: string;
  name: string;
  url: string;
}

export interface Prequel {
  mal_id: number;
  type: string;
  name: string;
  url: string;
}

export interface Sequel {
  mal_id: number;
  type: string;
  name: string;
  url: string;
}

export interface SideStory {
  mal_id: number;
  type: string;
  name: string;
  url: string;
}

export interface Related {
  Adaptation?: Adaptation[];
  Prequel?: Prequel[];
  Sequel?: Sequel[];
  'Side story'?: SideStory[];
}
interface Licensor {
  mal_id: number;
  type: string;
  name: string;
  url: string;
}

interface Studio {
  mal_id: number;
  type: string;
  name: string;
  url: string;
}

export interface MalAnimeResponse {
  request_hash: string;
  request_cached: boolean;
  request_cache_expiry: number;
  mal_id: number;
  url: string;
  image_url: string;
  trailer_url: string;
  title: string;
  title_english?: string;
  title_japanese: string;
  title_synonyms: string[];
  type: string;
  source: string;
  episodes: number;
  status: string;
  airing: boolean;
  aired: Aired;
  duration: string;
  rating: string;
  score: number;
  scored_by: number;
  rank: number;
  popularity: number;
  members: number;
  favorites: number;
  synopsis: string;
  background: string;
  premiered: string;
  broadcast: string;
  related: Related;
  producers: Producer[];
  licensors: Licensor[];
  studios: Studio[];
  genres: Genre[];
  opening_themes: string[];
  ending_themes: string[];
}

export interface RecommendationResponse {
  request_hash: string;
  request_cached: boolean;
  request_cache_expiry: number;
  recommendations: Recommendation[];
}
