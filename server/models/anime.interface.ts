import { Torrent } from './torrent.interface';
import { Field, ID, ObjectType, Int, ArgsType, Float } from 'type-graphql';
import { index as Index, prop as Property, arrayProp as ArrayProperty, Typegoose } from '@typegoose/typegoose';
import { ObjectId } from 'mongodb';
import { Genre } from './genre.interface';
import { Producer } from './producer.interface';
import {
  MalSeasonResponse,
  MalAnimeResponse,
  RecommendationResponse,
  Related as RelatedResponse,
} from './malresponse.interface';
import { Recommendation } from './recommendation.interface';
import { Aired } from './airing.interface';
import { Season } from '../jikants/src/interfaces/season/Season';

@ObjectType()
export class NextEpisode {
  @Field((type) => Int, { nullable: true })
  days?: number;

  @Field((type) => Int, { nullable: true })
  hours?: number;
}

@ObjectType()
export class Studio {
  @Field((type) => Int, { nullable: true })
  @Property()
  mal_id: number;

  @Field((type) => String, { nullable: true })
  @Property()
  type: string;

  @Field((type) => String, { nullable: true })
  @Property()
  name: string;

  @Field((type) => String, { nullable: true })
  @Property()
  url: string;
}

@ObjectType()
export class Adaptation {
  @Field((type) => Int, { nullable: true })
  @Property()
  mal_id: number;

  @Field((type) => String, { nullable: true })
  @Property()
  type: string;

  @Field((type) => String, { nullable: true })
  @Property()
  name: string;

  @Field((type) => String, { nullable: true })
  @Property()
  url: string;
}

@ObjectType()
export class Prequel {
  @Field((type) => Int, { nullable: true })
  @Property()
  mal_id: number;

  @Field((type) => String, { nullable: true })
  @Property()
  type: string;

  @Field((type) => String, { nullable: true })
  @Property()
  name: string;

  @Field((type) => String, { nullable: true })
  @Property()
  url: string;
}

@ObjectType()
export class Sequel {
  @Field((type) => Int, { nullable: true })
  @Property()
  mal_id: number;

  @Field((type) => String, { nullable: true })
  @Property()
  type: string;

  @Field((type) => String, { nullable: true })
  @Property()
  name: string;

  @Field((type) => String, { nullable: true })
  @Property()
  url: string;
}

@ObjectType()
export class SideStory {
  @Field((type) => Int, { nullable: true })
  @Property()
  mal_id: number;

  @Field((type) => String, { nullable: true })
  @Property()
  type: string;

  @Field((type) => String, { nullable: true })
  @Property()
  name: string;

  @Field((type) => String, { nullable: true })
  @Property()
  url: string;
}

@ObjectType()
export class Related {
  @Field((type) => [Adaptation], { nullable: true })
  @ArrayProperty({ items: Adaptation, default: [] })
  Adaptation: Adaptation[];

  @Field((type) => [Prequel], { nullable: true })
  @ArrayProperty({ items: Prequel, default: [] })
  Prequel: Prequel[];

  @Field((type) => [Sequel], { nullable: true })
  @ArrayProperty({ items: Sequel, default: [] })
  Sequel: Sequel[];

  @Field((type) => [SideStory], { nullable: true })
  @ArrayProperty({ items: SideStory, default: [] })
  Sidestory: SideStory[];
}

@ObjectType()
@Index({ score: -1 })
@Index({ normalizedName: 1 })
export class Anime {
  @Field()
  readonly _id?: ObjectId;

  @Field((type) => Related, { nullable: true })
  @Property()
  related: Related;

  @Field((type) => Int, { nullable: true })
  @Property()
  rank: number;

  @Field((type) => [Studio], { nullable: true })
  @ArrayProperty({ items: Studio, default: [] })
  studios: Studio[];

  @Field((type) => String, { nullable: true })
  @Property({ index: true })
  normalizedName?: string;

  @Field((type) => [String], { nullable: true })
  @Property({ required: true })
  synonyms?: string[];

  @Field((type) => [Recommendation], { nullable: true })
  @ArrayProperty({ items: Recommendation, default: [] })
  recommendations: Recommendation[];

  @Field((type) => String, { nullable: true })
  @Property({ required: true })
  url?: string;

  @Field((type) => Int, { nullable: true })
  @Property({ required: true, unique: true })
  malId?: number;

  @Field((type) => String, { nullable: true })
  @Property({ required: true })
  title?: string;

  @Field((type) => String, { nullable: true })
  @Property()
  // tslint:disable-next-line: variable-name
  title_english?: string;

  @Field((type) => String, { nullable: true })
  @Property()
  // tslint:disable-next-line: variable-name
  image_url?: string;

  @Field((type) => String, { nullable: true })
  @Property()
  // tslint:disable-next-line: variable-name
  trailer_url?: string;

  @Field((type) => [Torrent], { nullable: true })
  @ArrayProperty({ items: Torrent, default: [] })
  torrents?: Torrent[];

  @Field((type) => String, { nullable: true })
  @Property()
  synopsis?: string;

  @Field((type) => String, { nullable: true })
  @Property({ required: true })
  season?: string;

  @Field((type) => Float, { nullable: true })
  @Property()
  score?: number;

  @Field((type) => String, { nullable: true })
  @Property()
  type?: string;

  @Field({ nullable: true })
  @Property()
  // tslint:disable-next-line: variable-name
  airing_start?: Date;

  @Field((type) => Int, { nullable: true })
  @Property()
  episodes?: number;

  @Field((type) => Int, { nullable: true })
  @Property()
  members?: number;

  @Field((type) => [Genre], { nullable: true })
  @Property()
  genres?: Genre[];

  @Field((type) => String, { nullable: true })
  @Property()
  source?: string;

  @Field((type) => [Producer], { nullable: true })
  @Property()
  producers?: Producer[];

  @Field((type) => [String], { nullable: true })
  @Property()
  licensors?: string[];

  @Field((type) => Boolean, { nullable: true })
  @Property()
  r18?: boolean;

  @Field((type) => Boolean, { nullable: true })
  @Property()
  kids?: boolean;

  @Field((type) => Boolean, { nullable: true })
  @Property()
  continuing?: boolean;

  @Field((type) => String, { nullable: true })
  @Property()
  status?: string;

  @Field((type) => String, { nullable: true })
  @Property()
  premiered: string;

  @Field((type) => NextEpisode, { nullable: true })
  nextEpisode?: NextEpisode | null;

  @Field((type) => Aired, { nullable: true })
  @Property()
  aired: Aired;

  @Field({ nullable: true })
  @Property({ default: new Date(0) })
  // tslint:disable-next-line: variable-name
  detailsUpdated?: Date;

  @Field({ nullable: true })
  @Property({ default: new Date(0) })
  // tslint:disable-next-line: variable-name
  recommendationsUpdated?: Date;

  @Field({ nullable: true })
  @Property({ default: new Date(0) })
  // tslint:disable-next-line: variable-name
  episodesUpdated?: Date;

  static fromMalResponse(malResponse: Season): Anime[] {
    return malResponse.anime.map((malAnime) =>
      new Anime()
        .setTitle(malAnime.title)
        .setAiringStart(malAnime.airing_start)
        .setContinuing(malAnime.continuing)
        .setEpisodeCount(malAnime.episodes)
        .setGenres(malAnime.genres)
        .setImageUrl(malAnime.image_url)
        .setKids(malAnime.kids)
        .setLicensors(malAnime.licensors)
        .setMalId(malAnime.mal_id)
        .setMemberCount(malAnime.members)
        .setProducers(malAnime.producers)
        .setR18(malAnime.r18)
        .setScore(malAnime.score)
        .setSeason(malResponse.season_year + malResponse.season_name.toLowerCase())
        .setSource(malAnime.source)
        .setSynonyms([])
        .setSynopsis(malAnime.synopsis)
        .setTorrents([])
        .setType(malAnime.type)
        .setUrl(malAnime.url)
    );
  }

  public setDetails(malResponse: MalAnimeResponse): Anime {
    return this.setSynonyms(malResponse.title_synonyms)
      .setStatus(malResponse.status)
      .setEngTitle(malResponse.title_english)
      .setEpisodeCount(malResponse.episodes)
      .setAired(malResponse.aired)
      .setRelated(malResponse.related)
      .setRank(malResponse.rank)
      .setStudios(malResponse.studios)
      .setPremiered(malResponse.premiered)
      .setTrailer(malResponse.trailer_url)
      .setImageUrl(malResponse.image_url)
      .setDetailsUpdated();
  }

  public copyDetails(other: Anime): Anime {
    return this.setSynonyms(other.synonyms)
      .setScore(other.score)
      .setStatus(other.status)
      .setEngTitle(other.title_english)
      .setEpisodeCount(other.episodes)
      .setAired(other.aired)
      .setRelated(other.related)
      .setRank(other.rank)
      .setStudios(other.studios)
      .setImageUrl(other.image_url)
      .setPremiered(other.premiered);
  }

  public getMatchNames() {
    return ([this.title, this.title_english, this.synonyms].flat(2).filter((_) => !!_) as string[]).map((str) =>
      this.normalize(str)
    );
  }

  public setRecommendation(malResponse: RecommendationResponse): Anime {
    return this.setRecommendations(malResponse.recommendations).setRecommendationsUpdated();
  }

  public setRecommendations(recommendations: Recommendation[]) {
    this.recommendations = recommendations;
    return this;
  }

  getRecommendations(): Recommendation[] {
    return this.recommendations;
  }

  setDetailsUpdated() {
    this.detailsUpdated = new Date(Date.now());
    return this;
  }

  getDetailsUpdated(): Date {
    return this.detailsUpdated;
  }

  setEpisodesUpdated() {
    this.episodesUpdated = new Date(Date.now());
    return this;
  }

  getEpisodesUpdated(): Date {
    return this.episodesUpdated;
  }

  setRecommendationsUpdated() {
    this.recommendationsUpdated = new Date(Date.now());
    return this;
  }

  getRecommendationsUpdated(): Date {
    return this.recommendationsUpdated;
  }

  setRelated(related: RelatedResponse) {
    this.related = {
      Prequel: related.Prequel,
      Sequel: related.Sequel,
      Sidestory: related['Side story'],
      Adaptation: related.Adaptation,
    };

    return this;
  }

  getRelated(): Related {
    return this.related;
  }

  setRank(rank: number) {
    this.rank = rank;
    return this;
  }

  getRank(): number {
    return this.rank;
  }

  setPremiered(premiered: string) {
    this.premiered = premiered;
    return this;
  }

  getPremiered(): string {
    return this.premiered;
  }

  setStudios(studios: Studio[]) {
    this.studios = studios;
    return this;
  }

  getStudios(): Studio[] {
    return this.studios;
  }

  setAired(aired: Aired) {
    this.aired = aired;
    return this;
  }

  getAired(): Aired {
    return this.aired;
  }

  setNormalizedName(str: string) {
    this.normalizedName = str;
    return this;
  }

  getNormalizedName(): string {
    if (!this.normalizedName) this.normalizedName = this.normalize(this.title);
    return this.normalizedName;
  }

  setSynonyms(synonims: string[]) {
    this.synonyms = synonims;
    return this;
  }

  addSynonym(synonim: string) {
    this.synonyms.push(synonim);
    return this;
  }

  getSynonyms(): string[] {
    return this.synonyms;
  }
  setUrl(url: string) {
    this.url = url;
    return this;
  }

  getUrl(): string {
    return this.url;
  }
  setMalId(id: number) {
    this.malId = id;
    return this;
  }

  getMalId(): number {
    return this.malId;
  }

  setTrailer(trailer_url: string) {
    this.trailer_url = trailer_url;
    return this;
  }
  getTrailer() {
    return this.trailer_url;
  }

  normalize(str: string): string {

    try {
      return str
        .split(/[^a-zA-Z0-9]/)
        .filter((x) => x !== '')
        .join('')
        .toLowerCase();

    } catch {
      console.log(str);
    }
  }

  setTitle(title: string) {
    this.title = title;
    this.normalizedName = this.normalize(title);
    return this;
  }

  getEngTitle(): string {
    return this.title_english;
  }

  setEngTitle(title: string) {
    this.title_english = title;
    return this;
  }

  getTitle(): string {
    return this.title;
  }

  setImageUrl(url: string) {
    this.image_url = url;
    return this;
  }

  getImageUrl(): string {
    return this.image_url;
  }

  setTorrents(torrents: Torrent[]) {
    this.torrents = torrents;
    return this;
  }

  addTorrent(torrent: Torrent) {
    if (this.torrents.some((t) => t.episode === torrent.episode)) {
      this.torrents = this.torrents.filter((t) => t.episode !== torrent.episode);
    }
    this.torrents.push(torrent);
    return this;
  }

  getTorrents(): Torrent[] {
    return this.torrents;
  }

  setSynopsis(synopsis: string) {
    this.synopsis = synopsis;
    return this;
  }

  getSynopsis(): string {
    return this.synopsis;
  }

  setSeason(season: string) {
    this.season = season;
    return this;
  }

  getSeason(): string {
    return this.season;
  }

  setScore(score: number) {
    this.score = score;
    return this;
  }

  getScore(): number {
    return this.score;
  }

  setType(type: string) {
    this.type = type;
    return this;
  }

  getType(): string {
    return this.type;
  }

  setEpisodeCount(count: number) {
    this.episodes = count;
    return this;
  }

  getEpisodeCount(): number {
    return this.episodes;
  }
  setAiringStart(date: Date) {
    this.airing_start = new Date(date);
    return this;
  }

  getAiringStart(): Date {
    return this.airing_start;
  }
  setMemberCount(count: number) {
    this.members = count;
    return this;
  }

  getMemberCount(): number {
    return this.members;
  }
  setGenres(genres: Genre[]) {
    this.genres = genres;
    return this;
  }

  addGenre(genre: Genre) {
    this.genres.push(genre);
    return this;
  }

  getGenres(): Genre[] {
    return this.genres;
  }

  setSource(source: string) {
    this.source = source;
    return this;
  }

  getSource(): string {
    return this.source;
  }
  setLicensors(licensors: string[]) {
    this.licensors = licensors;
    return this;
  }

  getLicensors(): string[] {
    return this.licensors;
  }

  setProducers(producers: Producer[]) {
    this.producers = producers;
    return this;
  }

  getProducers(): Producer[] {
    return this.producers;
  }

  setR18(r18: boolean) {
    this.r18 = r18;
    return this;
  }

  getR18(): boolean {
    return this.r18;
  }

  setKids(kids: boolean) {
    this.kids = kids;
    return this;
  }

  getKids(): boolean {
    return this.kids;
  }

  setContinuing(continuing: boolean) {
    this.continuing = continuing;
    return this;
  }

  getContinuing(): boolean {
    return this.continuing;
  }

  setStatus(status: string) {
    this.status = status;
    return this;
  }

  getStatus(): string {
    return this.status;
  }
}

export type SortType = {
  key: 'score' | 'members';
  order: 'asc' | 'desc';
};

@ArgsType()
export class GetAnimeArgs {
  @Field((type) => [String], { nullable: true })
  genres?: string[];

  @Field((type) => Int, { defaultValue: 0 })
  limit?: number;

  @Field((type) => String, { nullable: true })
  sortBy?: 'score' | 'members';

  @Field((type) => String, { defaultValue: 'asc' })
  order?: 'asc' | 'desc';

  @Field((type) => Boolean, { nullable: true })
  torrents?: boolean;

  @Field((type) => [Int], { nullable: true })
  malId?: number[] | number;

  @Field((type) => String, { nullable: true })
  title?: string | RegExp;

  @Field((type) => String, { nullable: true })
  season?: string;

  @Field((type) => Boolean, { nullable: true })
  r18?: any;

  @Field((type) => Boolean, { nullable: true })
  continuing?: boolean;

  @Field((type) => String, { nullable: true })
  fetchNewEpisodes?: 'fetch' | 'force';

  @Field((type) => String, { nullable: true })
  fetchRecommendations?: 'fetch' | 'force';

  @Field((type) => String, { nullable: true })
  fetchDetails?: 'fetch' | 'force';
}
