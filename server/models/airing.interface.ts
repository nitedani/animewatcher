import { Field, ID, ObjectType, Int, ArgsType, Float } from 'type-graphql';
import { prop as Property, arrayProp as ArrayProperty, Typegoose } from '@typegoose/typegoose';

@ObjectType()
export class From {
  @Field((type) => Int, { nullable: true })
  @Property()
  day: number;
  @Field((type) => Int, { nullable: true })
  @Property()
  month: number;
  @Field((type) => Int, { nullable: true })
  @Property()
  year: number;
}

@ObjectType()
export class To {
  @Field((type) => Int, { nullable: true })
  @Property()
  day: number;
  @Field((type) => Int, { nullable: true })
  @Property()
  month: number;
  @Field((type) => Int, { nullable: true })
  @Property()
  year: number;
}
@ObjectType()
export class Prop {
  @Field((type) => From, { nullable: true })
  @Property({ required: true })
  from: From;
  @Field((type) => To, { nullable: true })
  @Property({ required: true })
  to: To;
}

@ObjectType()
export class Aired {
  @Field({ nullable: true })
  @Property()
  from: Date;
  @Field({ nullable: true })
  @Property()
  to: Date;
  @Field((type) => Prop, { nullable: true })
  @Property({ required: true })
  prop: Prop;
  @Field((type) => String, { nullable: true })
  @Property({ required: true })
  string: string;
}
