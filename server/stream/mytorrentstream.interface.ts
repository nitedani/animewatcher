import * as TorrentStream from '../torrent-stream-master';
import { Subject } from 'rxjs';
import * as fs from 'fs';
import * as ParseTorrent from 'parse-torrent';
import * as ffmpeg from 'fluent-ffmpeg';

export declare class TorrentEngine {
  extracting: boolean;
  ffmpegHandler: ffmpeg.FfmpegCommand;
  filename: string;
  torrent: any;
  bitfield: Map<number, boolean>;
  completeBits: (number | boolean)[];
  interval: NodeJS.Timeout;
  index: TorrentFile;
  files: TorrentFile[];
  swarm: Swarm;
  infoHash: string;
  destroyed: Subject<boolean>;
  subsmetadata: ffmpeg.FfprobeData;
  torrentSubs: string[];
  crunchySubs: Promise<string[]>;
  ffLanguageArgs: string[];
  streamUrl: string;
  pieces: any[];
  reservations: any[];

  gc(): any;
  destroy(callback?: () => void): void;
  connect(peer: string): void;
  disconnect(peer: string): void;
  block(peer: string): void;
  remove(keepPieces: boolean, callback: () => void): void;
  listen(port?: number, callback?: () => void): void;

  // Events
  on(event: 'ready' | 'torrent' | 'idle' | string, callback: (cb?: any) => any): void;
  on(event: 'download', callback: (pieceIndex: number) => void): void;
  on(event: 'upload', callback: (pieceIndex: number, offset: number, length: number) => void): void;
}
export declare class TorrentEngineOptions {
  connections?: number; // Max amount of peers to be connected to.
  uploads?: number; // Number of upload slots.
  tmp?: string; // Root folder for the files storage. Default folder under /tmp/torrent-stream/{infoHash}.
  path?: string; // Path where to save the files. Overrides 'tmp'.
  verify?: boolean; // Verify previously stored data before starting.
  dht?: boolean; // Whether or not to use DHT to initialize the swarm.
  tracker?: boolean; // Whether or not to use trackers from torrent file or magnet link.
  trackers?: string[]; // Allows to declare additional custom trackers to use.
  storage?: any; // Use a custom storage backend rather than the default disk-backed one.
  pulse?: number;
  flood?: number;
  port?: number;
}

export declare class Swarm {
  pause: () => void;
  resume: () => void;
  downloaded: number;
  wires: any[];
}
export declare class TorrentFile {
  name: string;
  path: string;
  length: number;

  select(): void;
  deselect(): void;
  createReadStream(options?: ReadStreamOptions): fs.ReadStream;
}
export declare class ReadStreamOptions {
  start: number;
  end: number;
}

export function myTorrentStream(magnet: string | Buffer, options?: TorrentEngineOptions): TorrentEngine {
  return TorrentStream(magnet, options) as TorrentEngine;
}
