import { Injectable } from '@nestjs/common';
import { SocketGateway } from '../socket/socket.gateway';
import { myTorrentStream, TorrentEngine } from './mytorrentstream.interface';
import * as rangeParser from 'range-parser';
import * as pump from 'pump';
import * as mime from 'mime';
import * as fs from 'fs';
import * as pathToFfmpeg from 'ffmpeg-static';
import * as path from 'path';
import { Request, Response } from 'express';
import { Subject, ReplaySubject, of, from } from 'rxjs';
import { takeUntil, delayWhen, tap } from 'rxjs/operators';
import * as ffmpeg from 'fluent-ffmpeg';
import { CrunchySubScraper } from 'server/crunchysub/scrape';
ffmpeg.setFfmpegPath(pathToFfmpeg);

export interface Stream {
  socketId: string;
  url: string;
  malId: string;
  episodeId: string;
}

const languages = [
  'eng',
  'ger',
  'fre',
  'ita',
  'spa',
  'spala',
  'por',
  'rus',
  'pol',
  'dut',
  'nob',
  'fin',
  'tur',
  'swe',
  'gre',
  'heb',
  'rum',
  'ind',
  'tha',
  'kor',
  'dan',
  'jpn',
  'chi',
  'vie',
  'cze',
  'hun',
];

@Injectable()
export class StreamService {
  engines = new Map<string, TorrentEngine>();
  bitfields = new Map<string, ReplaySubject<number>>();
  completeSubs: string[] = [];
  ffmpegHandlers: { [hash: string]: ffmpeg.FfmpegCommand } = {};
  engineHashCounter: { [hash: string]: number } = {};
  ffmpegEngines: { [hash: string]: TorrentEngine } = {};
  trackers = [
    'http://nyaa.tracker.wf:7777/announce',
    'udp://open.stealth.si:80/announce',
    'udp://tracker.opentrackr.org:1337/announce',
    'udp://tracker.coppersurfer.tk:6969/announce',
    'udp://exodus.desync.com:6969/announce',
    'udp://tracker.uw0.xyz:6969/announce',
    'udp://tracker.zer0day.to:1337/announce',
    'udp://tracker.leechers-paradise.org:6969',
    'udp://explodie.org:6969',
    'udp://tracker.opentrackr.org:1337',
    'udp://tracker.internetwarriors.net:1337/announce',
    'http://mgtracker.org:6969/announce',
    'udp://ipv6.leechers-paradise.org:6969/announce',
    'http://sukebei.tracker.wf:7777/announce',
    'http://tracker.anirena.com:80/announce',
    'http://anidex.moe:6969/announce',
    'udp://tracker.uw0.xyz:6969/announce',
    'udp://tracker.zer0day.to:1337/announce',
    'udp://tracker.leechers-paradise.org:6969',
    'udp://explodie.org:6969',
    'udp://tracker.internetwarriors.net:1337/announce',
    'http://mgtracker.org:6969/announce',
    'udp://ipv6.leechers-paradise.org:6969/announce',
    'http://nyaa.tracker.wf:7777/announce',
    'http://sukebei.tracker.wf:7777/announce',
    'http://tracker.anirena.com:80/announce',
    'http://anidex.moe:6969/announce',
  ];

  constructor(private readonly socketGateway: SocketGateway) {
    fs.readFile(path.join('temp', 'subs', 'completeSub.json'), (err, data) => {
      if (err) {
        console.log('No CompleteSub.json found, ignoring');
      } else {
        this.completeSubs = JSON.parse(data.toString()) as string[];
        console.log('Complete sub read from completeSub.json');
      }
    });

    socketGateway.startStream$.subscribe((stream) => this.startStream(stream));
    socketGateway.reloadSubs$.subscribe((socketId) => this.reloadSubs(socketId));
    socketGateway.stopStream$.subscribe((socketId) => this.destroyEngine(socketId));
    socketGateway.socketDisconnect$.subscribe((socketId) => this.destroyEngine(socketId));
  }

  async getSubs(response: Response, id: string, language: string) {
    const engine = this.engines.get(id);
    if (!engine) return response.end();

    const subFileName = (await engine.crunchySubs).includes(language) ? `crunchy${language}.ass` : `${language}.ass`;

    console.log(subFileName);
    if (!subFileName) response.status(404).end();
    else
      response.sendFile(path.join(process.cwd(), 'temp', 'subs', engine.infoHash, subFileName), {}, (err) => {
        if (err) {
          // console.log(err);
          response.status(404).end();
        } else {
          console.log('Sent subs');
        }
      });
  }

  getStream(request: Request, response: Response, id: string) {
    const engine = this.engines.get(id);
    if (!engine) return response.end();

    response.setHeader('Cache-Control', 'no-transform');

    if (request.method === 'OPTIONS' && request.headers['access-control-request-headers']) {
      response.setHeader('Access-Control-Allow-Origin', request.headers.origin);
      response.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
      response.setHeader('Access-Control-Allow-Headers', request.headers['access-control-request-headers']);
      response.setHeader('Access-Control-Max-Age', '1728000');

      response.end();
      return;
    }

    if (request.headers.origin) response.setHeader('Access-Control-Allow-Origin', request.headers.origin);

    const file = engine.index;
    let range: any = request.headers.range;
    range = range && rangeParser(file.length, range)[0];
    response.setHeader('Accept-Ranges', 'bytes');
    response.setHeader('Content-Type', mime.getType(file.name));
    response.setHeader('transferMode.dlna.org', 'Streaming');
    response.setHeader(
      'contentFeatures.dlna.org',
      'DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01700000000000000000000000000000'
    );
    if (!range) {
      response.setHeader('Content-Length', file.length);
      if (request.method === 'HEAD') return response.end();
      /*
      if (/x265|hevc/gim.test(file.name))
        pump(
          ffmpeg(file.createReadStream())
            .format('matroska')
            .outputOptions([
              '-map 0',
              '-c:a copy',
              '-c:s copy',
              '-c:v h264_nvenc',
              '-pix_fmt yuv420p',
              '-crf 22',
              '-level 4.0',
              '-movflags +faststart'
            ])
            .on('error', error => {
              console.log(error);
            })
            .pipe(),
          response
        );

      else*/

      pump(file.createReadStream(), response);

      return;
    }

    response.statusCode = 206;
    response.setHeader('Content-Length', range.end - range.start + 1);
    response.setHeader('Content-Range', 'bytes ' + range.start + '-' + range.end + '/' + file.length);
    if (request.method === 'HEAD') return response.end();
    console.log(range);
    /*
    if (/x265|hevc/gim.test(file.name))
      pump(
        ffmpeg(file.createReadStream(range))
          .format('matroska')
          .outputOptions([
            '-map 0',
            '-c:a copy',
            '-c:s copy',
            '-c:v h264_nvenc',
            '-pix_fmt yuv420p',
            '-crf 22',
            '-level 4.0',
            '-movflags +faststart'
          ])
          .on('error', error => {
            console.log(error);
          })
          .pipe(),
        response
      );

    else*/

    const fileStream = file.createReadStream(range);
    pump(fileStream, response);
  }

  getFilesizeInBytes(pathToFile: string) {
    try {
      const stats = fs.statSync(pathToFile);
      const fileSizeInBytes = stats.size;
      return fileSizeInBytes;
    } catch (error) {
      return 0;
    }
  }

  // Here comes the command to re-extract subtitles
  async reloadSubs(id: string): Promise<boolean> {
    const engine = this.engines.get(id);
    if (!engine) {
      this.socketGateway.sendMessage(id, 'subtitles', 'unavailable');
      return false;
    }
    let ffmpegEngine = this.engines.get(engine.infoHash);
    // If this is true, then the subtitles were extracted earlier without an error, no need to extract them again.
    if (this.completeSubs.includes(engine.infoHash)) {
      if (!engine.subsmetadata && !ffmpegEngine) {
        engine.subsmetadata = JSON.parse(
          (await fs.promises.readFile(path.join('temp', 'subs', engine.infoHash, 'info.json'))).toString()
        );
        const subtitleStreams = engine.subsmetadata.streams.filter((stream) => stream.codec_type === 'subtitle');
        engine.torrentSubs = [...new Set(subtitleStreams.map((stream) => stream['tags']['language'] as string))];
      } else if (ffmpegEngine) {
        engine.torrentSubs = ffmpegEngine.torrentSubs;
      }

      this.socketGateway.sendMessage(id, 'languages', [
        ...new Set([
          ...engine.torrentSubs,

          // Wait for the scraping process to finish, then send a set of languages to browser
          ...(await engine.crunchySubs),
          ...(await fs.promises.readdir(path.join('temp', 'subs', engine.infoHash)))
            .map((filename) => filename.split('.')[0])
            .filter((filename) => languages.includes(filename)),
        ]),
      ]);

      // With this sent, the browser will know that the subtitles were extracted without error, and it will stop asking for re-extraction.
      this.socketGateway.sendMessage(id, 'subtitles', 'complete');
      return true;
    }
    ffmpegEngine = this.engines.get(engine.infoHash);
    const localPath = path.join('temp', 'torrent-stream', engine.infoHash, engine.index.path);

    if (!engine.subsmetadata && !ffmpegEngine) {
      try {
        engine.subsmetadata = JSON.parse(
          (await fs.promises.readFile(path.join('temp', 'subs', engine.infoHash, 'info.json'))).toString()
        );
      } catch (error) {
        console.log('Creating folder', engine.infoHash);
        await fs.promises.mkdir(path.join('temp', 'subs', engine.infoHash), { recursive: true });
        await new Promise<void>((resolve) => {
          ffmpeg.ffprobe(localPath, async (err, metadata) => {
            engine.subsmetadata = metadata;
            await fs.promises.writeFile(
              path.join('temp', 'subs', engine.infoHash, 'info.json'),
              JSON.stringify(metadata)
            );
            resolve();
          });
        });
      }
      ffmpegEngine = this.engines.get(engine.infoHash);
      const subtitleStreams = engine.subsmetadata.streams.filter((stream) => stream.codec_type === 'subtitle');
      engine.torrentSubs = [...new Set(subtitleStreams.map((stream) => stream['tags']['language'] as string))];
      this.socketGateway.sendMessage(id, 'languages', [
        ...new Set([
          ...engine.torrentSubs,
          ...(await engine.crunchySubs),
          ...(await fs.promises.readdir(path.join('temp', 'subs', engine.infoHash)))
            .map((filename) => filename.split('.')[0])
            .filter((filename) => languages.includes(filename)),
        ]),
      ]);
      this.socketGateway.sendMessage(id, 'subtitles', 'partial');
      return false;
    } else if (ffmpegEngine) {
      engine.torrentSubs = ffmpegEngine.torrentSubs;
      this.socketGateway.sendMessage(id, 'languages', [
        ...new Set([
          ...ffmpegEngine.torrentSubs,
          ...(await engine.crunchySubs),
          ...(await fs.promises.readdir(path.join('temp', 'subs', engine.infoHash)))
            .map((filename) => filename.split('.')[0])
            .filter((filename) => languages.includes(filename)),
        ]),
      ]);
    }
    ffmpegEngine = this.engines.get(engine.infoHash);
    // Allow only one extraction process for the same file at a time
    if (!ffmpegEngine && engine.index && !this.ffmpegHandlers[engine.infoHash]) {
      console.log('nem kellene');
      this.startFfmpegEngine(engine.streamUrl, engine.infoHash, engine.subsmetadata);
    } else {
      this.socketGateway.sendMessage(id, 'subtitles', 'partial');
    }
  }

  startFfmpegEngine(url: string, hash: string, subsmetadata: ffmpeg.FfprobeData) {
    let engine: TorrentEngine = this.engines.get(hash);
    if (engine) {
      if (this.ffmpegHandlers[engine.infoHash]) {
        this.ffmpegHandlers[engine.infoHash].kill('SIGINT');
      }
      this.engines.delete(hash);
      engine.index = null;
      engine.destroy();
    }

    try {
      engine = myTorrentStream(url, {
        tmp: path.join(process.cwd(), 'temp'),
        // pulse: 10 * 1024 * 1024,
        flood: 0,
        verify: false,
        port: Math.floor(Math.random() * 999) + 8000,
        trackers: this.trackers,
      });
      engine.destroyed = new Subject<boolean>();
    } catch (error) {
      console.log(error);
    }

    engine.subsmetadata = subsmetadata;
    const subtitleStreams = engine.subsmetadata.streams.filter((stream) => stream.codec_type === 'subtitle');
    engine.torrentSubs = [...new Set(subtitleStreams.map((stream) => stream['tags']['language'] as string))];

    this.engines.set(hash, engine);
    engine.on('uninterested', () => {
      engine.swarm.pause();
    });

    engine.on('error', (error) => {
      console.log(error);
    });

    engine.on('interested', () => {
      engine.swarm.resume();
    });

    let filename: string;

    let $bitfield: ReplaySubject<number>;
    engine.completeBits = [];
    engine.on('idle', () => {
      if (engine.completeBits[engine.completeBits.length - 1] !== true) {
        engine.completeBits.push(true);
      }
    });
    const onready = async () => {
      $bitfield = this.bitfields.get(engine.infoHash);
      if ($bitfield) {
        console.log('Already existing bitfield subject, joining others');
        $bitfield.pipe(takeUntil(engine.destroyed)).subscribe((bit) => {
          //  for (let i = 0; i < engine.swarm.wires.length; i++) engine.swarm.wires[i].have(bit);
          // engine.reservations[bit] = null;
          // engine.pieces[bit] = null;
          engine.bitfield.set(bit, true);
          //  engine.gc();
        });
      } else {
        console.log('THIS SHOULD BE UNREACHABLE CODE');
        throw new Error('ffmpeg engine cant find bitfield subject - THIS SHOULD BE UNREACHABLE CODE');
      }

      const largestFile = engine.files.reduce((a, b) => {
        return a.length > b.length ? a : b;
      });

      const index = engine.files.indexOf(largestFile);

      engine.files[index].select();
      engine.index = engine.files[index];

      filename = engine.index.name.split('/').pop().replace(/\{|\}/g, '');
      engine.filename = filename;

      try {
        const file = await fs.promises.readFile(path.join('temp', 'torrent-stream', engine.infoHash, 'bitfield.json'));
        const parsed = JSON.parse(file.toString());
        engine.completeBits = parsed;
        if (parsed[parsed.length - 1] === true) {
          for (let i = 0; i < parsed.length - 1; i++) {
            const bit = parsed[i];
            engine.pieces[bit] = null;
            engine.bitfield.set(bit, true);
          }
          engine.completeBits.push(true);
        } else {
          for (const bit of parsed) {
            engine.pieces[bit] = null;
            engine.bitfield.set(bit, true);
          }
        }
      } catch (error) {
        console.log(error);
      }

      let errorCount = 0;
      // Base argument for ffmpeg, this will un-map audio and video track, only keeping subtitle
      const baseOptions = ['-map', '-0:a?', '-map', '-0:v?', '-c', 'copy', '-flush_packets', '1'];
      // These will be the file-specific arguments for ffmpeg, later extracting every subtitle track that the file contains.

      let stream;

      if (engine.completeBits[engine.completeBits.length - 1] === true) {
        stream = fs.createReadStream(path.join('temp', 'torrent-stream', engine.infoHash, engine.index.path));
      } else {
        stream = engine.index.createReadStream();
      }

      stream.on('error', console.log);

      this.ffmpegHandlers[engine.infoHash] = ffmpeg(stream).format('ass').inputOptions(['-y']);

      engine.torrentSubs.forEach((language) => {
        this.ffmpegHandlers[engine.infoHash]
          .output(path.join('temp', 'subs', engine.infoHash, `${language}.ass`))
          .outputOptions([
            ...baseOptions,
            '-map',
            `0:${subtitleStreams.find((sub) => sub['tags']['language'] === language).index}?`,
          ]);
      });

      // console.log([...baseOptions, ...engine.ffLanguageArgs]);

      this.ffmpegHandlers[engine.infoHash]
        .on('error', (err, stdout, stderr) => {
          console.log(399, err.toString());
          stream.destroy();
          // This would happen if ffmpeg gets an argument to extract a track it can't find.
          if (err.toString().includes('matches no streams')) {
            errorCount++;
          } else {
            this.ffmpegHandlers[engine.infoHash] = null;
            delete this.ffmpegHandlers[engine.infoHash];
          }
        })
        .on('stderr', (stderr) => {
          console.log(stderr.toString());
          if (stderr.toString().includes('invalid')) errorCount++;
          if (errorCount > 10) {
            if (this.ffmpegHandlers[engine.infoHash]) this.ffmpegHandlers[engine.infoHash].kill('SIGINT');
          }
        })
        .on('end', async (stdout, stderr) => {
          stream.destroy();
          console.log('FFmpeg exited');
          this.ffmpegHandlers[engine.infoHash] = null;
          delete this.ffmpegHandlers[engine.infoHash];
          if (errorCount === 0 && engine.completeBits[engine.completeBits.length - 1] === true) {
            this.completeSubs.push(engine.infoHash);
            await fs.promises.writeFile(
              path.join('temp', 'subs', 'completeSub.json'),
              JSON.stringify(this.completeSubs)
            );
          }
        });

      this.ffmpegHandlers[engine.infoHash].run();
    };

    if (engine.torrent) onready();
    else engine.on('ready', onready);

    engine.listen();

    engine.on('verify', (index: number) => {
      $bitfield.next(index);
    });
  }

  startStream(stream: Stream) {
    const startPercentage = 100;
    let lastSentPercentage = null;

    let engine: TorrentEngine = this.engines.get(stream.socketId);
    if (engine) {
      this.engines.delete(stream.socketId);
      engine.index = null;
      engine.destroy();
      clearInterval(engine.interval);
    }

    try {
      engine = myTorrentStream(stream.url, {
        tmp: path.join(process.cwd(), 'temp'),
        pulse: 10 * 1024 * 1024,
        flood: 0,
        verify: false,
        port: Math.floor(Math.random() * 999) + 8000,
        trackers: this.trackers,
      });
      engine.destroyed = new Subject<boolean>();
    } catch (error) {
      console.log(error);
    }

    engine.streamUrl = stream.url;
    this.engines.set(stream.socketId, engine);

    engine.on('uninterested', () => {
      engine.swarm.pause();
    });

    engine.on('error', (error) => {
      console.log(error);
    });

    engine.on('interested', () => {
      engine.swarm.resume();
    });

    let filename: string;
    let localPath: string;
    let $bitfield: ReplaySubject<number>;

    const onready = async () => {
      if (!this.engineHashCounter[engine.infoHash] || this.engineHashCounter[engine.infoHash] <= 0)
        this.engineHashCounter[engine.infoHash] = 1;
      else this.engineHashCounter[engine.infoHash]++;
      try {
        engine.crunchySubs = JSON.parse(
          (await fs.promises.readFile(path.join('temp', 'subs', engine.infoHash, 'crunchy.json'))).toString()
        );
      } catch (error) {
        await fs.promises.mkdir(path.join('temp', 'subs', engine.infoHash), { recursive: true });
        engine.crunchySubs = CrunchySubScraper.getSubs(stream.malId, stream.episodeId).then(async (files) => {
          const languages = files.map((file) => file.language);
          await fs.promises.writeFile(
            path.join('temp', 'subs', engine.infoHash, 'crunchy.json'),
            JSON.stringify(languages)
          );

          for (const file of files) {
            await fs.promises.writeFile(
              path.join('temp', 'subs', engine.infoHash, `crunchy${file.language}.ass`),
              file.file
            );
          }
          return languages;
        });
      }

      $bitfield = this.bitfields.get(engine.infoHash);
      if ($bitfield) {
        console.log('Already existing bitfield subject, joining others');
        $bitfield.pipe(takeUntil(engine.destroyed)).subscribe((bit) => {
          for (let i = 0; i < engine.swarm.wires.length; i++) engine.swarm.wires[i].have(bit);
          engine.reservations[bit] = null;
          engine.pieces[bit] = null;
          engine.bitfield.set(bit, true);
          engine.gc();
        });
      } else {
        console.log('Starting new bitfield subject');
        let verified = 0;
        $bitfield = new ReplaySubject<number>(64);
        this.bitfields.set(engine.infoHash, $bitfield);
        $bitfield
          .pipe(
            tap((bit) => {
              engine.completeBits.push(bit);
              engine.bitfield.set(bit, true);
              verified++;
            }),
            delayWhen(() =>
              verified > 64
                ? from(
                    (async () => {
                      try {
                        await fs.promises.writeFile(
                          path.join('temp', 'torrent-stream', engine.infoHash, 'bitfield.json'),
                          JSON.stringify(engine.completeBits)
                        );
                      } catch (error) {
                        console.log(error);
                      }
                      //  console.log('Bitfield saved to .json: ' + engine.infoHash);
                      verified = 0;
                    })()
                  )
                : of(true)
            )
          )
          .subscribe();

        engine.on('idle', async () => {
          if (engine.completeBits[engine.completeBits.length - 1] !== true) {
            engine.completeBits.push(true);
            await fs.promises.writeFile(
              path.join('temp', 'torrent-stream', engine.infoHash, 'bitfield.json'),
              JSON.stringify(engine.completeBits)
            );
            console.log('Final bitfield saved to .json: ' + engine.infoHash);
          }
        });
      }

      const largestFile = engine.files.reduce((a, b) => {
        return a.length > b.length ? a : b;
      });

      const index = engine.files.indexOf(largestFile);

      engine.files[index].select();
      engine.index = engine.files[index];

      filename = engine.index.name.split('/').pop().replace(/\{|\}/g, '');
      engine.filename = filename;

      localPath = path.join('temp', 'torrent-stream', engine.infoHash, engine.index.path);

      try {
        const file = await fs.promises.readFile(path.join('temp', 'torrent-stream', engine.infoHash, 'bitfield.json'));
        const parsed = JSON.parse(file.toString());
        engine.completeBits = parsed;
        if (parsed[parsed.length - 1] === true) {
          for (let i = 0; i < parsed.length - 1; i++) {
            const bit = parsed[i];
            engine.pieces[bit] = null;
            engine.bitfield.set(bit, true);
          }
          engine.completeBits.push(true);
        } else {
          for (const bit of parsed) {
            engine.pieces[bit] = null;
            engine.bitfield.set(bit, true);
          }
        }
        console.log('Bitfield restored from json');
      } catch (error) {
        engine.completeBits = [];
        console.log(error);
      }

      let downloadedPercentage = 0;

      const initialFileSize = this.getFilesizeInBytes(localPath);
      engine.interval = setInterval(() => {
        if (downloadedPercentage < startPercentage) {
          try {
            downloadedPercentage = Math.floor(((initialFileSize + engine.swarm.downloaded) / 100000000) * 100);
            if (downloadedPercentage > 100) downloadedPercentage = 100;
          } catch (error) {
            console.log(error);
          }
        } else {
          clearInterval(engine.interval);
        }

        if (lastSentPercentage !== downloadedPercentage) {
          this.socketGateway.sendMessage(stream.socketId, 'percentage', downloadedPercentage);
          lastSentPercentage = downloadedPercentage;
        }
      }, 1000);
    };

    if (engine.torrent) onready();
    else engine.on('ready', onready);

    engine.listen();

    engine.on('verify', (index: number) => {
      $bitfield.next(index);
    });
  }

  destroyEngine(id: string) {
    let engine = this.engines.get(id);
    if (engine) {
      this.engineHashCounter[engine.infoHash]--;
      if (this.engineHashCounter[engine.infoHash] === 0) {
        this.bitfields.get(engine.infoHash).complete();
        this.bitfields.delete(engine.infoHash);

        if (this.ffmpegHandlers[engine.infoHash]) this.ffmpegHandlers[engine.infoHash].kill('SIGINT');
        this.destroyEngine(engine.infoHash);
      }

      engine.destroyed.next(true);
      this.engines.delete(id);
      engine.index = null;
      engine.destroy();
      clearInterval(engine.interval);
      engine = null;
    }
  }
}
