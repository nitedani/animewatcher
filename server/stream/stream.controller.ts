import { Controller, Get, Res, Param, Req } from '@nestjs/common';
import { StreamService } from './stream.service';
import { Request, Response } from 'express';

@Controller('watch/*')
export class StreamController {
  constructor(private readonly streamService: StreamService) {}

  @Get(':id/subs/:lang')
  getSubs(@Res() res: Response, @Param('id') id: string, @Param('lang') language: string) {
    this.streamService.getSubs(res, id, language);
  }

  @Get(':id')
  getStream(@Req() req: Request, @Res() res: Response, @Param('id') id: string) {
    this.streamService.getStream(req, res, id);
  }
}
