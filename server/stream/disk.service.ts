import * as fs from 'fs';
import * as rimraf from 'rimraf';
import * as path from 'path';
const du = require('du');

class AnimeFolder {
  size: number;
  hash: string;
  lastUsed: number;

  constructor(hash: string, size: number, lastUsed?: number) {
    this.hash = hash;
    this.size = size;
    this.lastUsed = lastUsed;
  }

  async updateSize() {
    this.size = await du(path.join('temp', 'torrent-stream', this.hash));
    return this.size;
  }

  async dispose(): Promise<void> {
    return new Promise(resolve => {
      rimraf(path.join('temp', 'torrent-stream', this.hash), () => resolve());
    });
  }

  async touch(): Promise<void> {
    const time = new Date();
    this.lastUsed = time.getTime();
    return new Promise((resolve, reject) => {
      const root = path.join('temp', 'torrent-stream', this.hash);
      const filepath = path.join(root, 'time.stamp');
      fs.utimes(filepath, time, time, err => {
        if (err) {
          fs.open(filepath, 'w', (openErr, fd) => {
            if (openErr) {
              fs.mkdir(root, { recursive: true }, () => {
                fs.open(filepath, 'w', (openErr2, fd2) => {
                  openErr2 ? reject(openErr2) : fs.close(fd2, () => resolve());
                });
              });
            } else {
              fs.close(fd, () => resolve());
            }
          });
        }
        resolve();
      });
    });
  }
}

enum ByteSize {
  GB = 1073741824,
  MB = 1048576,
  KB = 1024
}

export class DiskManager {
  static root = path.join('temp', 'torrent-stream');
  static animeFolders: { [hash: string]: AnimeFolder } = {};
  static diskSize = parseInt(process.env.MAX_SIZE_GB, 10) * ByteSize.GB;
  static maintainFree = parseInt(process.env.MAINTAIN_FREE_GB, 10) * ByteSize.GB;
  static lastCheckedSpaceAt = 0;
  static filledSpace: number; // in bytes

  static async freeUpSpaceFor(hash: string, size: number) {
    const foldersToCheck = Object.values(DiskManager.animeFolders).filter(
      folder => folder.lastUsed >= DiskManager.lastCheckedSpaceAt
    );

    DiskManager.lastCheckedSpaceAt = new Date().getTime();
    const filledBeforeCheck = foldersToCheck.reduce((acc, curr) => (acc += curr.size), 0);
    let filledAfterCheck = 0;
    for (const folder of foldersToCheck) {
      filledAfterCheck += await folder.updateSize();
    }

    DiskManager.filledSpace += filledAfterCheck - filledBeforeCheck;
    const free = DiskManager.diskSize - DiskManager.filledSpace;

    const freeUpForFileSize = size + 100 * ByteSize.MB;
    const maintainedFreeSpace = this.maintainFree > freeUpForFileSize ? this.maintainFree : freeUpForFileSize;

    if (free < maintainedFreeSpace) {
      const toDelete = Object.values(DiskManager.animeFolders)
        .filter(folder => folder.hash !== hash)
        .sort((a, b) => a.lastUsed - b.lastUsed);
      let deletedSpace = 0;

      for (let index = 0; index < toDelete.length && free + deletedSpace < maintainedFreeSpace; index++) {
        const animeFolder = toDelete[index];
        await animeFolder.updateSize();
        await animeFolder.dispose();
        this.filledSpace -= animeFolder.size;
        DiskManager.animeFolders[animeFolder.hash] = null;
        delete DiskManager.animeFolders[animeFolder.hash];
        deletedSpace += animeFolder.size;
      }
    }
  }

  static async startWatching(hash: string, torrentSize: number) {
    if (!torrentSize) throw new Error('Torrent size needs to be defined!');
    const animeFolder = DiskManager.animeFolders[hash];
    if (animeFolder) {
      if (
        await new Promise(resolve => {
          fs.readFile(path.join(DiskManager.root, hash, 'bitfield.json'), (err, data) => {
            const parsed = !err && JSON.parse(data.toString());
            if (err || !parsed[parsed.length - 1] === true) resolve(true);
            else resolve(false);
          });
        })
      ) {
        const currentFolderSize = await animeFolder.updateSize();
        await DiskManager.freeUpSpaceFor(hash, torrentSize - currentFolderSize);
      }

      await animeFolder.touch();
    } else {
      await DiskManager.freeUpSpaceFor(hash, torrentSize);
      const newFolder = new AnimeFolder(hash, 0);
      await newFolder.touch();
      DiskManager.animeFolders[hash] = newFolder;
    }
  }

  static async initFolders() {
    const folders = fs
      .readdirSync(DiskManager.root, { withFileTypes: true })
      .filter(dirent => dirent.isDirectory())
      .map(dirent => dirent.name);

    let filled = 0;
    for (const hash of folders) {
      const size = await du(path.join(DiskManager.root, hash));
      filled += size;
      const timestamp = (await fs.promises.stat(path.join(DiskManager.root, hash, 'time.stamp'))).mtime.getTime();
      DiskManager.animeFolders[hash] = new AnimeFolder(hash, size, timestamp);
    }
    DiskManager.filledSpace = filled;
    console.log(`Init temp folder for anime, ${filled / ByteSize.GB} GB`);
    DiskManager.lastCheckedSpaceAt = new Date().getTime();
  }
}
