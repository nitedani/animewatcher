import * as got from 'got';
import * as cheerio from 'cheerio';
export class LivechartScraper {
  static releases: { [malId: number]: number } = {};

  static getRelease(malId: number) {
    return LivechartScraper.releases[malId];
  }

  static async initLivechart() {
    const response = await got(`https://www.livechart.me/`);
    const $ = cheerio.load(response.body);
    $('.anime')
      .toArray()
      .forEach(value => {
        const episodeLabel = $(value)
          .find('.episode-countdown')
          .attr('data-label');

        if (episodeLabel === 'EP1') {
          const malIdMatch = /(?<=anime\/)\d+/.exec(
            $(value)
              .find('.mal-icon')
              .attr('href')
          );
          console.log(malIdMatch)
          if (malIdMatch) {
            const malId = parseInt(malIdMatch[0], 10);

            const timestamp = parseInt(
              $(value)
                .find('time')
                .attr('data-timestamp'),
              10
            );

            LivechartScraper.releases[malId] = timestamp;
            
           
          }
        }
      });
      
  }
}
