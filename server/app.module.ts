import { Module, HttpModule, CacheModule } from '@nestjs/common';
import { AngularUniversalModule } from '@nestjs/ng-universal';
import { join } from 'path';
import { AppServerModule } from '../src/main.server';
import { StreamService } from './stream/stream.service';
import { SocketGateway } from './socket/socket.gateway';
import { AnimelistController } from './animelist/animelist.controller';
import { StreamController } from './stream/stream.controller';
import { TypegooseModule } from 'nestjs-typegoose';
import { GraphQLModule } from '@nestjs/graphql';
import { Anime } from './models/anime.interface';
import { AnimeRepositoryService } from './repository/anime-repository/anime-repository.service';
import { AnimeResolver } from './resolvers/anime/anime.resolver';
import { Torrent } from './models/torrent.interface';
import { ObjectIdScalar } from './graphql-scalars/object-id.scalar';
import { UpdateSchedulerService } from './animelist/services/update-scheduler.service';
import { AnimeMatcherService, TrackerFirst, AnimeFirst } from './animelist/services/anime-matcher.service';
import { NyaaTracker } from './animelist/services/nyaa.tracker.service';
import { TorrentSupplierService } from './animelist/services/torrent-supplier.service';
import { TransformerA, TransformerB } from './animelist/services/torrent-transformer.service';
import { SeasonResolver } from './resolvers/anime/seasons.resolver';

@Module({
  imports: [
    AngularUniversalModule.forRoot({
      bootstrap: AppServerModule,
      viewsPath: join(process.cwd(), 'dist/universal-starter-v9/browser')
    }),

    HttpModule,
    CacheModule.register({
      ttl: 60 * 30, // seconds
      max: 1000 // maximum number of items in cache
    }),
    GraphQLModule.forRoot({
      playground: process.env.PROD === 'false',
      autoSchemaFile: true,
      cacheControl: true
    }),
    TypegooseModule.forRoot(`mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/animewatcher`, {
      useNewUrlParser: true,
      user: process.env.MONGO_INITDB_ROOT_USERNAME,
      pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
      authSource: 'admin'
    }),
    TypegooseModule.forFeature([Anime, Torrent])
  ],
  controllers: [StreamController, AnimelistController],
  providers: [
    SocketGateway,
    StreamService,
    AnimeRepositoryService,
    AnimeResolver,
    SeasonResolver,
    ObjectIdScalar,
    UpdateSchedulerService,
    AnimeMatcherService,
    TrackerFirst,
    AnimeFirst,
    NyaaTracker,
    TorrentSupplierService,
    TransformerA,
    TransformerB
  ]
})
export class ApplicationModule {}
