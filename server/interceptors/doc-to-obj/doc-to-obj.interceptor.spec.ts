import { DocToObjInterceptor } from './doc-to-obj.interceptor';

describe('DocToObjInterceptor', () => {
  it('should be defined', () => {
    expect(new DocToObjInterceptor()).toBeDefined();
  });
});
