import { DocToObjGraphqlInterceptor } from './doc-to-obj-graphql.interceptor';

describe('DocToObjGraphqlInterceptor', () => {
  it('should be defined', () => {
    expect(new DocToObjGraphqlInterceptor()).toBeDefined();
  });
});
