import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { getClassForDocument } from '@typegoose/typegoose';
import { Model, Document } from 'mongoose';

@Injectable()
export class DocToObjInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map(doc => {
        if (Array.isArray(doc)) {
          return doc.map(item => (item instanceof Model ? this.convertDocument(item) : item));
        }
        if (doc instanceof Model) {
          return this.convertDocument(doc);
        }
        return doc;
      })
    );
  }

  convertDocument(doc: Document) {
    const convertedDocument = doc.toObject();
    const DocumentClass = getClassForDocument(doc);
    Object.setPrototypeOf(convertedDocument, DocumentClass.prototype);
    return convertedDocument;
    }
}
