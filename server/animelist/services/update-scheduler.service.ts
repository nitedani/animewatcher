import { Injectable, HttpService } from '@nestjs/common';
import { switchMap, map, tap, delay, throttle, mergeMap, mapTo } from 'rxjs/operators';
import { Observable, interval, Subscription, of, from, timer, Subject } from 'rxjs';
import { Anime } from 'server/models/anime.interface';
import { AnimeMatcherService, TrackerFirst } from './anime-matcher.service';
import { NyaaTracker } from './nyaa.tracker.service';
import { TransformerB } from './torrent-transformer.service';
import { AnimeRepositoryService } from 'server/repository/anime-repository/anime-repository.service';
import { MalSeasonResponse, MalAnimeResponse, RecommendationResponse } from 'server/models/malresponse.interface';
import JikanTS from '../../jikants/src/index';
import { Seasons, Season } from '../../jikants/src/interfaces/season/Season';

@Injectable()
export class UpdateSchedulerService {
  constructor(
    private animeMatcher: AnimeMatcherService,
    private animeRepository: AnimeRepositoryService,
    private http: HttpService
  ) {}
  $handler: Subscription;

  startScheduler(): void {
    if (this.$handler) this.$handler.unsubscribe();
    this.$handler = interval(3600000).subscribe((next) => {
      from(this.animeRepository.find({ query: { season: '2021spring' } }))
        //this.getAnime(this.animeUrl)
        .pipe(
          switchMap((anime) =>
            this.animeMatcher.match(anime, {
              strategy: TrackerFirst,
              supplierOptions: {
                tracker: NyaaTracker,
                trackerOptions: { quality: '1080', user: 'subsplease' },
                transformer: TransformerB,
              },
            })
          ),
          switchMap((anime) =>
            this.animeMatcher.match(anime, {
              strategy: TrackerFirst,
              supplierOptions: {
                tracker: NyaaTracker,
                trackerOptions: { quality: '1080', user: 'Erai-raws' },
                transformer: TransformerB,
              },
            })
          ),
          switchMap((anime) =>
            this.animeMatcher.match(anime, {
              strategy: TrackerFirst,
              supplierOptions: {
                tracker: NyaaTracker,
                trackerOptions: { quality: '1080', user: 'Judas' },
                transformer: TransformerB,
              },
            })
          )
        )
        .subscribe((anime) => this.animeRepository.updateOrInsertAll(anime));
    });
  }

  updateNow() {
    from(this.animeRepository.find({ query: { season: '2021spring' } }))
      //this.getAnime(this.animeUrl)
      .pipe(
        switchMap((anime) =>
          this.animeMatcher.match(anime, {
            strategy: TrackerFirst,
            supplierOptions: {
              tracker: NyaaTracker,
              trackerOptions: { quality: '1080', user: 'subsplease' },
              transformer: TransformerB,
            },
          })
        ),
        switchMap((anime) =>
          this.animeMatcher.match(anime, {
            strategy: TrackerFirst,
            supplierOptions: {
              tracker: NyaaTracker,
              trackerOptions: { quality: '1080', user: 'Erai-raws' },
              transformer: TransformerB,
            },
          })
        ),
        switchMap((anime) =>
          this.animeMatcher.match(anime, {
            strategy: TrackerFirst,
            supplierOptions: {
              tracker: NyaaTracker,
              trackerOptions: { quality: '1080', user: 'Judas' },
              transformer: TransformerB,
            },
          })
        )
      )
      .subscribe((anime) => this.animeRepository.updateOrInsertAll(anime));
  }

  async fillDb() {
    const yearsToGet = [2021];
    const seasons: Seasons[] = ['spring'];
    const requests: Promise<Season>[] = [];
    yearsToGet.forEach((year) => {
      seasons.forEach((season) => {
        requests.push(JikanTS.Season.anime(year, season));
      });
    });

    from(requests)
      .pipe(
        mergeMap((request) => from(request).pipe(delay(1000)), 1),
        map((response) => Anime.fromMalResponse(response))
      )
      .subscribe((anime) => this.animeRepository.updateOrInsertAll(anime));
  }

  async fillAnimeDetails(anime: Anime) {
    const response = await JikanTS.Anime.byId(anime.malId);
    if (!response) throw new Error('MAL DOWN');
    return anime.setDetails(response);
  }

  async fillAnimeRecommendations(anime: Anime) {
    return await JikanTS.Anime.recommendations(anime.malId).then((response) => anime.setRecommendation(response));
  }

  stopScheduler(): void {
    this.$handler.unsubscribe();
  }
}
