import { Injectable, Type } from '@nestjs/common';
import { Tracker, TrackerOptions } from './nyaa.tracker.service';
import { Torrent } from 'server/models/torrent.interface';
import { TorrentTransformer } from './torrent-transformer.service';
import { ModuleRef } from '@nestjs/core';

export interface TorrentSupplierOptions {
  tracker: Type<Tracker>;
  trackerOptions: TrackerOptions;
  transformer: Type<TorrentTransformer>;
}

@Injectable()
export class TorrentSupplierService {
  constructor(private moduleRef: ModuleRef) {}
  async get(options: TorrentSupplierOptions): Promise<Torrent[]> {
    const tracker = this.moduleRef.get(options.tracker);
    const transformer = this.moduleRef.get(options.transformer);
    const torrents = await tracker.get(options.trackerOptions);
    return transformer.transform(torrents);
  }
}
