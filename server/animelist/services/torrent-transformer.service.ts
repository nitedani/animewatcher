import { Torrent } from 'server/models/torrent.interface';
import { Injectable } from '@nestjs/common';

enum ByteSize {
  GB = 1073741824,
  MB = 1048576,
  KB = 1024,
}
export interface TorrentTransformer {
  transform: (torrents: Torrent[]) => Torrent[];
}

@Injectable()
export class TransformerA implements TorrentTransformer {
  transform(torrents: Torrent[]): Torrent[] {
    torrents.forEach((torrent) => {
      const end = torrent.name.lastIndexOf('-');
      torrent
        .setEpisode(
          torrent.name
            .substring(end)
            .split(' ')
            .filter((x) => x !== '')[1]
        )
        .setName(torrent.name.substring(torrent.name.indexOf(' '), end));
    });
    return torrents;
  }
}
@Injectable()
export class TransformerB implements TorrentTransformer {
  transform(torrents: Torrent[]): Torrent[] {
    torrents.forEach((torrent) => {
      const name = torrent.name.replace(/\[.*?\]/g, '').trim();
      const nameMatch = name.match(/.*?(?=(?: [-(|]))/g);
      torrent.setName(
        nameMatch ? nameMatch.filter((str) => str !== '').reduce((acc, curr) => (acc += ` ${curr}`)) : name
      );

      const canContainEpisode = name.replace(/.*?(?=(?: [-(|]))/g, '').trim();
      const epMatchRegex = /(?<![0-9Sa-z.]|season|season )(?:e|ep)?[0-9]{1,3}(?:\.5)?(?:(?![0-9a-z)-]|\.)|(?=v\d))/gi;
      const epMatch = canContainEpisode.match(epMatchRegex);
      const seasonMatch = name.match(/((?<=season |season|s |s)\d{1,2})/i);

      const season = seasonMatch ? seasonMatch[0].trim() : '1';

      torrent.setSeason(season);
      const isBundle =
        (((epMatch && epMatch.length > 1) || seasonMatch) && torrent.fileSize / ByteSize.MB > 2000) ||
        torrent.fileSize / ByteSize.MB > 3000;
      torrent.setBundle(isBundle);

      const episode = isBundle ? 'Bundle' : epMatch ? epMatch[0] : '01';
      torrent.setEpisode(episode);
    });
    return torrents;
  }
}
