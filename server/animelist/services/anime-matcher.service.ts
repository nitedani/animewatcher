import { Injectable, Type } from '@nestjs/common';
import { Anime } from 'server/models/anime.interface';
import { Torrent, TorrentCategory } from 'server/models/torrent.interface';
import { TorrentSupplierService, TorrentSupplierOptions } from './torrent-supplier.service';
import { ModuleRef } from '@nestjs/core';
import { from } from 'rxjs';
import { mergeMap, tap, takeWhile, map, takeLast } from 'rxjs/operators';

export interface MatchOptions {
  strategy: Type<MatchStrategy>;
  supplierOptions: TorrentSupplierOptions;
  accuracy?: number;
}

export interface AnimeFirstOptions extends MatchOptions {
  strategy: Type<AnimeFirstStrategy>;
  supplierOptions: TorrentSupplierOptions;
  maxWords?: number;
  minWords?: number;
}

export interface MatchStrategy {
  match?: (
    animeToMatch: Anime[] | Anime,
    supplierOptions: TorrentSupplierOptions,
    accuracy?: number
  ) => Promise<Anime[] | Anime>;
  matchOne?: (animeToMatch: Anime, supplierOptions: TorrentSupplierOptions, accuracy?: number) => Promise<Anime>;
}

export interface AnimeFirstStrategy extends MatchStrategy {
  matchOne: (
    animeToMatch: Anime,
    supplierOptions: TorrentSupplierOptions,
    maxWords?: number,
    minWords?: number,
    accuracy?: number
  ) => Promise<Anime>;
}

@Injectable()
export class TrackerFirst implements MatchStrategy {
  constructor(private torrentSupplier: TorrentSupplierService) {}

  private animeNameChanged(anime: Anime, torrent: Torrent): boolean {
    const lastItem = anime.torrents.length - 1;
    if (lastItem < 0) {
      return false;
    }
    const lastName = anime.torrents[lastItem].getNormalizedName();
    const newName = torrent.getNormalizedName();
    if (newName === lastName) {
      return false;
    }
    return true;
  }

  public async match(
    animeToMatch: Anime[] | Anime,
    supplierOptions: TorrentSupplierOptions,
    accuracy = 9
  ): Promise<Anime[] | Anime> {
    let torrents = await this.torrentSupplier.get(supplierOptions);
    animeToMatch = [animeToMatch].flat(1);
    torrents = [torrents].flat(1);

    for (const anime of animeToMatch) {
      const animeMatchNames = anime.getMatchNames().map((str) => str.substring(0, accuracy));
      for (const torrent of torrents) {
        if (
          // Browsers can't play HEVC
          !torrent.hevc &&
          torrent.category.includes(TorrentCategory.ENGSUB) &&
          animeMatchNames.includes(torrent.getNormalizedName().substring(0, accuracy)) &&
          (!anime.torrents.map((x) => x.episode).includes(torrent.episode) ||
            anime.torrents.find((x) => x.episode === torrent.episode)?.getSeeders() * 2 < torrent.getSeeders()) &&
          // Movies get uploaded long after release date
          ((anime.type === 'Movie' && anime.airing_start.getTime() < torrent.uploadDate.getTime()) ||
            (!this.animeNameChanged(anime, torrent) &&
              anime.airing_start.getTime() - 302400000 < torrent.uploadDate.getTime() &&
              (!anime.aired?.to || torrent.uploadDate.getTime() < anime.aired.to.getTime() + 604800000)))
        ) {
          torrent.setId(anime.malId + torrent.episode);
          anime.addTorrent(torrent);
        }
      }
      anime.torrents.sort((a, b) => parseInt(a.episode, 10) - parseInt(b.episode, 10));
    }
    return animeToMatch.length === 1 ? animeToMatch[0] : animeToMatch;
  }
}

@Injectable()
export class AnimeFirst implements AnimeFirstStrategy {
  constructor(private torrentSupplier: TorrentSupplierService) {}
  private animeNameChanged(anime: Anime, torrent: Torrent): boolean {
    const lastItem = anime.torrents.length - 1;
    if (lastItem < 0) {
      return false;
    }
    const lastName = anime.torrents[lastItem].getNormalizedName();
    const newName = torrent.getNormalizedName();
    if (newName === lastName) {
      return false;
    }
    return true;
  }

  private seasonChanged(anime: Anime, torrent: Torrent) {
    if (!anime.related.Prequel.length && !anime.related.Sequel.length) return false;
    const lastItem = anime.torrents.length - 1;
    if (lastItem < 0) {
      return false;
    }
    const lastSeason = anime.torrents[lastItem].season;
    const newSeason = torrent.season;
    return newSeason !== lastSeason;
  }

  private continueLooking(anime: Anime) {
    return (
      (anime.status === 'Finished Airing' ? anime.torrents.length < anime.episodes : !anime.torrents.length) ||
      anime.torrents.some((torr) => torr.getSeeders() < 10)
    );
  }

  subCompare(needle: string, haystack: string, min_substring_length = 3) {
    // Search possible substrings from largest to smallest:
    for (let i = needle.length; i >= min_substring_length; i--) {
      for (let j = 0; j <= needle.length - i; j++) {
        let substring = needle.substr(j, i);
        let k = haystack.indexOf(substring);
        if (k != -1) {
          return {
            found: true,
            substring: substring,
            needleIndex: j,
            haystackIndex: k,
          };
        }
      }
    }
    return {
      found: false,
    };
  }

  private removeLongestCommonPart(str1: string, str2: string) {
    const longestCommonPart = this.subCompare(str1, str2).substring;
    return (
      longestCommonPart && {
        str1: str1.replace(longestCommonPart, '').trim(),
        str2: str2.replace(longestCommonPart, '').trim(),
      }
    );
  }

  private seasonMatches(anime: Anime, torrent: Torrent): boolean {
    if (!anime.related.Prequel.length && !anime.related.Sequel.length) return true;
    const seasonNumberRegex = /(?<=season |season|s |^s| s)\d{1,2}|(?<!ep|episode)^ *\d{1,2}(?!\d|bit)/gim;
    const seasonRegex = / ?(?:season |season|s |s)(?=\d| \d)/gim;
    const normalizeWhitespace = / *[()\[\]:, \-] */g;
    const otherSeasonNames = [
      anime.related.Prequel.map((prequel) => prequel.name.replace(normalizeWhitespace, ' ')),
      anime.related.Sequel.map((sequel) => sequel.name.replace(normalizeWhitespace, ' ')),
    ].flat();
    const animeNameReplaced = anime.title.replace(normalizeWhitespace, ' ');

    const longestSubStringResults = otherSeasonNames
      .map((name) => this.subCompare(animeNameReplaced.replace(seasonRegex, ''), name.replace(seasonRegex, '')))
      .filter((result) => result.found && result.substring !== animeNameReplaced);
    const longestSubString =
      longestSubStringResults.length &&
      longestSubStringResults.sort((a, b) => b.substring.length - a.substring.length)[0].substring;

    let animeSeasonMatchString = longestSubString && animeNameReplaced.replace(longestSubString, '').trim();
    let torrentSeasonMatchString =
      longestSubString &&
      this.removeLongestCommonPart(longestSubString, torrent.name.replace(normalizeWhitespace, ' '))?.str2;

    if (animeSeasonMatchString && torrentSeasonMatchString) {
      let result = this.removeLongestCommonPart(
        animeSeasonMatchString.replace(seasonRegex, ''),
        torrentSeasonMatchString.replace(seasonRegex, '')
      );
      while (result) {
        animeSeasonMatchString = result.str1;
        torrentSeasonMatchString = result.str2;

        result = this.removeLongestCommonPart(
          animeSeasonMatchString.replace(seasonRegex, ''),
          torrentSeasonMatchString.replace(seasonRegex, '')
        );
      }

      if (animeSeasonMatchString === torrentSeasonMatchString) return true;

      const animeSeasonMatch = seasonNumberRegex.exec(animeSeasonMatchString);
      const torrentSeasonMatch = seasonNumberRegex.exec(torrentSeasonMatchString);

      const animeSeason = animeSeasonMatch && animeSeasonMatch[0].trim();
      const torrentSeason = torrentSeasonMatch && torrentSeasonMatch[0].trim();

      return (
        (animeSeason && torrentSeason && animeSeason === torrentSeason) ||
        this.subCompare(
          animeSeasonMatchString.replace(seasonRegex, ''),
          torrentSeasonMatchString.replace(seasonRegex, ''),
          2
        ).found
      );
    } else {
      // Fallback to checking torrent upload date
      return !anime.aired.to || torrent.uploadDate.getTime() < anime.aired.to.getTime() + 604800000;
    }
  }

  public async matchOne(
    animeToMatch: Anime,
    supplierOptions: TorrentSupplierOptions,
    maxWords: number,
    minWords: number,
    accuracy = 9
  ): Promise<Anime> {
    console.log([animeToMatch.title, animeToMatch.title_english, animeToMatch.synonyms].flat(1).filter((_) => !!_));

    const animeMatchNames = animeToMatch.getMatchNames().map((str) => str.substring(0, accuracy));

    if (!this.continueLooking(animeToMatch)) return animeToMatch;
    return await from(
      [
        ...new Set(
          ([animeToMatch.title, animeToMatch.title_english, animeToMatch.synonyms]
            .flat(1)
            .filter((_) => !!_) as string[]).flatMap((str) => {
            const searchWords: string[] = [];
            const split = str.split(/[^a-zA-Z0-9!]/).filter((x) => x !== '');
            const currMaxWords = Math.min(split.length, maxWords);

            for (let i = currMaxWords; i >= minWords; i--) {
              searchWords.push(split.slice(0, i).join(' '));
            }
            return searchWords;
          })
        ),
      ]
        .filter((a) => a.length > 1)
        .sort((a, b) => b.length - a.length)
    )
      .pipe(
        mergeMap((searchTerm) => {
          supplierOptions.trackerOptions.term = searchTerm;
          return this.torrentSupplier.get(supplierOptions);
        }, 1),
        map((torrents) => {
          for (const torrent of torrents) {
            if (
              // Browsers can't play HEVC
              !torrent.hevc &&
              torrent.category.includes(TorrentCategory.ENGSUB) &&
              animeMatchNames.includes(torrent.getNormalizedName().substring(0, accuracy)) &&
              // With more seeders videos load quicker, replace slow links
              (!animeToMatch.torrents.map((x) => x.episode).includes(torrent.episode) ||
                (animeToMatch.torrents.find((x) => x.episode === torrent.episode)?.seeders - 10) * 2 <
                  torrent.seeders) &&
              (!torrent.bundle || animeToMatch.type === 'Movie') &&
              // Movies get uploaded long after release date
              ((animeToMatch.type === 'Movie' && animeToMatch.aired.from.getTime() < torrent.uploadDate.getTime()) ||
                (!this.seasonChanged(animeToMatch, torrent) &&
                  animeToMatch.aired.from.getTime() - 302400000 < torrent.uploadDate.getTime() &&
                  this.seasonMatches(animeToMatch, torrent)))
            ) {
              torrent.setId(animeToMatch.malId + torrent.episode);
              animeToMatch.addTorrent(torrent);
            }
          }
          animeToMatch.torrents.sort((a, b) => parseInt(a.episode, 10) - parseInt(b.episode, 10));
          return animeToMatch;
        }),

        // If the anime is finished airing, we dont stop until found all episodes.
        // Otherwise look until every found episode has enough seeders, with 10 the video should start playing quick enough.
        takeWhile((anime) => this.continueLooking(anime), true),
        takeLast(1)
      )
      .toPromise();
  }
}

@Injectable()
export class AnimeMatcherService {
  constructor(private moduleRef: ModuleRef) {}

  async match(animeToMatch: Anime | Anime[], options: MatchOptions): Promise<Anime | Anime[]> {
    const matcher = this.moduleRef.get(options.strategy);
    return await matcher.match(animeToMatch, options.supplierOptions, options.accuracy);
  }

  async matchOne(animeToMatch: Anime, options: AnimeFirstOptions): Promise<Anime> {
    const matcher = this.moduleRef.get(options.strategy);
    return await matcher.matchOne(
      animeToMatch,
      options.supplierOptions,
      options.maxWords,
      options.minWords,
      options.accuracy
    );
  }
}
