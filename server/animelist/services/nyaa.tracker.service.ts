import { Injectable } from '@nestjs/common';
import { Torrent } from 'server/models/torrent.interface';
import { si, pantsu } from '../../nyaapi/src';

export interface TrackerOptions {
  quality?: '1080' | '720' | '480';
  term?: string;
  user?: string;
  h?: boolean;
}

export interface Tracker {
  get: (options: TrackerOptions) => Promise<Torrent[]>;
}

@Injectable()
export class NyaaTracker implements Tracker {
  public async get(options: TrackerOptions): Promise<Torrent[]> {
    if (!options.term) options.term = '';
    const result: Torrent[] = [];
    console.log(options.quality ? options.term + ' ' + options.quality : options.term);
    if (options.user) {
      await si
        .searchAllByUser({
          url: options.h ? 1 : 0,
          user: options.user,
          term: options.quality ? options.term + ' ' + options.quality : options.term,
          sort: 'seeders'
        })
        .then(data => {
          // console.log(data);
          data.forEach(nyaaTorrent => {
            result.push(Torrent.fromNyaaResponse(nyaaTorrent));
          });
        })
        .catch(err => console.log(err));
    } else {
      await si
        .searchAll({
          url: options.h ? 1 : 0,
          term: options.quality ? options.term + ' ' + options.quality : options.term,
          sort: 'seeders'
        })
        .then(data => {
          // console.log(data);
          data.forEach(nyaaTorrent => {
            result.push(Torrent.fromNyaaResponse(nyaaTorrent));
          });
        })
        .catch(err => console.log(err));
    }
    return result;
  }
}
