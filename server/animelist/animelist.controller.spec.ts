import { Test, TestingModule } from '@nestjs/testing';
import { AnimelistController } from './animelist.controller';

describe('Animelist Controller', () => {
  let controller: AnimelistController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AnimelistController],
    }).compile();

    controller = module.get<AnimelistController>(AnimelistController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
