import { Controller, Get, UseInterceptors } from '@nestjs/common';
import { DocToObjInterceptor } from 'server/interceptors/doc-to-obj/doc-to-obj.interceptor';
import { UpdateSchedulerService } from './services/update-scheduler.service';

@Controller('api/animelist')
@UseInterceptors(DocToObjInterceptor)
export class AnimelistController {
  constructor(private updateScheduler: UpdateSchedulerService) {}

  @Get('startupdate')
  startUpdate(): string {
    this.updateScheduler.startScheduler();
    return 'ok';
  }

  @Get('filldb')
  filldb(): string {
    this.updateScheduler.fillDb();
    return 'ok';
  }

  @Get('updateNow')
  updateNow(): string {
    this.updateScheduler.updateNow();
    return 'ok';
  }
}
