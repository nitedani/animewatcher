// Imports

const got = require('got')
import PMemoize from 'p-memoize';
import PQueue from 'p-queue';
const pino = require('pino')
const LRU = require('quick-lru')
// package.json
// Constants
export const baseUrl = `http://${process.env.JIKAN_HOST}:${process.env.JIKAN_PORT}/v3`;
export const queue = new PQueue({ concurrency: 2 });

// Custom http client
const http = got.extend({
  baseUrl,
  json: true
});

// Memoized http client
export const api = PMemoize(http, { cache: new LRU({ maxSize: 1000 }) });

// Fast JSON logger
export const Logger = pino({
  name: 'jikants',
  prettyPrint: true
});
