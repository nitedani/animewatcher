import { Resolver, Query, ResolveProperty, Args, Parent } from '@nestjs/graphql';
import { Anime, GetAnimeArgs, NextEpisode, SortType } from 'server/models/anime.interface';
import { Torrent } from 'server/models/torrent.interface';
import { AnimeRepositoryService } from 'server/repository/anime-repository/anime-repository.service';
import { UseInterceptors } from '@nestjs/common';
import { DocToObjGraphqlInterceptor } from 'server/interceptors/doc-to-obj/doc-to-obj-graphql.interceptor';
import { Int, MiddlewareFn, UseMiddleware, NextFn } from 'type-graphql';
import { AnimeMatcherService, TrackerFirst, AnimeFirst } from 'server/animelist/services/anime-matcher.service';
import { of, from } from 'rxjs';
import { mergeMap, concatAll, tap, map, mergeAll, combineAll, toArray, flatMap } from 'rxjs/operators';
import { NyaaTracker } from 'server/animelist/services/nyaa.tracker.service';
import { TransformerB } from 'server/animelist/services/torrent-transformer.service';
import { UpdateSchedulerService } from 'server/animelist/services/update-scheduler.service';
import { CacheScope } from 'apollo-cache-control';
import { LivechartScraper } from 'server/livechart/scrape';

export function CacheControl(maxAge: number = 60): MiddlewareFn {
  return async ({ info: { cacheControl }, args }, next) => {
    const result = await next();
    console.log(result);
    return result;
    // if (args['sortBy'] !== undefined) cacheControl.setCacheHint({ maxAge, scope: CacheScope.Public });
  };
}

@Resolver((of) => Anime)
@UseInterceptors(DocToObjGraphqlInterceptor)
export class AnimeResolver {
  constructor(
    private readonly animeRepository: AnimeRepositoryService,
    private readonly matcherService: AnimeMatcherService,
    private updateScheduler: UpdateSchedulerService
  ) {}

  diff_weeks(dt2: Date, dt1: Date) {
    if (!dt2 || !dt1) return Infinity;
    let diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60 * 60 * 24 * 7;
    return Math.abs(Math.round(diff));
  }

  @Query((returns) => [Anime])
  @UseMiddleware(CacheControl())
  async anime(@Args() args: GetAnimeArgs) {
    const mongoQuery = {
      query: {
        ...(args.malId &&
          ((args.malId as number[]).length > 1 ? { malId: { $in: args.malId } } : { malId: args.malId[0] })),
        ...(args.title && { title: new RegExp(args.title.toString().replace(/[^a-zA-Z0-9 ]/g, ''), 'i') }),
        ...(args.season && { season: args.season }),
        ...(args.r18 !== undefined && { r18: args.r18 }),
        ...(args.torrents && { torrents: { $exists: true, $ne: [] } }),
        ...(args.genres && { genres: { $all: args.genres.map((genre) => ({ $elemMatch: { name: genre } })) } }),
        ...(!args.continuing &&
          args.season && {
            airing_start: { $gte: new Date((parseInt(/\d{4}/.exec(args.season)[0], 10) - 1).toString()) },
          }),
        members: { $gt: 1000 },
      },
      ...(args.sortBy && { sort: { key: args.sortBy, order: args.order } as SortType }),
      ...(args.limit && { limit: args.limit }),
    };

    const animeArr = await this.animeRepository.find(mongoQuery);

    if (args.fetchDetails) {
      const updates: Anime[] = [];
      for (let i = 0; i < animeArr.length; i++) {
        if (args.fetchDetails === 'force' || this.diff_weeks(animeArr[i].detailsUpdated, new Date(Date.now())) > 1) {
          const update = await this.updateScheduler.fillAnimeDetails(animeArr[i]);
          if (update) {
            animeArr[i] = update;
            updates.push(animeArr[i]);
          } else {
            break;
          }
        }
      }
      if (updates.length) this.animeRepository.updateOrInsertAll(updates);
    }

    if (args.fetchRecommendations) {
      const updates: Anime[] = [];
      for (let i = 0; i < animeArr.length; i++) {
        if (
          args.fetchRecommendations === 'force' ||
          this.diff_weeks(animeArr[i].recommendationsUpdated, new Date(Date.now())) > 4
        ) {
          animeArr[i] = await this.updateScheduler.fillAnimeRecommendations(animeArr[i]);
          updates.push(animeArr[i]);
        }
      }
      if (updates.length) this.animeRepository.updateOrInsertAll(updates);
    }

    if (args.fetchNewEpisodes) {
      const toReturn = await from(animeArr)
        .pipe(
          mergeMap(
            (anime) =>
              this.diff_weeks(anime.detailsUpdated, new Date(Date.now())) > 4
                ? this.updateScheduler.fillAnimeDetails(anime)
                : of(anime),
            1
          ),
          mergeMap(
            (anime) =>
              args.fetchNewEpisodes === 'force' ||
              (this.diff_weeks(anime.episodesUpdated, new Date(Date.now())) > 4 &&
                anime.airing_start?.getTime() < new Date(Date.now()).getTime() &&
                (!anime.episodes || anime.episodes > anime.torrents.length))
                ? of(anime).pipe(
                    flatMap((a) =>
                      this.matcherService.matchOne(a, {
                        strategy: AnimeFirst,
                        supplierOptions: {
                          tracker: NyaaTracker,
                          trackerOptions: { quality: '1080', user: 'Erai-raws' },
                          transformer: TransformerB,
                        },
                        maxWords: 2,
                        minWords: 1,
                      })
                    ),

                    flatMap((a) =>
                      this.matcherService.matchOne(a, {
                        strategy: AnimeFirst,
                        supplierOptions: {
                          tracker: NyaaTracker,
                          trackerOptions: { quality: '1080', user: 'subsplease' },
                          transformer: TransformerB,
                        },
                        maxWords: 2,
                        minWords: 1,
                      })
                    ),
                    flatMap((a) =>
                      this.matcherService.matchOne(a, {
                        strategy: AnimeFirst,
                        supplierOptions: {
                          tracker: NyaaTracker,
                          trackerOptions: { quality: '1080', user: 'Judas' },
                          transformer: TransformerB,
                        },
                        maxWords: 2,
                        minWords: 1,
                      })
                    ),
                    flatMap((a) =>
                      this.matcherService.matchOne(a, {
                        strategy: AnimeFirst,
                        supplierOptions: {
                          tracker: NyaaTracker,
                          trackerOptions: { quality: '1080', user: 'motbob' },
                          transformer: TransformerB,
                        },
                        maxWords: 2,
                        minWords: 1,
                      })
                    ),
                    flatMap((a) =>
                      this.matcherService.matchOne(a, {
                        strategy: AnimeFirst,
                        supplierOptions: {
                          tracker: NyaaTracker,
                          trackerOptions: { quality: '1080' },
                          transformer: TransformerB,
                        },
                        maxWords: 2,
                        minWords: 1,
                      })
                    ),

                    tap((a) => a.setEpisodesUpdated())
                  )
                : of(anime),
            1
          ),
          toArray()
        )
        .toPromise();

      this.animeRepository.updateOrInsertAll(toReturn);
      return [toReturn].flat(1);
    }
    return animeArr;
  }

  @ResolveProperty('nextEpisode', (returns) => NextEpisode, { nullable: true })
  nextEpisode(@Parent() anime: Anime) {
    const { torrents } = anime;
    if (torrents.length > 1 && (!anime.episodes || anime.episodes > torrents.length)) {
      let differences: number[] = [];
      for (let index = 1; index < torrents.length; index++) {
        differences.push(torrents[index].uploadDate.getTime() - torrents[index - 1].uploadDate.getTime());
      }
      differences = differences.filter(
        (difference) => difference > 1000 * 60 * 60 * 24 * 6 && difference < 1000 * 60 * 60 * 24 * 8
      );
      if (differences.length) {
        const nextEpTime =
          torrents[torrents.length - 1].uploadDate.getTime() +
          differences.reduce((a, b) => a + b, 0) / differences.length;
        const remainingDays = (nextEpTime - Date.now()) / 1000 / 60 / 60 / 24;
        return remainingDays > 0
          ? {
              days: Math.floor(remainingDays),
              hours: Math.floor((remainingDays * 24) % 24),
            }
          : remainingDays > -1
          ? { days: 0, hours: 0 }
          : null;
      } else {
        return null;
      }
    } else if (torrents.length === 1 && (!anime.episodes || anime.episodes > 1)) {
      const nextEpTime = torrents[0].uploadDate.getTime() + 604800000;
      const remainingDays = (nextEpTime - Date.now()) / 1000 / 60 / 60 / 24;
      return remainingDays > 0
        ? {
            days: Math.floor(remainingDays),
            hours: Math.floor((remainingDays * 24) % 24),
          }
        : remainingDays > -1
        ? { days: 0, hours: 0 }
        : null;
    } else if (torrents.length === 0) {
      const firstEpTime = LivechartScraper.releases[anime.malId];
      if (firstEpTime) {
        const remainingDays = (firstEpTime * 1000 - Date.now()) / 1000 / 60 / 60 / 24;
        return remainingDays > 0
          ? {
              days: Math.floor(remainingDays),
              hours: Math.floor((remainingDays * 24) % 24),
            }
          : remainingDays > -1
          ? { days: 0, hours: 0 }
          : null;
      } else {
        return null;
      }
    }
  }
}
