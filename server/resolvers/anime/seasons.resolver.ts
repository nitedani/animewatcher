import { Season } from 'server/models/season.interface';
import { Resolver, Query, ResolveProperty, Args, Parent } from '@nestjs/graphql';
import { AnimeRepositoryService } from 'server/repository/anime-repository/anime-repository.service';
import { UseInterceptors } from '@nestjs/common';
import { DocToObjGraphqlInterceptor } from 'server/interceptors/doc-to-obj/doc-to-obj-graphql.interceptor';


@Resolver(of => Season)
@UseInterceptors(DocToObjGraphqlInterceptor)
export class SeasonResolver {
  constructor(private readonly animeRepository: AnimeRepositoryService) {}


  @Query(returns => [Season])
  async season() {
    return await this.animeRepository.getSeasons();
  }
}