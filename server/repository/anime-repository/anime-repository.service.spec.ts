import { Test, TestingModule } from '@nestjs/testing';
import { AnimeRepositoryService } from './anime-repository.service';

describe('AnimeRepositoryService', () => {
  let service: AnimeRepositoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AnimeRepositoryService],
    }).compile();

    service = module.get<AnimeRepositoryService>(AnimeRepositoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
