import { Injectable } from '@nestjs/common';
import { Anime, SortType } from 'server/models/anime.interface';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Torrent } from 'server/models/torrent.interface';
import { BulkWriteResult } from 'mongodb';
import { Season } from 'server/models/season.interface';

@Injectable()
export class AnimeRepositoryService {
  constructor(
    @InjectModel(Anime) private readonly animeModel: ReturnModelType<typeof Anime>,
    @InjectModel(Torrent) private readonly torrentModel: ReturnModelType<typeof Torrent>
  ) {}

  async getSeasons(): Promise<Season | Season[]> {
    const seasons: string[] = await this.animeModel.distinct('season').exec();
    const seasonsMapped: Season[] = seasons.map(season => ({
      value: season,
      viewValue: season.substr(0, 4) + ' ' + season.substr(4).toUpperCase(),
      year: season.substr(0, 4),
      season: season.substr(4).toUpperCase()
    }));
    return seasonsMapped;
  }

  async create(anime: Anime | Anime[]): Promise<Anime | Anime[]> {
    if (Array.isArray(anime)) {
      const toDb: any[] = [];
      anime.forEach(a => {
        toDb.push(new this.animeModel(a));
      });
      await this.animeModel.collection.insertMany(toDb);
      return toDb;
    } else {
      const createdAnime = new this.animeModel(anime);
      return await createdAnime.save();
    }
  }

  async findById(id: number, projection?: {}): Promise<Anime> {
    return await this.animeModel.findOne({ malId: id }, projection).exec();
  }

  async findOne(query: {}, projection?: {}): Promise<Anime> {
    return await this.animeModel.findOne(query, projection).exec();
  }

  async find(opts: { query?: {}; projection?: {}; sort?: SortType; limit?: number }): Promise<Anime[]> {
    if (opts.sort) {
      const sortOpts = {};
      sortOpts[opts.sort.key] = opts.sort.order === 'asc' ? 1 : -1;
      return await this.animeModel
        .find(opts.query, opts.projection)
        .sort(sortOpts)
        .limit(opts.limit)
        .exec();
    } else {
      return await this.animeModel
        .find(opts.query, opts.projection)
        .limit(opts.limit)
        .exec();
    }
  }

  async updateOrInsertAll(anime: Anime[] | Anime) {
    let updateResponse: BulkWriteResult | [] = [];
    let createResponse: Anime | Anime[] = [];

    if (!Array.isArray(anime)) anime = [anime];
    const fromDb = await this.find({ query: { malId: { $in: anime.map(a => a.malId) } } });
    const idsInDb = fromDb.map(b => b.malId);
    const newAnime = anime.filter(a => !idsInDb.includes(a.malId));
    if (newAnime.length > 0) createResponse = await this.create(newAnime);

    const updates = anime
      .filter(a => idsInDb.includes(a.malId))
      .map(a => ({
        malId: a.malId,
        properties: {
          members: a.members,
          score: a.score,
          image_url: a.image_url,
          continuing: a.continuing,
          synopsis: a.synopsis,
          season: a.season,
          title_english: a.title_english,
          status: a.status,
          recommendations: a.recommendations,
          aired: a.aired,
          detailsUpdated: a.detailsUpdated,
          recommendationsUpdated: a.recommendationsUpdated,
          episodesUpdated: a.episodesUpdated,
          rank: a.rank,
          related: a.related,
          studios: a.studios,
          premiered: a.premiered,
          trailer_url: a.trailer_url
        },
        synonyms: a.synonyms,
        newEpisodes: a.torrents
          .filter(
            torrent =>
              !fromDb
                .find(b => b.malId === a.malId)
                .getTorrents()
                .map(t => t.episode)
                .includes(torrent.episode)
          )
          .map(newTorrent => new this.torrentModel(newTorrent))
      }));

    if (0 < updates.length) {
      const bulk = this.animeModel.collection.initializeOrderedBulkOp();
      updates.forEach(update => {
        if (update.newEpisodes.length) update.properties.episodesUpdated = new Date(Date.now());
        bulk.find({ malId: update.malId }).updateOne({
          $push: { torrents: { $each: update.newEpisodes, $sort: { episode: 1 } } },
          $addToSet: { synonyms: { $each: update.synonyms } },
          $set: update.properties
        });
      });
      updateResponse = await bulk.execute();
    }
    return { createResponse, updateResponse };
  }
}
