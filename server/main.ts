import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './app.module';
import * as fs from 'fs';
import * as compression from 'compression';
const helmet = require('helmet');
import { ExpressAdapter } from '@nestjs/platform-express';
import { DiskManager } from './stream/disk.service';
import { LivechartScraper } from './livechart/scrape';
import * as path from 'path';

async function bootstrap() {
  const express = new ExpressAdapter();
  express.use(helmet({ contentSecurityPolicy: false }));
  express.use(compression());

  const app = await NestFactory.create(
    ApplicationModule,
    express,
    /*
    process.env.PROD === 'true'
      ? {
          httpsOptions: {
            key: fs.readFileSync(path.join(process.cwd(), process.env.PRIVKEY)),
            cert: fs.readFileSync(path.join(process.cwd(), process.env.CERT)),
          },
        }
      : {}
      */
  );

  app.enableCors({
    methods: 'GET',
    maxAge: 3600,
  });

  //await LivechartScraper.initLivechart();
  await DiskManager.initFolders();
  await app.listen(process.env.PORT || 4200);
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.

declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = (mainModule && mainModule.filename) || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  bootstrap().catch((err) => console.error(err));
}
