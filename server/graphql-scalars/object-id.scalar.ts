import { ObjectId } from 'mongodb';
import { Scalar, CustomScalar } from '@nestjs/graphql';
import { Kind, ValueNode } from 'graphql';

@Scalar('ObjectId', type => ObjectId)
export class ObjectIdScalar implements CustomScalar<string, ObjectId> {
  description: 'Mongo object id scalar type';
  parseValue(value: string) {
    return new ObjectId(value); // value from the client input variables
  }
  serialize(value: ObjectId) {
    return value.toHexString(); // value sent to the client
  }
  parseLiteral(ast) {
    if (ast.kind === Kind.STRING) {
      return new ObjectId(ast.value); // value from the client query
    }
    return null;
  }
}
