declare const __webpack_require__: any;
declare const __non_webpack_require__: NodeRequire;
const requireFunc = typeof __webpack_require__ === 'function' ? __non_webpack_require__ : require;
import * as got from 'got';
import puppeteer from 'puppeteer-extra';
import { join } from 'path';
const ChromeRuntimePlugin = requireFunc('puppeteer-extra-plugin-stealth/evasions/chrome.runtime');
const ConsoleDebugPlugin = requireFunc('puppeteer-extra-plugin-stealth/evasions/console.debug');
const IFrameContentWindowPlugin = requireFunc('puppeteer-extra-plugin-stealth/evasions/iframe.contentWindow');
const MediaCodecsPlugin = requireFunc('puppeteer-extra-plugin-stealth/evasions/media.codecs');
const NavigatorLanguagesPlugin = requireFunc('puppeteer-extra-plugin-stealth/evasions/navigator.languages');
const NavigatorPermissionsPlugin = requireFunc('puppeteer-extra-plugin-stealth/evasions/navigator.permissions');
const NavigatorPlugins = requireFunc('puppeteer-extra-plugin-stealth/evasions/navigator.plugins');
const WebdriverPlugin = requireFunc('puppeteer-extra-plugin-stealth/evasions/navigator.webdriver');
const UserAgent = requireFunc('puppeteer-extra-plugin-stealth/evasions/user-agent-override');
const WebglVendorPlugin = requireFunc('puppeteer-extra-plugin-stealth/evasions/webgl.vendor');
const WindowOuterDimensionsPlugin = requireFunc('puppeteer-extra-plugin-stealth/evasions/window.outerdimensions');
import { from } from 'rxjs';
import { mergeMap, toArray, map, filter } from 'rxjs/operators';
import * as cheerio from 'cheerio';
import { Browser } from 'puppeteer';
import * as proxyChain from 'proxy-chain';

puppeteer
  .use(ChromeRuntimePlugin())
  .use(ConsoleDebugPlugin())
  .use(IFrameContentWindowPlugin())
  .use(MediaCodecsPlugin())
  .use(NavigatorLanguagesPlugin())
  .use(NavigatorPermissionsPlugin())
  .use(NavigatorPlugins())
  .use(WebdriverPlugin())
  .use(WebglVendorPlugin())
  .use(WindowOuterDimensionsPlugin())
  .use(UserAgent());

export class CrunchySubScraper {
  static languageMap = {
    enUS: 'eng',
    esLA: 'spala',
    esES: 'spa',
    frFR: 'fre',
    ptBR: 'por',
    arME: 'ara',
    itIT: 'ita',
    deDE: 'ger',
    ruRU: 'rus',
  };

  public static async getSubs(malId: string | number, episode: string | number) {
    if (typeof episode === 'string') episode = parseInt(episode, 10);

    let browser: Browser = null;
    let newProxyUrl: any;

    async function cleanUp() {
      if (browser) {
        await browser.close();
        browser = null;
      }
      if (newProxyUrl) {
        await proxyChain.closeAnonymizedProxy(newProxyUrl, true);
        newProxyUrl = null;
      }
    }
    console.log(episode);

    try {
      const api = `https://api.malsync.moe/mal/anime/${malId}`;
      const response = JSON.parse((await got(api, { timeout: 3000 })).body);
      newProxyUrl = await proxyChain.anonymizeProxy(process.env.PROXY_URL);
      const crunchyPage =
        response['Sites'] && response['Sites']['Crunchyroll'] && Object.values(response['Sites']['Crunchyroll'])[0];
      console.log('Crunchypage is', crunchyPage);
      if (!crunchyPage) {
        await cleanUp();
        return [];
      }

      const crunchyUrl = crunchyPage['url'];

      browser = await puppeteer.launch({
        headless: true,
        args: ['--no-sandbox', '--disable-setuid-sandbox', `--proxy-server=${newProxyUrl}`],
        executablePath: process.env.CHROME_BIN,
      });

      console.log('Launched puppeteer');
      const page = await browser.newPage();

      console.log('Opened new page');
      const crunchyResponse = await (await page.goto(crunchyUrl))?.text();

      // console.log(crunchyResponse);
      const hasVerification = crunchyResponse.includes('Please complete the security');
      console.log('Response has verification =', hasVerification);

      if (!crunchyResponse || crunchyResponse.includes('Please complete the security')) {
        await cleanUp();
        return [];
      }

      const crunchyResponseSrc = crunchyResponse;

      let $ = cheerio.load(crunchyResponseSrc);

      const selected = $('.list-of-seasons .hover-bubble.group-item a')
        .filter((a, element) => !element.attribs['title'].includes('Dub'))
        .map((a, element) => element.attribs['href'])
        .get();

      const episodeUrl = selected.find((url) => url.includes(`episode-${episode}-`));
      console.log(episodeUrl);
      if (!episodeUrl) {
        await cleanUp();
        return [];
      }
      const crunchyEpisodeResponse = (await page.goto('https://www.crunchyroll.com/' + episodeUrl))?.text();

      if (!crunchyEpisodeResponse) {
        await cleanUp();
        return [];
      }

      $ = cheerio.load(await crunchyEpisodeResponse);

      const subtitleMatch = /(?<="subtitles": ?)\[.*\]/.exec(
        $('#showmedia_video_box script:not([src])')[0].children[0].data as string
      );

      if (!subtitleMatch) {
        await cleanUp();
        return [];
      }
      const subtitles: { language: string; url: string; title: string; format: string }[] = JSON.parse(
        subtitleMatch[0]
      );
      const files = await from(subtitles)
        .pipe(
          mergeMap(
            async (sub) => ({
              file: (await got(sub.url)).body,
              language: CrunchySubScraper.languageMap[sub.language] as string,
            }),
            1
          ),
          filter((file) => !!file.language),
          toArray()
        )
        .toPromise();
      await browser.close();
      await proxyChain.closeAnonymizedProxy(newProxyUrl, true);
      return files;
    } catch (error) {
      console.log(error);
      await cleanUp();
      return [];
    }
  }
}
/*
async function main() {


  const files = await CrunchySubScraper.getSubs(40221, 1);
}
main();
*/
