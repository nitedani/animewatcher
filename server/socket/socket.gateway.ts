import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect
} from '@nestjs/websockets';
import { Logger, Inject, forwardRef } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { Stream } from '../stream/stream.service';
import { Subject, Observable } from 'rxjs';
import { AnimeRepositoryService } from 'server/repository/anime-repository/anime-repository.service';
import { DiskManager } from 'server/stream/disk.service';

@WebSocketGateway()
export class SocketGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  constructor(private readonly animeRepository: AnimeRepositoryService) {}

  public startStream$ = new Subject<Stream>();
  public reloadSubs$ = new Subject<string>();
  public socketDisconnect$ = new Subject<string>();
  public stopStream$ = new Subject<string>();

  @WebSocketServer() server: Server;
  private logger: Logger = new Logger('SocketGateway');

  @SubscribeMessage('startstream')
  startStream(client: Socket, opts: { torrentId: string; malId: string; episode: string }): void {
    console.log(client.id, opts.torrentId);

    this.animeRepository.findOne({ torrents: { $elemMatch: { uniqueID: opts.torrentId } } }).then(async anime => {
      if (anime) {
        const torrent = anime.torrents.find(t => t.uniqueID === opts.torrentId);
        const url = torrent.links.magnet;
        const hash = /(?<=btih:)([A-F\d]+)/im.exec(url)[0];
        await DiskManager.startWatching(hash, torrent.fileSize);
        console.log(url);
        if (url) this.startStream$.next({ socketId: client.id, url, malId: opts.malId, episodeId: opts.episode });
      }
    });
  }

  @SubscribeMessage('stopstream')
  stopStream(client: Socket): void {
    this.stopStream$.next(client.id);
  }

  @SubscribeMessage('reloadsubs')
  reloadSubs(client: Socket): void {
    this.reloadSubs$.next(client.id);
  }

  sendMessage(id: string, event: string, message: any) {
    this.server.to(id).emit(event, JSON.stringify(message));
  }

  afterInit(server: Server) {
    this.logger.log('Init');
  }

  handleDisconnect(client: Socket) {
    this.socketDisconnect$.next(client.id);
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  handleConnection(client: Socket, ...args: any[]) {
    this.logger.log(`Client connected: ${client.id}`);
  }
}
