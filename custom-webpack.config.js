const webpack = require('webpack');

module.exports = {
  module: {
    rules: [
      {
        // regex for the files that are problematic
        test: /clone-deep.*/,
        loader: 'string-replace-loader',
        options: {
          // match a require function call where the argument isn't a string
          // also capture the first character of the args so we can ignore it later
          search: 'require[(]([^\'"])',
          // replace the 'require(' with a '__non_webpack_require__(', meaning it will require the files at runtime
          // $1 grabs the first capture group from the regex, the one character we matched and don't want to lose
          replace: '__non_webpack_require__($1',
          flags: 'g'
        }
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.FLUENTFFMPEG_COV': false
    })
  ]
};
